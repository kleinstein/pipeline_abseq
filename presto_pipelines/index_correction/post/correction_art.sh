#!/bin/bash

# Bash script for UID pipeline after indexing correction
# Ruoyi Jiang
# 7/18/17

#REFERENCES
REFERENCE_DIR=/ysm-gpfs/pi/kleinstein/oconnor_abseq/reference

PRIMER_DIR=$REFERENCE_DIR/primers
PRIMERS1=$PRIMER_DIR/AbSeqV3_Human_R1CPrimers.fasta
PRIMERS2=$PRIMER_DIR/AbSeqV3_Human_R2TSPrimers.fasta
CPRIMERS=$PRIMER_DIR/AbSeqV3_Human_InternalCRegion.fasta
REF_FILE=$REFERENCE_DIR/germlines/IMGT_Human_IGV_ungapped_2017-01-29.fasta

#SPECIFIC RUN
SAMPLE_ID=$1
RAW_DIR=$2
PRESTO_DIR=$3

#Set starting raw files
FILE1=$RAW_DIR/$(ls $RAW_DIR | grep $SAMPLE_ID | grep "R1")
FILE2=$RAW_DIR/$(ls $RAW_DIR | grep $SAMPLE_ID | grep "R2")

#Set output directories
OUTPUT_DIR=$PRESTO_DIR/presto
LOG_DIR=$PRESTO_DIR/log
TABLE_DIR=$PRESTO_DIR/table
PIPELINE_LOG=$PRESTO_DIR/$SAMPLE_ID-pipeline.log
ERROR_LOG=$PRESTO_DIR/$SAMPLE_ID-error.log




#SPECIFIC RUN
# SAMPLE_ID=DM1
# RAW_DIR=/ysm-gpfs/pi/kleinstein/oconnor_abseq/oconnor_myositis/raw/RQ2410_A7VEF_2014-04-2/A7VEF_DM1
# PRESTO_DIR=/ysm-gpfs/pi/kleinstein/oconnor_abseq/oconnor_myositis/presto/std_uid/RQ2410_A7VEF_2014-04-2/DM1



#SplitSeq.py sample, sample 50k sequences randomly from the concatenated fastq file
SplitSeq.py sample \
-s $OUTPUT_DIR/$SAMPLE_ID-INDEX.fastq \
-n 10000 \
--outname ${SAMPLE_ID}-SPLIT-SAMPLE --outdir $OUTPUT_DIR \
>> $PIPELINE_LOG \
2> $ERROR_LOG


#EstimateErrorTotal.py: estimate the sequence error rate in the first 50 base pairs as a proxy for the UID error rate
#UID pipeline specific function
EstimateErrorTotal.py \
-s $OUTPUT_DIR/$SAMPLE_ID-SPLIT-SAMPLE_sample1-n10000.fastq \
-f BARCODE \
-len 50 \
--dtn \
--log $LOG_DIR/$SAMPLE_ID-EstimateErrorTotalUID.log \
--outname "$SAMPLE_ID-ERR-uid" --outdir $OUTPUT_DIR \
>> $PIPELINE_LOG \
2> $ERROR_LOG


# OUTPUT_DIR=/ysm-gpfs/pi/kleinstein/oconnor_abseq/oconnor_myositis/presto/std_uid/RQ2410_A7VEF_2014-04-2/DM3/presto
# LOG_DIR=/ysm-gpfs/pi/kleinstein/oconnor_abseq/oconnor_myositis/presto/std_uid/RQ2410_A7VEF_2014-04-2/DM3/log
# SAMPLE_ID=DM3

#Extract the threshold hamming distance for collapsing UIDs (average linkage) from EET
UID_THRESHOLD=$(cat $OUTPUT_DIR/$SAMPLE_ID-ERR-uid_dist-error.tab | grep 'UID_THRESH_95' | awk '{print $2}')
AWK_EXPR_UID="BEGIN {print (17-$UID_THRESHOLD)/17}"
UID_THRESHOLD_PERCENT=$(awk "$AWK_EXPR_UID")

#ClusterSingle.py: cluster reads by UID group, creates new field CLUSTER_UID
#UID pipeline specific function
ClusterDirty.py barcode \
-s $OUTPUT_DIR/$SAMPLE_ID-INDEX.fastq \
-f BARCODE \
-k CLUSTER_UID \
--id $UID_THRESHOLD_PERCENT \
--log $LOG_DIR/$SAMPLE_ID-ClusterUID.log \
--outname "$SAMPLE_ID-ERR-uid" --outdir $OUTPUT_DIR \
>> $PIPELINE_LOG \
2> $ERROR_LOG





#SplitSeq.py sample, sample 50k sequences randomly from the concatenated fastq file
SplitSeq.py sample \
-s $OUTPUT_DIR/$SAMPLE_ID-ERR-uid_cluster-pass.fastq \
-n 50000 \
--outname ${SAMPLE_ID}-SPLIT-SAMPLE-seq --outdir $OUTPUT_DIR \
>> $PIPELINE_LOG \
2> $ERROR_LOG

#EstimateErrorTotal.py: repeat EET for the whole sequence
#UID pipeline specific function
EstimateErrorTotal.py \
-s $OUTPUT_DIR/$SAMPLE_ID-SPLIT-SAMPLE-seq_sample1-n50000.fastq \
-f BARCODE \
-len 500 \
--dtn \
--log $LOG_DIR/$SAMPLE_ID-EstimateErrorTotalSeq.log \
--outname "$SAMPLE_ID-ERR-seq" --outdir $OUTPUT_DIR \
>> $PIPELINE_LOG \
2> $ERROR_LOG


#Extract the threshold hamming distance for collapsing sequences 
#SEQ_THRESHOLD=$(cat $OUTPUT_DIR/$SAMPLE_ID-ERR-seq_dist-error.tab | grep 'SEQ_THRESH' | grep -v 'SEQ_THRESH'_95 | awk '{print $2}')
#AWK_EXPR_SEQ="BEGIN {print (500-$SEQ_THRESHOLD)/500}"
#SEQ_THRESHOLD_PERCENT=$(awk "$AWK_EXPR_SEQ")
SEQ_THRESHOLD_PERCENT=0.7

#ClusterSets.py: cluster sequences within UID clusters
ClusterSets.py set \
-s $OUTPUT_DIR/$SAMPLE_ID-ERR-uid_cluster-pass.fastq \
-f CLUSTER_UID \
-k CLUSTER_SEQ \
--id $SEQ_THRESHOLD_PERCENT \
--log $LOG_DIR/$SAMPLE_ID-ClusterSeq.log \
--outname "$SAMPLE_ID-ERR-seq" --outdir $OUTPUT_DIR \
>> $PIPELINE_LOG \
2> $ERROR_LOG






#ParseHeaders.py copy: collapse the Cluster_UID and Cluster_SEQ to create a final "true" barcode, CLUSTER_UID+CLUSTER_SEQ=CLUSTER_SEQ corresponding to individual mRNA molecules
ParseHeaders.py copy \
-s $OUTPUT_DIR/$SAMPLE_ID-ERR-seq_cluster-pass.fastq \
-f CLUSTER_UID \
-k CLUSTER_SEQ \
--act set \
--outname "$SAMPLE_ID-ERR-seq" --outdir $OUTPUT_DIR \
>> $PIPELINE_LOG \
2> $ERROR_LOG


#PairSeq.py: transfer the new barcodes over to the original read1 
PairSeq.py \
-1 $OUTPUT_DIR/$SAMPLE_ID-INDEX-R1.fastq \
-2 $OUTPUT_DIR/$SAMPLE_ID-ERR-seq_reheader.fastq \
--2f CLUSTER_SEQ \
--coord presto \
--outname "$SAMPLE_ID-R1-CLUST" --outdir $OUTPUT_DIR \
>> $PIPELINE_LOG \
2> $ERROR_LOG

#PairSeq.py: transfer the new barcodes over to the original read2
PairSeq.py \
-1 $OUTPUT_DIR/$SAMPLE_ID-INDEX-R2.fastq \
-2 $OUTPUT_DIR/$SAMPLE_ID-ERR-seq_reheader.fastq \
--2f CLUSTER_SEQ \
--coord presto \
--outname "$SAMPLE_ID-R2-CLUST" --outdir $OUTPUT_DIR \
>> $PIPELINE_LOG \
2> $ERROR_LOG



#BuildConsensus.py:
#No need for max_error since ClusterSetsSeq has ensured that each group is from a "true" molecule
#--maxgap for building consensus, the frequency of a given position for . or - above which, the position is simply deleted (cleans indels)
#--prcons is a frequency threshold for assigning a primer group
#-q is the phred score below which consensus building is avoided (here, not used and set to 0, the default)
BuildConsensus.py \
-s $OUTPUT_DIR/$SAMPLE_ID-R1-CLUST-1_pair-pass.fastq \
--bf CLUSTER_SEQ \
--pf PRIMER \
-q 0 \
--maxgap 0.5 \
--prcons 0.6 \
--log $LOG_DIR/$SAMPLE_ID-R1_BuildConsensus.log \
--outname "$SAMPLE_ID-R1" --outdir $OUTPUT_DIR \
>> $PIPELINE_LOG \
2> $ERROR_LOG

BuildConsensus.py \
-s $OUTPUT_DIR/$SAMPLE_ID-R2-CLUST-1_pair-pass.fastq \
--bf CLUSTER_SEQ \
--pf PRIMER \
-q 0 \
--maxgap 0.5 \
--log $LOG_DIR/$SAMPLE_ID-R2_BuildConsensus.log \
--outname "$SAMPLE_ID-R2" --outdir $OUTPUT_DIR \
>> $PIPELINE_LOG \
2> $ERROR_LOG

#PairSeq.py: re-pair the reads after consensus building 
PairSeq.py \
-1 $OUTPUT_DIR/$SAMPLE_ID-R1_consensus-pass.fastq \
-2 $OUTPUT_DIR/$SAMPLE_ID-R2_consensus-pass.fastq \
--coord presto \
--outname "$SAMPLE_ID-BC" --outdir $OUTPUT_DIR \
>> $PIPELINE_LOG \
2> $ERROR_LOG




#### STANDARD PIPELINE ####




#AssemblePairs.py align
AP_ALN_SCANREV=true
AP_ALN_MAXERR=0.3
AP_ALN_MINLEN=8
AP_ALN_ALPHA=1e-5

AssemblePairs.py align \
-1 "$OUTPUT_DIR/$SAMPLE_ID"-BC-2_pair-pass.fastq \
-2 "$OUTPUT_DIR/$SAMPLE_ID"-BC-1_pair-pass.fastq \
--1f PRCONS CONSCOUNT \
--2f CONSCOUNT \
--coord presto \
--rc tail \
--minlen $AP_ALN_MINLEN \
--maxerror $AP_ALN_MAXERR \
--alpha $AP_ALN_ALPHA \
--scanrev \
--failed \
--log "$LOG_DIR/$SAMPLE_ID"_AssemblePairsAlign.log \
--outname $SAMPLE_ID-ALN --outdir $OUTPUT_DIR \
>> $PIPELINE_LOG \
2> $ERROR_LOG

#AssemblePairs.py reference
AP_REF_MINIDENT=0.5
AP_REF_EVALUE=1e-5
AP_REF_MAXHITS=100

AssemblePairs.py reference \
-1 "$OUTPUT_DIR/$SAMPLE_ID"-ALN-1_assemble-fail.fastq \
-2 "$OUTPUT_DIR/$SAMPLE_ID"-ALN-2_assemble-fail.fastq \
-r $REF_FILE \
--1f PRCONS CONSCOUNT \
--2f CONSCOUNT \
--coord presto \
--rc tail \
--minident $AP_REF_MINIDENT \
--evalue $AP_REF_EVALUE \
--maxhits $AP_REF_MAXHITS \
--exec blastn \
--dbexec makeblastdb \
--aligner blastn \
--failed \
--log "$LOG_DIR/$SAMPLE_ID"_AssemblePairsReference.log \
--outname $SAMPLE_ID-REF --outdir $OUTPUT_DIR \
>> $PIPELINE_LOG \
2> $ERROR_LOG

#Concatenate the outputs from AssemblePairs.py align and reference
cat $OUTPUT_DIR/$SAMPLE_ID-ALN_assemble-pass.fastq $OUTPUT_DIR/$SAMPLE_ID-REF_assemble-pass.fastq > \
    $OUTPUT_DIR/$SAMPLE_ID-CAT_assemble-pass.fastq



#MaskPrimers.py align: align C-region primers

MaskPrimers.py align \
-s "$OUTPUT_DIR/$SAMPLE_ID"-CAT_assemble-pass.fastq \
-p $CPRIMERS \
--maxlen 100 \
--maxerror 0.4 \
--mode tag \
--revpr \
--skiprc \
--failed \
--log "$LOG_DIR/$SAMPLE_ID"_MaskPrimersCRegion.log \
--outname $SAMPLE_ID-CR --outdir $OUTPUT_DIR \
>> $PIPELINE_LOG \
2> $ERROR_LOG

#ParseHeaders.py rename: PRIMER -> CREGION renaming
ParseHeaders.py rename \
-s "$OUTPUT_DIR/$SAMPLE_ID"-CR_primers-pass.fastq \
-f PRIMER \
-k CREGION \
--outname $SAMPLE_ID-CR --outdir $OUTPUT_DIR \
>> $PIPELINE_LOG \
2> $ERROR_LOG	



#ParseHeaders.py collapse: return the minimum conscount
ParseHeaders.py collapse \
-s "$OUTPUT_DIR/$SAMPLE_ID"-CR_reheader.fastq \
-f CONSCOUNT \
--act min \
--outname $SAMPLE_ID-FIN --outdir $OUTPUT_DIR \
>> $PIPELINE_LOG \
2> $ERROR_LOG	

#CollapseSeq.py: create DUPCOUNT
CollapseSeq.py \
-s "$OUTPUT_DIR/$SAMPLE_ID"-FIN_reheader.fastq \
-n 0 \
--uf PRCONS CREGION \
--cf CONSCOUNT \
--act sum \
--inner \
--keepmiss \
--outname $SAMPLE_ID-FIN --outdir $OUTPUT_DIR \
>> $PIPELINE_LOG \
2> $ERROR_LOG	

#SplitSeq.py group: only report those sequences with CONSCOUNT greater than 2
SplitSeq.py group \
-s "$OUTPUT_DIR/$SAMPLE_ID"-FIN_collapse-unique.fastq \
-f CONSCOUNT \
--num 2 \
>> $PIPELINE_LOG \
2> $ERROR_LOG	



###PARSE LOGS#### 
#No need for pipeline log checking
ParseHeaders.py table \
-s "$OUTPUT_DIR/$SAMPLE_ID"-FIN_reheader.fastq \
-f ID PRCONS CREGION CONSCOUNT \
--outname $SAMPLE_ID-Final --outdir $TABLE_DIR \
>> $PIPELINE_LOG \
2> $ERROR_LOG	

ParseHeaders.py table \
-s "$OUTPUT_DIR/$SAMPLE_ID"-FIN_collapse-unique.fastq \
-f ID PRCONS CREGION CONSCOUNT DUPCOUNT \
--outname $SAMPLE_ID-Final-Unique --outdir $TABLE_DIR \
>> $PIPELINE_LOG \
2> $ERROR_LOG	

ParseHeaders.py table \
-s "$OUTPUT_DIR/$SAMPLE_ID"-FIN_collapse-unique_atleast-2.fastq \
-f ID PRCONS CREGION CONSCOUNT DUPCOUNT \
--outname $SAMPLE_ID-Final-Unique-Atleast2 --outdir $TABLE_DIR \
>> $PIPELINE_LOG \
2> $ERROR_LOG	

#No need for pipeline log checking
#ParseLog.py -l "$LOG_DIR/$SAMPLE_ID"-R[1-2]_FilterSeq.log -f ID QUALITY --outdir $TABLE_DIR 2> $ERROR_LOG
ParseLog.py -l "$LOG_DIR/$SAMPLE_ID"-R[1-2]_MaskPrimers.log -f ID BARCODE PRIMER ERROR --outdir $TABLE_DIR 2> $ERROR_LOG
ParseLog.py -l "$LOG_DIR/$SAMPLE_ID"-R[1-2]_BuildConsensus.log -f BARCODE SEQCOUNT CONSCOUNT PRIMER PRCONS PRCOUNT PRFREQ ERROR --outdir $TABLE_DIR 2> $ERROR_LOG
ParseLog.py -l "$LOG_DIR/$SAMPLE_ID"_AssemblePairsAlign.log -f ID LENGTH OVERLAP ERROR PVALUE FIELDS1 FIELDS2 --outdir $TABLE_DIR 2> $ERROR_LOG
ParseLog.py -l "$LOG_DIR/$SAMPLE_ID"_AssemblePairsReference.log -f ID REFID LENGTH OVERLAP GAP EVALUE1 EVALUE2 IDENTITY FIELDS1 FIELDS2 --outdir $TABLE_DIR 2> $ERROR_LOG
ParseLog.py -l "$LOG_DIR/$SAMPLE_ID"_MaskPrimersCRegion.log -f ID PRIMER ERROR --outdir $TABLE_DIR 2> $ERROR_LOG



#DUPLICATES

# UID error correction
# http://journals.plos.org/plosone/article?id=10.1371/journal.pone.0146638
#
#
# UID development in Ig seq
# http://advances.sciencemag.org/content/advances/2/3/e1501371.full.pdf
#
# Lev based barcode correction
# http://bmcbioinformatics.biomedcentral.com/articles/10.1186/1471-2105-14-272
#
# Tutorial 3
# http://www.imtools.org/tutorial/
