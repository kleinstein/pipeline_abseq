#!/bin/bash

# Bash script for STD pipeline after indexing correction
# Ruoyi Jiang
# 7/18/17

#REFERENCES
REFERENCE_DIR=/ysm-gpfs/pi/kleinstein/oconnor_abseq/reference

PRIMER_DIR=$REFERENCE_DIR/primers
PRIMERS1=$PRIMER_DIR/AbSeqV3_Human_R1CPrimers.fasta
PRIMERS2=$PRIMER_DIR/AbSeqV3_Human_R2TSPrimers.fasta
CPRIMERS=$PRIMER_DIR/AbSeqV3_Human_InternalCRegion.fasta
REF_FILE=$REFERENCE_DIR/germlines/IMGT_Human_IGV_ungapped_2017-01-29.fasta

#SPECIFIC RUN
RAW_DIR=$1
PRESTO_DIR=$2

SAMPLE=$(basename ${RAW_DIR})

#Set starting raw files
FILE1=$RAW_DIR/$(ls $RAW_DIR | grep "R1")
FILE2=$RAW_DIR/$(ls $RAW_DIR | grep "R2")

#Set output directories
OUTPUT_DIR=$PRESTO_DIR/presto
LOG_DIR=$PRESTO_DIR/log
TABLE_DIR=$PRESTO_DIR/table
PIPELINE_LOG=$PRESTO_DIR/$SAMPLE-pipeline.log
ERROR_LOG=$PRESTO_DIR/$SAMPLE-error.log


#BuildConsensus.py: build consensus for UID groups, create CONSCOUNT

#This first command generates a PRCONS field from the PRIMER field using --prcons
BuildConsensus.py \
-s "$OUTPUT_DIR/$SAMPLE"-INDEX-R1.fastq \
--bf BARCODE \
--pf PRIMER \
-q 0 \
--maxerror 0.1 \
--maxgap 0.5 \
--prcons 0.6 \
--log "$LOG_DIR/$SAMPLE"-R1_BuildConsensus.log \
--outname $SAMPLE-R1 --outdir $OUTPUT_DIR \
>> $PIPELINE_LOG \
2> $ERROR_LOG

BuildConsensus.py \
-s "$OUTPUT_DIR/$SAMPLE"-INDEX-R2.fastq \
--bf BARCODE \
--pf PRIMER \
-q 0 \
--maxerror 0.1 \
--maxgap 0.5 \
--log "$LOG_DIR/$SAMPLE"-R2_BuildConsensus.log \
--outname $SAMPLE-R2 --outdir $OUTPUT_DIR \
>> $PIPELINE_LOG \
2> $ERROR_LOG

#PairSeq.py: align the groups after BuildConsensus
PairSeq.py \
-1 "$OUTPUT_DIR/$SAMPLE"-R1_consensus-pass.fastq \
-2 "$OUTPUT_DIR/$SAMPLE"-R2_consensus-pass.fastq  \
--coord presto \
--outdir $OUTPUT_DIR \
>> $PIPELINE_LOG \
2> $ERROR_LOG



#AssemblePairs.py align
AP_ALN_SCANREV=true
AP_ALN_MAXERR=0.3
AP_ALN_MINLEN=8
AP_ALN_ALPHA=1e-5

AssemblePairs.py align \
-1 "$OUTPUT_DIR/$SAMPLE"-R2_consensus-pass_pair-pass.fastq \
-2 "$OUTPUT_DIR/$SAMPLE"-R1_consensus-pass_pair-pass.fastq \
--1f PRCONS CONSCOUNT \
--2f CONSCOUNT \
--coord presto \
--rc tail \
--minlen $AP_ALN_MINLEN \
--maxerror $AP_ALN_MAXERR \
--alpha $AP_ALN_ALPHA \
--scanrev \
--failed \
--log "$LOG_DIR/$SAMPLE"_AssemblePairsAlign.log \
--outname $SAMPLE-ALN --outdir $OUTPUT_DIR \
>> $PIPELINE_LOG \
2> $ERROR_LOG

#AssemblePairs.py reference
AP_REF_MINIDENT=0.5
AP_REF_EVALUE=1e-5
AP_REF_MAXHITS=100

AssemblePairs.py reference \
-1 "$OUTPUT_DIR/$SAMPLE"-ALN-1_assemble-fail.fastq \
-2 "$OUTPUT_DIR/$SAMPLE"-ALN-2_assemble-fail.fastq \
-r $REF_FILE \
--1f PRCONS CONSCOUNT \
--2f CONSCOUNT \
--coord presto \
--rc tail \
--minident $AP_REF_MINIDENT \
--evalue $AP_REF_EVALUE \
--maxhits $AP_REF_MAXHITS \
--exec blastn \
--dbexec makeblastdb \
--aligner blastn \
--failed \
--log "$LOG_DIR/$SAMPLE"_AssemblePairsReference.log \
--outname $SAMPLE-REF --outdir $OUTPUT_DIR \
>> $PIPELINE_LOG \
2> $ERROR_LOG


#Concatenate the outputs from AssemblePairs.py align and reference
if [ "$(ls -A "${OUTPUT_DIR}"/${SAMPLE}-REF_assemble-pass.fastq)" ]; then
    cat $OUTPUT_DIR/$SAMPLE-ALN_assemble-pass.fastq $OUTPUT_DIR/$SAMPLE-REF_assemble-pass.fastq > $OUTPUT_DIR/$SAMPLE-CAT_assemble-pass.fastq
else 
    cp $OUTPUT_DIR/$SAMPLE-ALN_assemble-pass.fastq $OUTPUT_DIR/$SAMPLE-CAT_assemble-pass.fastq
fi



#MaskPrimers.py align: align C-region primers
MaskPrimers.py align \
-s "$OUTPUT_DIR/$SAMPLE"-CAT_assemble-pass.fastq \
-p $CPRIMERS \
--maxlen 100 \
--maxerror 0.4 \
--mode tag \
--revpr \
--skiprc \
--failed \
--log "$LOG_DIR/$SAMPLE"_MaskPrimersCRegion.log \
--outname $SAMPLE-CR --outdir $OUTPUT_DIR \
>> $PIPELINE_LOG \
2> $ERROR_LOG

#ParseHeaders.py rename: PRIMER -> CREGION renaming
ParseHeaders.py rename \
-s "$OUTPUT_DIR/$SAMPLE"-CR_primers-pass.fastq \
-f PRIMER \
-k CREGION \
--outname $SAMPLE-CR --outdir $OUTPUT_DIR \
>> $PIPELINE_LOG \
2> $ERROR_LOG	



#ParseHeaders.py collapse: return the minimum conscount
ParseHeaders.py collapse \
-s "$OUTPUT_DIR/$SAMPLE"-CR_reheader.fastq \
-f CONSCOUNT \
--act min \
--outname $SAMPLE-FIN --outdir $OUTPUT_DIR \
>> $PIPELINE_LOG \
2> $ERROR_LOG	

#CollapseSeq.py: create DUPCOUNT
CollapseSeq.py \
-s "$OUTPUT_DIR/$SAMPLE"-FIN_reheader.fastq \
-n 0 \
--uf PRCONS CREGION \
--cf CONSCOUNT \
--act sum \
--inner \
--keepmiss \
--outname $SAMPLE-FIN --outdir $OUTPUT_DIR \
>> $PIPELINE_LOG \
2> $ERROR_LOG	

#SplitSeq.py group: only report those sequences with CONSCOUNT greater than 2
SplitSeq.py group \
-s "$OUTPUT_DIR/$SAMPLE"-FIN_collapse-unique.fastq \
-f CONSCOUNT \
--num 2 \
>> $PIPELINE_LOG \
2> $ERROR_LOG	




###PARSE LOGS#### 
#No need for pipeline log checking
ParseHeaders.py table \
-s "$OUTPUT_DIR/$SAMPLE"-FIN_reheader.fastq \
-f ID PRCONS CREGION CONSCOUNT \
--outname $SAMPLE-Final --outdir $TABLE_DIR \
>> $PIPELINE_LOG \
2> $ERROR_LOG	

ParseHeaders.py table \
-s "$OUTPUT_DIR/$SAMPLE"-FIN_collapse-unique.fastq \
-f ID PRCONS CREGION CONSCOUNT DUPCOUNT \
--outname $SAMPLE-Final-Unique --outdir $TABLE_DIR \
>> $PIPELINE_LOG \
2> $ERROR_LOG	

ParseHeaders.py table \
-s "$OUTPUT_DIR/$SAMPLE"-FIN_collapse-unique_atleast-2.fastq \
-f ID PRCONS CREGION CONSCOUNT DUPCOUNT \
--outname $SAMPLE-Final-Unique-Atleast2 --outdir $TABLE_DIR \
>> $PIPELINE_LOG \
2> $ERROR_LOG	

#No need for pipeline log checking
ParseLog.py -l "$LOG_DIR/$SAMPLE"-R[1-2]_FilterSeq.log -f ID QUALITY --outdir $TABLE_DIR 2> $ERROR_LOG
ParseLog.py -l "$LOG_DIR/$SAMPLE"-R[1-2]_MaskPrimers.log -f ID BARCODE PRIMER ERROR --outdir $TABLE_DIR 2> $ERROR_LOG
ParseLog.py -l "$LOG_DIR/$SAMPLE"-R[1-2]_BuildConsensus.log -f BARCODE SEQCOUNT CONSCOUNT PRIMER PRCONS PRCOUNT PRFREQ ERROR --outdir $TABLE_DIR 2> $ERROR_LOG
ParseLog.py -l "$LOG_DIR/$SAMPLE"_AssemblePairsAlign.log -f ID LENGTH OVERLAP ERROR PVALUE FIELDS1 FIELDS2 --outdir $TABLE_DIR 2> $ERROR_LOG
ParseLog.py -l "$LOG_DIR/$SAMPLE"_AssemblePairsReference.log -f ID REFID LENGTH OVERLAP GAP EVALUE1 EVALUE2 IDENTITY FIELDS1 FIELDS2 --outdir $TABLE_DIR 2> $ERROR_LOG
ParseLog.py -l "$LOG_DIR/$SAMPLE"_MaskPrimersCRegion.log -f ID PRIMER ERROR --outdir $TABLE_DIR 2> $ERROR_LOG

#FINAL_DIR selections
mv ${OUTPUT_DIR}/${SAMPLE_D}-FIN_collapse-unique_atleast-2.fastq ${FINAL_DIR}/${SAMPLE_D}-FIN_collapse-unique_atleast-2.fastq
