#!/bin/bash

# Bash script for STD pipeline prior to indexing correction
# Ruoyi Jiang
# 7/18/17

#REFERENCES
REFERENCE_DIR=/ysm-gpfs/pi/kleinstein/oconnor_abseq/reference

PRIMER_DIR=$REFERENCE_DIR/primers
PRIMERS1=$PRIMER_DIR/AbSeqV3_Human_R1CPrimers.fasta
PRIMERS2=$PRIMER_DIR/AbSeqV3_Human_R2TSPrimers.fasta
CPRIMERS=$PRIMER_DIR/AbSeqV3_Human_InternalCRegion.fasta
REF_FILE=$REFERENCE_DIR/germlines/IMGT_Human_IGV_ungapped_2017-01-29.fasta

#SPECIFIC RUN
RAW_DIR=$1
PRESTO_DIR=$2

SAMPLE_ID=$(basename ${RAW_DIR})

#Set starting raw files
FILE1=$RAW_DIR/$(ls $RAW_DIR | grep "R1")
FILE2=$RAW_DIR/$(ls $RAW_DIR | grep "R2")

#Set output directories
OUTPUT_DIR=$PRESTO_DIR/presto
LOG_DIR=$PRESTO_DIR/log
TABLE_DIR=$PRESTO_DIR/table
FINAL_DIR=${PRESTO_DIR}/output

mkdir -p ${PRESTO_DIR}
mkdir -p ${OUTPUT_DIR}
mkdir -p ${LOG_DIR}
mkdir -p ${TABLE_DIR}
mkdir -p ${FINAL_DIR}

PIPELINE_LOG=$PRESTO_DIR/$SAMPLE_ID-pipeline.log
ERROR_LOG=$PRESTO_DIR/$SAMPLE_ID-error.log


#Start logs
date > $PIPELINE_LOG
date > $ERROR_LOG

#FilterSeq.py quality: remove sequences that have average phred scores below q (20)
FilterSeq.py quality \
-s $FILE1 \
-q 20 \
--log "$LOG_DIR/$SAMPLE_ID"-R1_FilterSeq.log \
--outname $SAMPLE_ID-R1 --outdir $OUTPUT_DIR \
>> $PIPELINE_LOG \
2> $ERROR_LOG

FilterSeq.py quality \
-s $FILE2 \
-q 20 \
--log "$LOG_DIR/$SAMPLE_ID"-R2_FilterSeq.log \
--outname $SAMPLE_ID-R2 --outdir $OUTPUT_DIR \
>> $PIPELINE_LOG \
2> $ERROR_LOG


#MaskPrimers.py score --mode cut: identify and cut out primers
MaskPrimers.py score \
-s "$OUTPUT_DIR/$SAMPLE_ID"-R1_quality-pass.fastq \
-p $PRIMERS1 \
--mode cut \
--start 0 \
--maxerror 0.2 \
--log "$LOG_DIR/$SAMPLE_ID"-R1_MaskPrimers.log \
--outname $SAMPLE_ID-R1 --outdir $OUTPUT_DIR \
>> $PIPELINE_LOG \
2> $ERROR_LOG

MaskPrimers.py score \
-s "$OUTPUT_DIR/$SAMPLE_ID"-R2_quality-pass.fastq \
-p $PRIMERS2 \
--mode cut \
--start 17 \
--barcode \
--maxerror 0.5 \
--log "$LOG_DIR/$SAMPLE_ID"-R2_MaskPrimers.log \
--outname $SAMPLE_ID-R2 --outdir $OUTPUT_DIR \
>> $PIPELINE_LOG \
2> $ERROR_LOG

#PairSeq.py: pair the reads by illumina coord
PairSeq.py \
-1 "$OUTPUT_DIR/$SAMPLE_ID"-R1_primers-pass.fastq \
-2 "$OUTPUT_DIR/$SAMPLE_ID"-R2_primers-pass.fastq \
--2f BARCODE \
--coord illumina \
--outdir $OUTPUT_DIR \
>> $PIPELINE_LOG \
2> $ERROR_LOG



#AlignSets.py: align UID groups for BuildConsensus
AlignSets.py muscle \
-s "$OUTPUT_DIR/$SAMPLE_ID"-R1_primers-pass_pair-pass.fastq \
--exec muscle \
--log "$LOG_DIR/$SAMPLE_ID"-R1_AlignSets.log \
--outname $SAMPLE_ID-R1 --outdir $OUTPUT_DIR \
>> $PIPELINE_LOG \
2> $ERROR_LOG

AlignSets.py muscle \
-s "$OUTPUT_DIR/$SAMPLE_ID"-R2_primers-pass_pair-pass.fastq \
--exec muscle \
--log "$LOG_DIR/$SAMPLE_ID"-R2_AlignSets.log \
--outname $SAMPLE_ID-R2 --outdir $OUTPUT_DIR \
>> $PIPELINE_LOG \
2> $ERROR_LOG


#PairSeq.py: pair the reads by illumina coord, for AssemblePairs
PairSeq.py \
-1 "$OUTPUT_DIR/$SAMPLE_ID"-R1_align-pass.fastq \
-2 "$OUTPUT_DIR/$SAMPLE_ID"-R2_align-pass.fastq \
--coord illumina \
--outdir $OUTPUT_DIR \
>> $PIPELINE_LOG \
2> $ERROR_LOG		
	
	
#AssemblePairs.py join: Concatenate the sequence pairs end-2-end to EstimateError
AssemblePairs.py join \
-1 "$OUTPUT_DIR/$SAMPLE_ID"-R1_align-pass_pair-pass.fastq \
-2 "$OUTPUT_DIR/$SAMPLE_ID"-R2_align-pass_pair-pass.fastq \
--1f PRIMER \
--2f BARCODE \
--coord illumina \
--log $LOG_DIR/$SAMPLE_ID-AssemblePairsJoin.log \
--outname $SAMPLE_ID-INDEX --outdir $OUTPUT_DIR \
>> $PIPELINE_LOG \
2> $ERROR_LOG

#Add the sample identity to the outputs
ParseHeaders.py add \
-s "$OUTPUT_DIR/$SAMPLE_ID"-INDEX_assemble-pass.fastq \
-f SAMPLE \
-u $SAMPLE_ID \
--outname $SAMPLE_ID-SAMPLE --outdir $OUTPUT_DIR \
>> $PIPELINE_LOG \
2> $ERROR_LOG

ParseHeaders.py add \
-s "$OUTPUT_DIR/$SAMPLE_ID"-R1_align-pass_pair-pass.fastq \
-f SAMPLE \
-u $SAMPLE_ID \
--outname $SAMPLE_ID-SAMPLE-R1 --outdir $OUTPUT_DIR \
>> $PIPELINE_LOG \
2> $ERROR_LOG

ParseHeaders.py add \
-s "$OUTPUT_DIR/$SAMPLE_ID"-R2_align-pass_pair-pass.fastq \
-f SAMPLE \
-u $SAMPLE_ID \
--outname $SAMPLE_ID-SAMPLE-R2 --outdir $OUTPUT_DIR \
>> $PIPELINE_LOG \
2> $ERROR_LOG

#Rename the final output from the PRE section of the pipeline
mv $OUTPUT_DIR/${SAMPLE_ID}-SAMPLE_reheader.fastq $OUTPUT_DIR/${SAMPLE_ID}-PRE.fastq
mv $OUTPUT_DIR/${SAMPLE_ID}-SAMPLE-R1_reheader.fastq $OUTPUT_DIR/${SAMPLE_ID}-PRE-R1.fastq
mv $OUTPUT_DIR/${SAMPLE_ID}-SAMPLE-R2_reheader.fastq $OUTPUT_DIR/${SAMPLE_ID}-PRE-R2.fastq
