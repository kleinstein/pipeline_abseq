
#!/bin/bash

# Bash script for no indexing correction
# Ruoyi Jiang
# 7/18/17

#REFERENCES
REFERENCE_DIR=/ysm-gpfs/pi/kleinstein/oconnor_abseq/reference

PRIMER_DIR=$REFERENCE_DIR/primers
PRIMERS1=$PRIMER_DIR/AbSeqV3_Human_R1CPrimers.fasta
PRIMERS2=$PRIMER_DIR/AbSeqV3_Human_R2TSPrimers.fasta
CPRIMERS=$PRIMER_DIR/AbSeqV3_Human_InternalCRegion.fasta
REF_FILE=$REFERENCE_DIR/germlines/IMGT_Human_IGV_ungapped_2017-01-29.fasta

#SPECIFIC RUN
EXP_DIR=$1

#EXP_DIR=/ysm-gpfs/pi/kleinstein/oconnor_abseq/oconnor_derisi/presto/no_std/RQ1

#Create a hidden folder for the pipeline run 
INDEX_DIR=$EXP_DIR/.indexing_no
mkdir -p $INDEX_DIR 

#Set output directories
OUTPUT_DIR=$INDEX_DIR/presto
LOG_DIR=$INDEX_DIR/log
PIPELINE_LOG=$INDEX_DIR/pipeline.log
ERROR_LOG=$INDEX_DIR/error.log

mkdir -p $OUTPUT_DIR
mkdir -p $LOG_DIR


SAMPLES=$(ls $EXP_DIR)


#Concatenate the PRE files in each sample directory
for SAMPLE in $SAMPLES; do
	cat $EXP_DIR/$SAMPLE/presto/${SAMPLE}-PRE.fastq 
done > $OUTPUT_DIR/JOIN.fastq &

for SAMPLE in $SAMPLES; do
	cat $EXP_DIR/$SAMPLE/presto/${SAMPLE}-PRE-R1.fastq 
done > $OUTPUT_DIR/JOIN-R1.fastq &

for SAMPLE in $SAMPLES; do
	cat $EXP_DIR/$SAMPLE/presto/${SAMPLE}-PRE-R2.fastq 
done > $OUTPUT_DIR/JOIN-R2.fastq &

##AsemblePass converts the headers illumina->presto (to strip the R1, R2 info)

#ConvertHeaders.py: convert headers of read1, read2 files to prepare for PairSeq to transfer over the new barcode CLUSTER_SEQ
ConvertHeaders.py illumina \
-s $OUTPUT_DIR/JOIN-R1.fastq \
--outname "JOIN-R1-COORD" --outdir $OUTPUT_DIR \
>> $PIPELINE_LOG \
2> $ERROR_LOG

ConvertHeaders.py illumina \
-s $OUTPUT_DIR/JOIN-R2.fastq \
--outname "JOIN-R2-COORD" --outdir $OUTPUT_DIR \
>> $PIPELINE_LOG \
2> $ERROR_LOG

#Split the concatenated file into INDEX files
SplitSeq.py group \
-s $OUTPUT_DIR/JOIN.fastq \
-f SAMPLE \
--outname INDEX-ASSEMBLE --outdir $OUTPUT_DIR \
>> $PIPELINE_LOG \
2> $ERROR_LOG

#Split the concatenated file into INDEX files
SplitSeq.py group \
-s $OUTPUT_DIR/JOIN-R1-COORD_convert-pass.fastq \
-f SAMPLE \
--outname INDEX-R1 --outdir $OUTPUT_DIR \
>> $PIPELINE_LOG \
2> $ERROR_LOG

#Split the concatenated file into INDEX files
SplitSeq.py group \
-s $OUTPUT_DIR/JOIN-R2-COORD_convert-pass.fastq \
-f SAMPLE \
--outname INDEX-R2 --outdir $OUTPUT_DIR \
>> $PIPELINE_LOG \
2> $ERROR_LOG

#Move and rename the resulting output files
for SAMPLE in $SAMPLES; do
	FILE=$(ls $OUTPUT_DIR | grep $SAMPLE | grep INDEX-ASSEMBLE)
	mv $OUTPUT_DIR/$FILE $EXP_DIR/$SAMPLE/presto/$SAMPLE-INDEX.fastq
    FILE_R1=$(ls $OUTPUT_DIR | grep $SAMPLE | grep INDEX-R1)
    mv $OUTPUT_DIR/$FILE_R1 $EXP_DIR/$SAMPLE/presto/$SAMPLE-INDEX-R1.fastq
    FILE_R2=$(ls $OUTPUT_DIR | grep $SAMPLE | grep INDEX-R2)
    mv $OUTPUT_DIR/$FILE_R2 $EXP_DIR/$SAMPLE/presto/$SAMPLE-INDEX-R2.fastq
done

