#!/bin/bash

# Bash script for consensus indexing correction
# Ruoyi Jiang
# 7/18/17


#REFERENCES
REFERENCE_DIR=/ysm-gpfs/pi/kleinstein/oconnor_abseq/reference

PRIMER_DIR=$REFERENCE_DIR/primers
PRIMERS1=$PRIMER_DIR/AbSeqV3_Human_R1CPrimers.fasta
PRIMERS2=$PRIMER_DIR/AbSeqV3_Human_R2TSPrimers.fasta
CPRIMERS=$PRIMER_DIR/AbSeqV3_Human_InternalCRegion.fasta
REF_FILE=$REFERENCE_DIR/germlines/IMGT_Human_IGV_ungapped_2017-01-29.fasta

#SPECIFIC RUN
EXP_DIR=$1

#EXP_DIR=/ysm-gpfs/pi/kleinstein/oconnor_abseq/oconnor_myositis/presto/consensus/RQ2410_A7VEF_2014-04-2

#Create a hidden folder for the pipeline run 
INDEX_DIR=$EXP_DIR/.indexing_uidcons
mkdir -p $INDEX_DIR 

#Set output directories
OUTPUT_DIR=$INDEX_DIR/presto
LOG_DIR=$INDEX_DIR/log
PIPELINE_LOG=$INDEX_DIR/pipeline.log
ERROR_LOG=$INDEX_DIR/error.log

mkdir -p $OUTPUT_DIR
mkdir -p $LOG_DIR


#Concatenate the PRE files in each sample directory
SAMPLES=$(ls $EXP_DIR)


for SAMPLE in $SAMPLES; do
    cat $EXP_DIR/$SAMPLE/presto/${SAMPLE}-PRE.fastq
done > $OUTPUT_DIR/JOIN.fastq &

for SAMPLE in $SAMPLES; do
    cat $EXP_DIR/$SAMPLE/presto/${SAMPLE}-PRE-R1.fastq
done > $OUTPUT_DIR/JOIN-R1.fastq &

for SAMPLE in $SAMPLES; do
    cat $EXP_DIR/$SAMPLE/presto/${SAMPLE}-PRE-R2.fastq
done > $OUTPUT_DIR/JOIN-R2.fastq &



#Sample from the JOIN.fastq
SplitSeq.py sample \
-s "${OUTPUT_DIR}"/JOIN.fastq \
-n 5000 --outname JOIN --outdir "${OUTPUT_DIR}" \
>> "${PIPELINE_LOG}" \
2> "${ERROR_LOG}"


#EstimateError.py
EstimateError.py barcode \
-s "${OUTPUT_DIR}"/JOIN_sample1-n5000.fastq \
-f BARCODE \
--outname JOIN --outdir "${OUTPUT_DIR}" \
>> "${PIPELINE_LOG}" \
2> "${ERROR_LOG}"


#ClusterSets.py barcode: Cluster barcodes
TABLE="${OUTPUT_DIR}"/JOIN_thresh-barcode.tab
UID_THRESHOLD_PERCENT=`python3 << END
import pandas as pd
threshold=1 - pd.read_table("${TABLE}", index_col='TYPE')['THRESH']['ALL']
if threshold < 0.80:
    print(0.80)
else:
    print(threshold)
END`

#TODO: cd-hit-est removed because exceeded 32GB on farnam
ClusterSets.py barcode \
-s "${OUTPUT_DIR}"/JOIN.fastq \
-f BARCODE \
-k INDEX_UID \
--ident ${UID_THRESHOLD_PERCENT} \
--outname JOIN-uid --outdir ${OUTPUT_DIR} \
--nproc 1 \
>> "${PIPELINE_LOG}" \
2> "${ERROR_LOG}"


#Sample from the uid-cluster file
SplitSeq.py sample \
-s "${OUTPUT_DIR}"/JOIN-uid_cluster-pass.fastq \
-n 10000 --outname JOIN --outdir "${OUTPUT_DIR}" \
>> "${PIPELINE_LOG}" \
2> "${ERROR_LOG}"

#EstimateError.py
EstimateError.py set \
-s "${OUTPUT_DIR}"/JOIN_sample1-n10000.fastq \
-f INDEX_UID -n 2 \
--outname JOIN --outdir "${OUTPUT_DIR}" \
>> "${PIPELINE_LOG}" \
2> "${ERROR_LOG}"

TABLE="${OUTPUT_DIR}"/JOIN_thresh-set.tab

#ClusterSingle.py: cluster the UIDs across all samples for similar UIDs
SEQ_THRESHOLD_PERCENT=`python3 << END
import pandas as pd
threshold=1 - pd.read_table("${TABLE}", index_col='TYPE')['THRESH']['ALL']
if threshold < 0.80:
    print(0.80)
else:
    print(threshold)
END`

#ClusterSets.py: cluster the sequences within the UID groups across all samples
ClusterSets.py set \
-s "${OUTPUT_DIR}"/JOIN-uid_cluster-pass.fastq \
-f INDEX_UID \
-k INDEX_SEQ \
--ident ${SEQ_THRESHOLD_PERCENT} \
--cluster cd-hit-est \
--outname JOIN-seq --outdir ${OUTPUT_DIR} \
>> "${PIPELINE_LOG}" \
2> "${ERROR_LOG}"




#ParseHeaders.py copy: collapse the INDEX_UID and INDEX_SEQ to create a final "true" barcode
ParseHeaders.py merge \
-s "${OUTPUT_DIR}"/JOIN-seq_cluster-pass.fastq \
-f INDEX_UID INDEX_SEQ \
-k INDEX_NEW \
--outname JOIN-seq --outdir "${OUTPUT_DIR}" \
>> "${PIPELINE_LOG}" \
2> "${ERROR_LOG}"


#Resolve collisions
UnifyHeaders.py consensus \
-s "${OUTPUT_DIR}"/JOIN-seq_reheader.fastq \
-f INDEX_NEW \
-k SAMPLE \
--outname JOIN --outdir "${OUTPUT_DIR}" \
--log "${LOG_DIR}"/UnifyHeaders.log \
>> "${PIPELINE_LOG}" \
2> "${ERROR_LOG}"


#SpletSeq.py group: divide into SAMPLEs
SplitSeq.py group \
-s "${OUTPUT_DIR}"/JOIN_unify-pass.fastq \
-f SAMPLE \
--outname JOIN --outdir "${OUTPUT_DIR}" \
>> "${PIPELINE_LOG}" \
2> "${ERROR_LOG}"


for SAMPLE in $SAMPLES; do
    PairSeq.py \
    -1 $OUTPUT_DIR/JOIN-R1.fastq \
    -2 $OUTPUT_DIR/JOIN_SAMPLE-${SAMPLE}.fastq \
    --2f INDEX_NEW \
    --coord presto \
    --outname ${SAMPLE}-INDEX-R1 \
    --outdir $OUTPUT_DIR \
    >> $PIPELINE_LOG \
    2> $ERROR_LOG
    
    rm $OUTPUT_DIR/${SAMPLE}-INDEX-R1-2_pair-pass.fastq
done

for SAMPLE in $SAMPLES; do
    PairSeq.py \
    -1 $OUTPUT_DIR/JOIN-R2.fastq \
    -2 $OUTPUT_DIR/JOIN_SAMPLE-${SAMPLE}.fastq \
    --2f INDEX_NEW \
    --coord presto \
    --outname ${SAMPLE}-INDEX-R2 \
    --outdir $OUTPUT_DIR \
    >> $PIPELINE_LOG \
    2> $ERROR_LOG
    
    rm $OUTPUT_DIR/${SAMPLE}-INDEX-R2-2_pair-pass.fastq
done

#Move and rename the resulting output files 
for SAMPLE in $SAMPLES; do
    FILE=JOIN_SAMPLE-${SAMPLE}.fastq
    mv $OUTPUT_DIR/$FILE ${EXP_DIR}/${SAMPLE}/presto/${SAMPLE}-INDEX.fastq
    FILE_R1=${SAMPLE}-INDEX-R1-1_pair-pass.fastq
    mv $OUTPUT_DIR/$FILE_R1 ${EXP_DIR}/${SAMPLE}/presto/${SAMPLE}-INDEX-R1.fastq
    FILE_R2=${SAMPLE}-INDEX-R2-1_pair-pass.fastq
    mv $OUTPUT_DIR/$FILE_R2 ${EXP_DIR}/${SAMPLE}/presto/${SAMPLE}-INDEX-R2.fastq
done

