
#!/bin/bash

# Bash script for no indexing correction
# Ruoyi Jiang
# 7/18/17

#REFERENCES
REFERENCE_DIR=/ysm-gpfs/pi/kleinstein/oconnor_abseq/reference

PRIMER_DIR=$REFERENCE_DIR/primers
PRIMERS1=$PRIMER_DIR/AbSeqV3_Human_R1CPrimers.fasta
PRIMERS2=$PRIMER_DIR/AbSeqV3_Human_R2TSPrimers.fasta
CPRIMERS=$PRIMER_DIR/AbSeqV3_Human_InternalCRegion.fasta
REF_FILE=$REFERENCE_DIR/germlines/IMGT_Human_IGV_ungapped_2017-01-29.fasta

#SPECIFIC RUN
EXP_DIR=$1

# EXP_DIR=/ysm-gpfs/pi/kleinstein/oconnor_abseq/oconnor_myositis/presto/uid_std/RQ2410_A7VEF_2014-04-2

#Create a hidden folder for the pipeline run 
INDEX_DIR=$EXP_DIR/.indexing_uidcons
mkdir -p $INDEX_DIR 

#Set output directories
OUTPUT_DIR=$INDEX_DIR/presto
LOG_DIR=$INDEX_DIR/log
PIPELINE_LOG=$INDEX_DIR/pipeline.log
ERROR_LOG=$INDEX_DIR/error.log

mkdir -p $OUTPUT_DIR
mkdir -p $LOG_DIR


#Concatenate the PRE files in each sample directory
SAMPLES=$(ls $EXP_DIR)




for SAMPLE in $SAMPLES; do
    cat $EXP_DIR/$SAMPLE/presto/${SAMPLE}-PRE.fastq
done > $OUTPUT_DIR/JOIN.fastq &

for SAMPLE in $SAMPLES; do
    cat $EXP_DIR/$SAMPLE/presto/${SAMPLE}-PRE-R1.fastq
done > $OUTPUT_DIR/JOIN-R1.fastq &

for SAMPLE in $SAMPLES; do
    cat $EXP_DIR/$SAMPLE/presto/${SAMPLE}-PRE-R2.fastq
done > $OUTPUT_DIR/JOIN-R2.fastq &


# #SplitSeq.py sample, sample 50k sequences randomly from the concatenated fastq file
SplitSeq.py sample \
-s $OUTPUT_DIR/JOIN.fastq \
-n 50000 \
--outname SPLIT-SAMPLE --outdir $OUTPUT_DIR \
>> $PIPELINE_LOG \
2> $ERROR_LOG

#EstimateErrorTotal.py: estimate the sequence error rate in the first 50 base pairs as a proxy for the UID error rate
EstimateErrorTotal.py \
-s $OUTPUT_DIR/SPLIT-SAMPLE_sample1-n50000.fastq \
-f BARCODE \
-len 50 \
--dtn \
--outname JOIN-ERR-uid --outdir $OUTPUT_DIR \
>> $PIPELINE_LOG \
2> $ERROR_LOG


# #OUTPUT_DIR=/home/ruoyi/kleinstein/abseq/oconnor_myositis/presto/uidel_std/RQ2410_A7VEF_2014-04-2/.indexing_uid/presto
# #Extract the threshold hamming distance for collapsing UIDs (average linkage) from EET
UID_THRESHOLD=$(cat $OUTPUT_DIR/JOIN-ERR-uid_dist-error.tab | grep 'UID_THRESH_95' | awk '{print $2}')
AWK_EXPR_UID="BEGIN {print (17-$UID_THRESHOLD)/17}"
UID_THRESHOLD_PERCENT=$(awk "$AWK_EXPR_UID")

#ClusterSingle.py: cluster the UIDs across all samples for similar UIDs (3 hrs)
ClusterDirty.py  barcode \
-s $OUTPUT_DIR/JOIN.fastq \
-f BARCODE \
-k INDEX_UID \
--ident $UID_THRESHOLD_PERCENT \
--outname JOIN-uid --outdir $OUTPUT_DIR \
--nproc 1 \
>> $PIPELINE_LOG \
2> $ERROR_LOG





#SplitSeq.py sample, sample 50k sequences randomly from the concatenated fastq file
SplitSeq.py sample \
-s $OUTPUT_DIR/JOIN-uid_cluster-pass.fastq \
-n 50000 \
--outname SPLIT-SAMPLE-seq --outdir $OUTPUT_DIR \
>> $PIPELINE_LOG \
2> $ERROR_LOG

#EstimateErrorTotal.py: estimate the sequence error rate in the first 50 base pairs as a proxy for the sequence error rate
EstimateErrorTotal.py \
-s $OUTPUT_DIR/SPLIT-SAMPLE-seq_sample1-n50000.fastq \
-f BARCODE \
-len 500 \
--dtn \
--outname JOIN-ERR-seq --outdir $OUTPUT_DIR \
>> $PIPELINE_LOG \
2> $ERROR_LOG

#Extract the threshold hamming distance for collapsing sequences (average linkage) from EET
# SEQ_THRESHOLD=$(cat $OUTPUT_DIR/JOIN-ERR-seq_dist-error.tab | grep 'SEQ_THRESH' | grep -v 'SEQ_THRESH'_95 | awk '{print $2}')
# AWK_EXPR_SEQ="BEGIN {print (500-$SEQ_THRESHOLD)/500}"
#SEQ_THRESHOLD_PERCENT=$(awk "$AWK_EXPR_SEQ")
SEQ_THRESHOLD_PERCENT=0.70

#ClusterSets.py: cluster the sequences within the UID groups across all samples (3 hrs)
ClusterSets.py set \
-s $OUTPUT_DIR/JOIN-uid_cluster-pass.fastq \
-f INDEX_UID \
-k INDEX_SEQ \
--ident $SEQ_THRESHOLD_PERCENT \
--outname JOIN-seq --outdir $OUTPUT_DIR \
>> $PIPELINE_LOG \
2> $ERROR_LOG


#ParseHeaders.py copy: collapse the Cluster_UID and Cluster_SEQ to create a final "true" barcode, CLUSTER_UID+CLUSTER_SEQ=CLUSTER_SEQ corresponding to individual mRNA molecules
ParseHeaders.py copy \
-s $OUTPUT_DIR/JOIN-seq_cluster-pass.fastq \
-f INDEX_UID \
-k INDEX_SEQ \
--outname JOIN-seq --outdir $OUTPUT_DIR \
>> $PIPELINE_LOG \
2> $ERROR_LOG

#Resolve indexing collisions
UnifyHeaders.py consensus \
-s $OUTPUT_DIR/JOIN-seq_reheader.fastq \
-f INDEX_SEQ \
-k SAMPLE \
--outname JOIN --outdir $OUTPUT_DIR \
--log $LOG_DIR/UnifyHeaders.log \
>> $PIPELINE_LOG \
2> $ERROR_LOG

# # #ConvertHeaders.py: convert headers of read1, read2 files to prepare for PairSeq to transfer over the new barcode CLUSTER_SEQ
# # ConvertHeaders.py illumina \
# # -s $OUTPUT_DIR/JOIN-R1.fastq \
# # --outname "JOIN-R1-COORD" --outdir $OUTPUT_DIR \
# # >> $PIPELINE_LOG \
# # 2> $ERROR_LOG
#
# # ConvertHeaders.py illumina \
# # -s $OUTPUT_DIR/JOIN-R2.fastq \
# # --outname "JOIN-R2-COORD" --outdir $OUTPUT_DIR \
# # >> $PIPELINE_LOG \
# # 2> $ERROR_LOG

SplitSeq.py group \
-s $OUTPUT_DIR/JOIN_unify-pass.fastq \
-f SAMPLE \
--outname INDEX-ASSEMBLE --outdir $OUTPUT_DIR \
>> $PIPELINE_LOG \
2> $ERROR_LOG

for SAMPLE in $SAMPLES; do
    PairSeq.py \
    -1 $OUTPUT_DIR/JOIN-R1.fastq \
    -2 $OUTPUT_DIR/INDEX-ASSEMBLE_SAMPLE-${SAMPLE}.fastq \
    --coord presto \
    --outname ${SAMPLE}-INDEX-R1 \
    --outdir $OUTPUT_DIR \
    >> $PIPELINE_LOG \
    2> $ERROR_LOG
    
    rm $OUTPUT_DIR/${SAMPLE}-INDEX-R1-2_pair-pass.fastq
done

for SAMPLE in $SAMPLES; do
    PairSeq.py \
    -1 $OUTPUT_DIR/JOIN-R2.fastq \
    -2 $OUTPUT_DIR/INDEX-ASSEMBLE_SAMPLE-${SAMPLE}.fastq \
    --coord presto \
    --outname ${SAMPLE}-INDEX-R2 \
    --outdir $OUTPUT_DIR \
    >> $PIPELINE_LOG \
    2> $ERROR_LOG
    
    rm $OUTPUT_DIR/${SAMPLE}-INDEX-R2-2_pair-pass.fastq
done

#Move and rename the resulting output files 
for SAMPLE in $SAMPLES; do
    FILE=$(ls $OUTPUT_DIR | grep $SAMPLE | grep INDEX-ASSEMBLE)
    mv $OUTPUT_DIR/$FILE $EXP_DIR/$SAMPLE/presto/$SAMPLE-INDEX.fastq
    FILE_R1=$(ls $OUTPUT_DIR | grep $SAMPLE | grep INDEX-R1)
    mv $OUTPUT_DIR/$FILE_R1 $EXP_DIR/$SAMPLE/presto/$SAMPLE-INDEX-R1.fastq
    FILE_R2=$(ls $OUTPUT_DIR | grep $SAMPLE | grep INDEX-R2)
    mv $OUTPUT_DIR/$FILE_R2 $EXP_DIR/$SAMPLE/presto/$SAMPLE-INDEX-R2.fastq
done


