#!/bin/bash

# Bash script for CONS pipeline for ART output v2 with threshold correction
# Ruoyi Jiang
# 6/18/18


#INPUT
THRESHOLD=$1
RAW_DIR_ORIGINAL=$2
PRESTO_DIR_ORIGINAL=$3

#REFERENCE
#REFERENCE_DIR=/ysm-gpfs/pi/kleinstein/oconnor_abseq/reference
REFERENCE_DIR=/home/ruoyi/kleinstein/abseq/reference

PRIMER_DIR="${REFERENCE_DIR}"/primers
PRIMERS1="${PRIMER_DIR}"/AbSeqV3_Human_R1CPrimers.fasta
PRIMERS2="${PRIMER_DIR}"/AbSeqV3_Human_R2TSPrimers.fasta
CPRIMERS="${PRIMER_DIR}"/AbSeqV3_Human_InternalCRegion.fasta
REF_FILE="${REFERENCE_DIR}"/germlines/IMGT_Human_IGV_ungapped_2017-01-29.fasta

mkdir -p $PRESTO_DIR_ORIGINAL

SAMPLE_IDS=$(ls ${RAW_DIR_ORIGINAL})



####
# Pre-Indexing 
####

for SAMPLE_ID in $SAMPLE_IDS; do
    
    RAW_DIR=${RAW_DIR_ORIGINAL}/${SAMPLE_ID}
    PRESTO_DIR=${PRESTO_DIR_ORIGINAL}/${SAMPLE_ID}
    
    mkdir -p ${PRESTO_DIR}
    
    echo "Pre-Index Correction: ${SAMPLE_ID}"
    
    #Set starting raw files
    FILE1="${RAW_DIR}"/$(ls ${RAW_DIR} | grep "R1")
    FILE2="${RAW_DIR}"/$(ls ${RAW_DIR} | grep "R2")

    #Set output directories
    OUTPUT_DIR="${PRESTO_DIR}"/presto
    LOG_DIR="${PRESTO_DIR}"/log
    TABLE_DIR="${PRESTO_DIR}"/table

    mkdir -p $OUTPUT_DIR
    mkdir -p $LOG_DIR
    mkdir -p $TABLE_DIR

    PIPELINE_LOG="${PRESTO_DIR}"/"${SAMPLE_ID}"-pipeline.log
    ERROR_LOG="${PRESTO_DIR}"/"${SAMPLE_ID}"-error.log


    #Start logs
    date > "${PIPELINE_LOG}"
    date > "${ERROR_LOG}"




    #FilterSeq.py quality: remove sequences that have average phred scores below q (20)
    FilterSeq.py quality \
    -s "${FILE1}" \
    -q 20 \
    --log "${LOG_DIR}"/"${SAMPLE_ID}"-R1_FilterSeq.log \
    --outname "${SAMPLE_ID}"-R1 --outdir "${OUTPUT_DIR}" \
    >> "${PIPELINE_LOG}" \
    2> "${ERROR_LOG}"

    FilterSeq.py quality \
    -s "${FILE2}" \
    -q 20 \
    --log "${LOG_DIR}"/"${SAMPLE_ID}"-R2_FilterSeq.log \
    --outname "${SAMPLE_ID}"-R2 --outdir "${OUTPUT_DIR}" \
    >> "${PIPELINE_LOG}" \
    2> "${ERROR_LOG}"


    #MaskPrimers.py score --mode cut: identify and cut out primers
    MaskPrimers.py score \
    -s "${OUTPUT_DIR}"/"${SAMPLE_ID}"-R1_quality-pass.fastq \
    -p "${PRIMERS1}" \
    --mode cut \
    --start 0 \
    --maxerror 0.2 \
    --log "${LOG_DIR}"/"${SAMPLE_ID}"-R1_MaskPrimers.log \
    --outname "${SAMPLE_ID}"-R1 --outdir "${OUTPUT_DIR}" \
    >> "${PIPELINE_LOG}" \
    2> "${ERROR_LOG}"

    MaskPrimers.py score \
    -s "${OUTPUT_DIR}"/"${SAMPLE_ID}"-R2_quality-pass.fastq \
    -p "${PRIMERS2}" \
    --mode cut \
    --start 17 \
    --barcode \
    --maxerror 0.5 \
    --log "${LOG_DIR}"/"${SAMPLE_ID}"-R2_MaskPrimers.log \
    --outname "${SAMPLE_ID}"-R2 --outdir "${OUTPUT_DIR}" \
    >> "${PIPELINE_LOG}" \
    2> "${ERROR_LOG}"

    #PairSeq.py: pair the reads by presto coord (because ART)
    PairSeq.py \
    -1 "${OUTPUT_DIR}"/"${SAMPLE_ID}"-R1_primers-pass.fastq \
    -2 "${OUTPUT_DIR}"/"${SAMPLE_ID}"-R2_primers-pass.fastq \
    --2f BARCODE \
    --coord presto \
    --outdir "${OUTPUT_DIR}" \
    >> "${PIPELINE_LOG}" \
    2> "${ERROR_LOG}"
    
    #AssemblePairs.py join: Concatenate the sequence pairs end-2-end to EstimateError
    AssemblePairs.py join \
    -1 "${OUTPUT_DIR}"/"${SAMPLE_ID}"-R1_primers-pass_pair-pass.fastq \
    -2 "${OUTPUT_DIR}"/"${SAMPLE_ID}"-R2_primers-pass_pair-pass.fastq \
    --1f PRIMER \
    --2f BARCODE \
    --coord presto \
    --log "${LOG_DIR}"/"${SAMPLE_ID}"-AssemblePairsJoin.log \
    --outname "${SAMPLE_ID}"-INDEX \
    --outdir "${OUTPUT_DIR}" \
    >> "${PIPELINE_LOG}" \
    2> "${ERROR_LOG}"
    
    #Add the sample identity to the outputs
    ParseHeaders.py add \
    -s "${OUTPUT_DIR}"/"${SAMPLE_ID}"-INDEX_assemble-pass.fastq \
    -f SAMPLE \
    -u ${SAMPLE_ID} \
    --outname "${SAMPLE_ID}"-INDEX --outdir "${OUTPUT_DIR}" \
    >> "${PIPELINE_LOG}" \
    2> "${ERROR_LOG}"

done


####
# Indexing 
####

echo "Indexing Correction"

mkdir -p ${PRESTO_DIR_ORIGINAL}/.consensus

#Set output directories
OUTPUT_DIR=${PRESTO_DIR_ORIGINAL}/.consensus/presto
LOG_DIR=${PRESTO_DIR_ORIGINAL}/.consensus/log
TABLE_DIR=${PRESTO_DIR_ORIGINAL}/.consensus/table

mkdir -p $OUTPUT_DIR
mkdir -p $LOG_DIR
mkdir -p $TABLE_DIR


PIPELINE_LOG=${PRESTO_DIR_ORIGINAL}/.consensus/JOIN-pipeline.log
ERROR_LOG=${PRESTO_DIR_ORIGINAL}/.consensus/JOIN-error.log

#Start logs
date > "${PIPELINE_LOG}"
date > "${ERROR_LOG}"


for SAMPLE_ID in $SAMPLE_IDS; do
    cat ${PRESTO_DIR_ORIGINAL}/${SAMPLE_ID}/presto/${SAMPLE_ID}-INDEX_reheader.fastq 
done > ${OUTPUT_DIR}/JOIN.fastq

for SAMPLE_ID in $SAMPLE_IDS; do
    cat ${PRESTO_DIR_ORIGINAL}/${SAMPLE_ID}/presto/${SAMPLE_ID}-R1_primers-pass_pair-pass.fastq 
done > ${OUTPUT_DIR}/JOIN-R1.fastq

for SAMPLE_ID in $SAMPLE_IDS; do
    cat ${PRESTO_DIR_ORIGINAL}/${SAMPLE_ID}/presto/${SAMPLE_ID}-R2_primers-pass_pair-pass.fastq 
done > ${OUTPUT_DIR}/JOIN-R2.fastq

#EstimateError.py
EE_HOME=/home/ruoyi/kleinstein/hg/presto/bin
${EE_HOME}/EstimateError.py \
-s "${OUTPUT_DIR}"/JOIN.fastq \
-f BARCODE \
--outname JOIN --outdir "${OUTPUT_DIR}" \
>> "${PIPELINE_LOG}" \
2> "${ERROR_LOG}"

#ClusterSingle.py: cluster the UIDs across all samples for similar UIDs
UID_THRESHOLD_PERCENT=$THRESHOLD

ClusterDirty.py  barcode \
-s "${OUTPUT_DIR}"/JOIN.fastq \
-f BARCODE \
-k INDEX_UID \
--ident ${UID_THRESHOLD_PERCENT} \
--outname JOIN-uid --outdir ${OUTPUT_DIR} \
--nproc 1 \
>> "${PIPELINE_LOG}" \
2> "${ERROR_LOG}"


#ClusterSets.py: cluster the sequences within the UID groups across all samples
SEQ_THRESHOLD_PERCENT=$THRESHOLD

ClusterSets.py set \
-s "${OUTPUT_DIR}"/JOIN-uid_cluster-pass.fastq \
-f INDEX_UID \
-k INDEX_SEQ \
--ident ${SEQ_THRESHOLD_PERCENT} \
--outname JOIN-seq --outdir ${OUTPUT_DIR} \
>> "${PIPELINE_LOG}" \
2> "${ERROR_LOG}"


#ParseHeaders.py copy: collapse the INDEX_UID and INDEX_SEQ to create a final "true" barcode
ParseHeaders.py merge \
-s "${OUTPUT_DIR}"/JOIN-seq_cluster-pass.fastq \
-f INDEX_UID INDEX_SEQ \
-k INDEX_NEW \
--outname JOIN-seq --outdir "${OUTPUT_DIR}" \
>> "${PIPELINE_LOG}" \
2> "${ERROR_LOG}"


#Resolve collisions
UnifyHeaders.py consensus \
-s "${OUTPUT_DIR}"/JOIN-seq_reheader.fastq \
-f INDEX_NEW \
-k SAMPLE \
--outname JOIN --outdir "${OUTPUT_DIR}" \
--log "${LOG_DIR}"/UnifyHeaders.log \
>> "${PIPELINE_LOG}" \
2> "${ERROR_LOG}"


#SpletSeq.py group: divide into SAMPLEs
SplitSeq.py group \
-s "${OUTPUT_DIR}"/JOIN_unify-pass.fastq \
-f SAMPLE \
--outname JOIN --outdir "${OUTPUT_DIR}" \
>> "${PIPELINE_LOG}" \
2> "${ERROR_LOG}"


for SAMPLE_ID in $SAMPLE_IDS; do
    PairSeq.py \
    -1 $OUTPUT_DIR/JOIN-R1.fastq \
    -2 $OUTPUT_DIR/JOIN_SAMPLE-${SAMPLE_ID}.fastq \
    --2f INDEX_NEW \
    --coord presto \
    --outname ${SAMPLE_ID}-INDEX-R1 \
    --outdir $OUTPUT_DIR \
    >> $PIPELINE_LOG \
    2> $ERROR_LOG
    
    rm $OUTPUT_DIR/${SAMPLE_ID}-INDEX-R1-2_pair-pass.fastq
done

for SAMPLE_ID in $SAMPLE_IDS; do
    PairSeq.py \
    -1 $OUTPUT_DIR/JOIN-R2.fastq \
    -2 $OUTPUT_DIR/JOIN_SAMPLE-${SAMPLE_ID}.fastq \
    --2f INDEX_NEW \
    --coord presto \
    --outname ${SAMPLE_ID}-INDEX-R2 \
    --outdir $OUTPUT_DIR \
    >> $PIPELINE_LOG \
    2> $ERROR_LOG
    
    rm $OUTPUT_DIR/${SAMPLE_ID}-INDEX-R2-2_pair-pass.fastq
done

#Move and rename the resulting output files 
for SAMPLE_ID in $SAMPLE_IDS; do
    FILE=JOIN_SAMPLE-${SAMPLE_ID}.fastq
    mv $OUTPUT_DIR/$FILE ${PRESTO_DIR_ORIGINAL}/${SAMPLE_ID}/presto/${SAMPLE_ID}-INDEX.fastq
    FILE_R1=${SAMPLE_ID}-INDEX-R1-1_pair-pass.fastq
    mv $OUTPUT_DIR/$FILE_R1 ${PRESTO_DIR_ORIGINAL}/${SAMPLE_ID}/presto/${SAMPLE_ID}-INDEX-R1.fastq
    FILE_R2=${SAMPLE_ID}-INDEX-R2-1_pair-pass.fastq
    mv $OUTPUT_DIR/$FILE_R2 ${PRESTO_DIR_ORIGINAL}/${SAMPLE_ID}/presto/${SAMPLE_ID}-INDEX-R2.fastq
done






####
# Post-Indexing 
####


for SAMPLE_ID in $SAMPLE_IDS; do
    
    echo "Post-Index Correction: ${SAMPLE_ID}"
    
    RAW_DIR=${RAW_DIR_ORIGINAL}/${SAMPLE_ID}
    PRESTO_DIR=${PRESTO_DIR_ORIGINAL}/${SAMPLE_ID}
    
    #Set output directories
    OUTPUT_DIR="${PRESTO_DIR}"/presto
    LOG_DIR="${PRESTO_DIR}"/log
    TABLE_DIR="${PRESTO_DIR}"/table

    mkdir -p $OUTPUT_DIR
    mkdir -p $LOG_DIR
    mkdir -p $TABLE_DIR

    PIPELINE_LOG="${PRESTO_DIR}"/"${SAMPLE_ID}"-pipeline.log
    ERROR_LOG="${PRESTO_DIR}"/"${SAMPLE_ID}"-error.log
    

    BuildConsensus.py \
    -s "${OUTPUT_DIR}"/${SAMPLE_ID}-INDEX-R1.fastq \
    --bf INDEX_NEW \
    --pf PRIMER \
    -q 0 \
    --maxgap 0.5 \
    --prcons 0.6 \
    --log "${LOG_DIR}"/"${SAMPLE_ID}"-R1_BuildConsensus.log \
    --outname ${SAMPLE_ID}-R1 --outdir "${OUTPUT_DIR}" \
    >> $PIPELINE_LOG \
    2> $ERROR_LOG

    BuildConsensus.py \
    -s "${OUTPUT_DIR}"/${SAMPLE_ID}-INDEX-R2.fastq \
    --bf INDEX_NEW \
    -q 0 \
    --maxgap 0.5 \
    --log "${LOG_DIR}"/"${SAMPLE_ID}"-R2_BuildConsensus.log \
    --outname ${SAMPLE_ID}-R2 --outdir "${OUTPUT_DIR}" \
    >> $PIPELINE_LOG \
    2> $ERROR_LOG

    #PairSeq.py: re-pair the reads after consensus building 
    PairSeq.py \
    -1 "${OUTPUT_DIR}"/${SAMPLE_ID}-R1_consensus-pass.fastq \
    -2 "${OUTPUT_DIR}"/${SAMPLE_ID}-R2_consensus-pass.fastq \
    --coord presto \
    --outname "${SAMPLE_ID}"-consensus --outdir "${OUTPUT_DIR}" \
    >> $PIPELINE_LOG \
    2> $ERROR_LOG






    #AssemblePairs.py align
    AP_ALN_SCANREV=true
    AP_ALN_MAXERR=0.3
    AP_ALN_MINLEN=8
    AP_ALN_ALPHA=1e-5

    AssemblePairs.py align \
    -1 "${OUTPUT_DIR}"/${SAMPLE_ID}-consensus-2_pair-pass.fastq \
    -2 "${OUTPUT_DIR}"/${SAMPLE_ID}-consensus-1_pair-pass.fastq \
    --1f PRCONS CONSCOUNT \
    --2f CONSCOUNT \
    --coord presto \
    --rc tail \
    --minlen ${AP_ALN_MINLEN} \
    --maxerror ${AP_ALN_MAXERR} \
    --alpha ${AP_ALN_ALPHA} \
    --scanrev \
    --failed \
    --log "${LOG_DIR}"/"${SAMPLE_ID}"_AssemblePairsAlign.log \
    --outname "${SAMPLE_ID}"-ALN --outdir "${OUTPUT_DIR}" \
    >> $PIPELINE_LOG \
    2> $ERROR_LOG

    #AssemblePairs.py reference
    AP_REF_MINIDENT=0.5
    AP_REF_EVALUE=1e-5
    AP_REF_MAXHITS=100

    AssemblePairs.py reference \
    -1 "${OUTPUT_DIR}"/${SAMPLE_ID}-ALN-1_assemble-fail.fastq \
    -2 "${OUTPUT_DIR}"/${SAMPLE_ID}-ALN-2_assemble-fail.fastq \
    -r ${REF_FILE} \
    --1f PRCONS CONSCOUNT \
    --2f CONSCOUNT \
    --coord presto \
    --rc tail \
    --minident ${AP_REF_MINIDENT} \
    --evalue ${AP_REF_EVALUE} \
    --maxhits ${AP_REF_MAXHITS} \
    --exec blastn \
    --dbexec makeblastdb \
    --aligner blastn \
    --failed \
    --log "${LOG_DIR}"/"${SAMPLE_ID}"_AssemblePairsReference.log \
    --outname "${SAMPLE_ID}"-REF --outdir "${OUTPUT_DIR}" \
    >> $PIPELINE_LOG \
    2> $ERROR_LOG



    if [ "$(ls -A "${OUTPUT_DIR}"/${SAMPLE_ID}-REF_assemble-pass.fastq)" ]; then
        cat $OUTPUT_DIR/$SAMPLE_ID-ALN_assemble-pass.fastq $OUTPUT_DIR/$SAMPLE_ID-REF_assemble-pass.fastq > $OUTPUT_DIR/$SAMPLE_ID-CAT_assemble-pass.fastq
    else 
        cp $OUTPUT_DIR/$SAMPLE_ID-ALN_assemble-pass.fastq $OUTPUT_DIR/$SAMPLE_ID-CAT_assemble-pass.fastq
    fi


    #MaskPrimers.py align: align C-region primers
    MaskPrimers.py align \
    -s "${OUTPUT_DIR}"/"${SAMPLE_ID}"-CAT_assemble-pass.fastq \
    -p ${CPRIMERS} \
    --maxlen 100 \
    --maxerror 0.4 \
    --mode tag \
    --revpr \
    --skiprc \
    --failed \
    --log "${LOG_DIR}"/"${SAMPLE_ID}"_MaskPrimersCRegion.log \
    --outname "${SAMPLE_ID}"-CR --outdir "${OUTPUT_DIR}" \
    >> $PIPELINE_LOG \
    2> $ERROR_LOG

    #ParseHeaders.py rename: PRIMER -> CREGION renaming
    ParseHeaders.py rename \
    -s "${OUTPUT_DIR}"/"${SAMPLE_ID}"-CR_primers-pass.fastq \
    -f PRIMER \
    -k CREGION \
    --outname "${SAMPLE_ID}"-CR --outdir "${OUTPUT_DIR}" \
    >> $PIPELINE_LOG \
    2> $ERROR_LOG	



    #ParseHeaders.py collapse: return the minimum conscount
    ParseHeaders.py collapse \
    -s "${OUTPUT_DIR}"/"${SAMPLE_ID}"-CR_reheader.fastq \
    -f CONSCOUNT \
    --act min \
    --outname "${SAMPLE_ID}"-FIN --outdir "${OUTPUT_DIR}" \
    >> $PIPELINE_LOG \
    2> $ERROR_LOG	

    #CollapseSeq.py: create DUPCOUNT
    CollapseSeq.py \
    -s "${OUTPUT_DIR}"/"${SAMPLE_ID}"-FIN_reheader.fastq \
    -n 0 \
    --uf PRCONS CREGION \
    --cf CONSCOUNT \
    --act sum \
    --inner \
    --keepmiss \
    --outname "${SAMPLE_ID}"-FIN --outdir "${OUTPUT_DIR}" \
    >> $PIPELINE_LOG \
    2> $ERROR_LOG	

    #SplitSeq.py group: only report those sequences with CONSCOUNT greater than 2
    SplitSeq.py group \
    -s "${OUTPUT_DIR}"/"${SAMPLE_ID}"-FIN_collapse-unique.fastq \
    -f CONSCOUNT \
    --num 2 \
    >> $PIPELINE_LOG \
    2> $ERROR_LOG	


    #ParseHeaders.py: converts outputs to logs
    ParseHeaders.py table \
    -s "$OUTPUT_DIR/$SAMPLE_ID"-FIN_reheader.fastq \
    -f ID PRCONS CREGION CONSCOUNT \
    --outname "${SAMPLE_ID}"-Final --outdir "${TABLE_DIR}" \
    >> "${PIPELINE_LOG}" \
    2> "${ERROR_LOG}"

    ParseHeaders.py table \
    -s "$OUTPUT_DIR/$SAMPLE_ID"-FIN_collapse-unique.fastq \
    -f ID PRCONS CREGION CONSCOUNT DUPCOUNT \
    --outname "${SAMPLE_ID}"-Final-Unique --outdir "${TABLE_DIR}" \
    >> "${PIPELINE_LOG}" \
    2> "${ERROR_LOG}"

    ParseHeaders.py table \
    -s "$OUTPUT_DIR/$SAMPLE_ID"-FIN_collapse-unique_atleast-2.fastq \
    -f ID PRCONS CREGION CONSCOUNT DUPCOUNT \
    --outname "${SAMPLE_ID}"-Final-Unique-Atleast2 --outdir "${TABLE_DIR}" \
    >> "${PIPELINE_LOG}" \
    2> "${ERROR_LOG}"

    #No need for pipeline log checking
    ParseLog.py -l "${LOG_DIR}"/"${SAMPLE_ID}"-R[1-2]_FilterSeq.log -f ID QUALITY --outdir $TABLE_DIR  >> "${PIPELINE_LOG}" 2> "${ERROR_LOG}"
    ParseLog.py -l "${LOG_DIR}"/"${SAMPLE_ID}"-R[1-2]_MaskPrimers.log -f ID BARCODE PRIMER ERROR --outdir $TABLE_DIR >> "${PIPELINE_LOG}" 2> "${ERROR_LOG}"
    ParseLog.py -l "${LOG_DIR}"/"${SAMPLE_ID}"-R[1-2]_BuildConsensus.log -f BARCODE SEQCOUNT CONSCOUNT PRIMER PRCONS PRCOUNT PRFREQ ERROR --outdir $TABLE_DIR >> "${PIPELINE_LOG}" 2> "${ERROR_LOG}"
    ParseLog.py -l "${LOG_DIR}"/"${SAMPLE_ID}"_AssemblePairsAlign.log -f ID LENGTH OVERLAP ERROR PVALUE FIELDS1 FIELDS2 --outdir $TABLE_DIR >> "${PIPELINE_LOG}" 2> "${ERROR_LOG}"
    ParseLog.py -l "${LOG_DIR}"/"${SAMPLE_ID}"_AssemblePairsReference.log -f ID REFID LENGTH OVERLAP GAP EVALUE1 EVALUE2 IDENTITY FIELDS1 FIELDS2 --outdir $TABLE_DIR >> "${PIPELINE_LOG}" 2> "${ERROR_LOG}"
    ParseLog.py -l "${LOG_DIR}"/"${SAMPLE_ID}"_MaskPrimersCRegion.log -f ID PRIMER ERROR --outdir $TABLE_DIR >> "${PIPELINE_LOG}" 2> "${ERROR_LOG}"
done



