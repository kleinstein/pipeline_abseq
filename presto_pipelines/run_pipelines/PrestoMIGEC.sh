#!/bin/bash

# Bash script for MIGEC pipeline prior to indexing correction for ART output v2
# Ruoyi Jiang
# 6/16/18


#INPUT
RAW_DIR_ORIGINAL=$1
PRESTO_DIR_ORIGINAL=$2


#REFERENCE
#REFERENCE_DIR=/ysm-gpfs/pi/kleinstein/oconnor_abseq/reference
REFERENCE_DIR=/home/ruoyi/kleinstein/abseq/reference

PRIMER_DIR="${REFERENCE_DIR}"/primers
PRIMERS1="${PRIMER_DIR}"/AbSeqV3_Human_R1CPrimers.fasta
PRIMERS2="${PRIMER_DIR}"/AbSeqV3_Human_R2TSPrimers.fasta
CPRIMERS="${PRIMER_DIR}"/AbSeqV3_Human_InternalCRegion.fasta
REF_FILE="${REFERENCE_DIR}"/germlines/IMGT_Human_IGV_ungapped_2017-01-29.fasta

#MIGEC SPECIFIC
MIGEC_BARCODES=$REFERENCE_DIR/migec_barcodes.txt

mkdir -p $PRESTO_DIR_ORIGINAL

SAMPLE_IDS=$(ls ${RAW_DIR_ORIGINAL})



####
# Migec
####

for SAMPLE_ID in $SAMPLE_IDS; do


    RAW_DIR=${RAW_DIR_ORIGINAL}/${SAMPLE_ID}
    PRESTO_DIR=${PRESTO_DIR_ORIGINAL}/${SAMPLE_ID}

    mkdir -p ${PRESTO_DIR}

    echo "Migec: ${SAMPLE_ID}"

    #Set starting raw files
    FILE1="${RAW_DIR}"/$(ls ${RAW_DIR} | grep "R1")
    FILE2="${RAW_DIR}"/$(ls ${RAW_DIR} | grep "R2")

    #Set output directories
    OUTPUT_DIR="${PRESTO_DIR}"/presto
    LOG_DIR="${PRESTO_DIR}"/log
    TABLE_DIR="${PRESTO_DIR}"/table

    mkdir -p $OUTPUT_DIR
    mkdir -p $LOG_DIR
    mkdir -p $TABLE_DIR

    PIPELINE_LOG="${PRESTO_DIR}"/"${SAMPLE_ID}"-pipeline.log
    ERROR_LOG="${PRESTO_DIR}"/"${SAMPLE_ID}"-error.log


    #Start logs
    date > "${PIPELINE_LOG}"
    date > "${ERROR_LOG}"



    #order of R1 and R2 is important, FILE2 must have the barcodes here and ends up getting reverse complemented
    migec Checkout \
    -cute \
    --overlap \
    --overlap-max-offset 400 \
    $MIGEC_BARCODES \
    $FILE2 \
    $FILE1 \
    $OUTPUT_DIR/checkout/ \
    >> $PIPELINE_LOG \
    2> $ERROR_LOG

    migec Histogram \
    $OUTPUT_DIR/checkout/ \
    $OUTPUT_DIR/histogram/ \
    >> $PIPELINE_LOG \
    2> $ERROR_LOG

    migec AssembleBatch \
    --force-overseq 2 \
    --force-collision-filter \
    $OUTPUT_DIR/checkout/ \
    $OUTPUT_DIR/histogram/ \
    $OUTPUT_DIR/assemble/ \
    >> $PIPELINE_LOG \
    2> $ERROR_LOG

    MaskPrimers.py score \
    -s ${OUTPUT_DIR}/assemble/BAR_R12.t2.cf.fastq \
    -p $PRIMERS2 \
    --mode cut \
    --start 0 \
    --maxerror 0.5 \
    --outname ${SAMPLE_ID}-R2 --outdir ${OUTPUT_DIR} \
    >> $PIPELINE_LOG \
    2> $ERROR_LOG

    MaskPrimers.py score \
    -s ${OUTPUT_DIR}/${SAMPLE_ID}-R2_primers-pass.fastq \
    -p $PRIMERS1 \
    --mode cut \
    --start 0 \
    --maxerror 0.2 \
    --revpr \
    --outname ${SAMPLE_ID}-R1 --outdir ${OUTPUT_DIR} \
    >> $PIPELINE_LOG \
    2> $ERROR_LOG

    ParseHeaders.py delete \
    -s $OUTPUT_DIR/${SAMPLE_ID}-R1_primers-pass.fastq \
    -f PRIMER \
    --outname $SAMPLE_ID-PR --outdir $OUTPUT_DIR \
    >> $PIPELINE_LOG \
    2> $ERROR_LOG

    #MaskPrimers.py align: align C-region primers
    MaskPrimers.py align \
    -s $OUTPUT_DIR/${SAMPLE_ID}-PR_reheader.fastq \
    -p $CPRIMERS \
    --maxlen 100 \
    --maxerror 0.4 \
    --mode tag \
    --revpr \
    --skiprc \
    --failed \
    --outname $SAMPLE_ID-CR --outdir $OUTPUT_DIR \
    >> $PIPELINE_LOG \
    2> $ERROR_LOG

    #ParseHeaders.py rename: PRIMER -> CREGION renaming
    ParseHeaders.py rename \
    -s $OUTPUT_DIR/${SAMPLE_ID}-CR_primers-pass.fastq \
    -f PRIMER \
    -k CREGION \
    --outname $SAMPLE_ID-CR --outdir $OUTPUT_DIR \
    >> $PIPELINE_LOG \
    2> $ERROR_LOG	

    cp $OUTPUT_DIR/${SAMPLE_ID}-CR_reheader.fastq $OUTPUT_DIR/${SAMPLE_ID}-FIN_collapse-unique_atleast-2.fastq
    cp $OUTPUT_DIR/${SAMPLE_ID}-CR_reheader.fastq $OUTPUT_DIR/${SAMPLE_ID}-FIN_collapse-unique.fastq

done



