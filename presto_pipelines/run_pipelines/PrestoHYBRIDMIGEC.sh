#!/bin/bash

# Bash script for MIGEC pipeline prior to indexing correction for ART output v2
# Ruoyi Jiang
# 6/18/18


#INPUT
RAW_DIR_ORIGINAL=$1
PRESTO_DIR_ORIGINAL=$2


#REFERENCE
#REFERENCE_DIR=/ysm-gpfs/pi/kleinstein/oconnor_abseq/reference
REFERENCE_DIR=/home/ruoyi/kleinstein/abseq/reference

PRIMER_DIR="${REFERENCE_DIR}"/primers
PRIMERS1="${PRIMER_DIR}"/AbSeqV3_Human_R1CPrimers.fasta
PRIMERS2="${PRIMER_DIR}"/AbSeqV3_Human_R2TSPrimers.fasta
CPRIMERS="${PRIMER_DIR}"/AbSeqV3_Human_InternalCRegion.fasta
REF_FILE="${REFERENCE_DIR}"/germlines/IMGT_Human_IGV_ungapped_2017-01-29.fasta

#MIGEC SPECIFIC
MIGEC_BARCODES=$REFERENCE_DIR/migec_barcodes.txt

mkdir -p $PRESTO_DIR_ORIGINAL

SAMPLE_IDS=$(ls ${RAW_DIR_ORIGINAL})



####
# Migec
####

for SAMPLE_ID in $SAMPLE_IDS; do
    
    RAW_DIR=${RAW_DIR_ORIGINAL}/${SAMPLE_ID}
    PRESTO_DIR=${PRESTO_DIR_ORIGINAL}/${SAMPLE_ID}
    
    mkdir -p ${PRESTO_DIR}
    
    echo "Migec: ${SAMPLE_ID}"
    
    #Set starting raw files
    FILE1="${RAW_DIR}"/$(ls ${RAW_DIR} | grep "R1")
    FILE2="${RAW_DIR}"/$(ls ${RAW_DIR} | grep "R2")
    
    #Set output directories
    OUTPUT_DIR="${PRESTO_DIR}"/presto
    LOG_DIR="${PRESTO_DIR}"/log
    TABLE_DIR="${PRESTO_DIR}"/table

    mkdir -p $OUTPUT_DIR
    mkdir -p $LOG_DIR
    mkdir -p $TABLE_DIR

    PIPELINE_LOG="${PRESTO_DIR}"/"${SAMPLE_ID}"-pipeline.log
    ERROR_LOG="${PRESTO_DIR}"/"${SAMPLE_ID}"-error.log


    #Start logs
    date > "${PIPELINE_LOG}"
    date > "${ERROR_LOG}"

    
  
    #Run MIGEC to generate barcode reads

    #order of R1 and R2 is important, FILE2 must have the barcodes here and ends up getting reverse complemented
    migec Checkout \
    -cute $MIGEC_BARCODES \
    $FILE2 \
    $FILE1 \
    $OUTPUT_DIR/checkout/ \
    >> $PIPELINE_LOG \
    2> $ERROR_LOG

    migec Histogram \
    $OUTPUT_DIR/checkout/ \
    $OUTPUT_DIR/histogram/ \
    >> $PIPELINE_LOG \
    2> $ERROR_LOG

    migec AssembleBatch \
    --force-overseq 2 \
    --force-collision-filter \
    $OUTPUT_DIR/checkout/ \
    $OUTPUT_DIR/histogram/ \
    $OUTPUT_DIR/assemble/ \
    >> $PIPELINE_LOG \
    2> $ERROR_LOG

    #Run script to convert the MIGEC headers to presto style headers
    #also R1 and R2 are reversed by MIGEC so you need to undo the reversal
    ConvertMIGEC.py \
    $OUTPUT_DIR/assemble/BAR_R1.t2.cf.fastq \
    $OUTPUT_DIR/${SAMPLE_ID}-R2-MIGEC.fastq \
    >> $PIPELINE_LOG \
    2> $ERROR_LOG

    ConvertMIGEC.py \
    $OUTPUT_DIR/assemble/BAR_R2.t2.cf.fastq \
    $OUTPUT_DIR/${SAMPLE_ID}-R1-MIGEC.fastq \
    >> $PIPELINE_LOG \
    2> $ERROR_LOG

    #Reverse complement
    ReverseComplement.py \
    $OUTPUT_DIR/${SAMPLE_ID}-R1-MIGEC.fastq \
    $OUTPUT_DIR/${SAMPLE_ID}-R1-MIGEC_flip.fastq \
    >> $PIPELINE_LOG \
    2> $ERROR_LOG



    #########PRESTO
    #At this point, R1 and R2 are flipped, need to reverse this during MP
    #Run presto maskprimers
    MaskPrimers.py score \
    -s $OUTPUT_DIR/${SAMPLE_ID}-R1-MIGEC_flip.fastq \
    -p $PRIMERS1 \
    --mode cut \
    --start 0 \
    --maxerror 0.2 \
    --log $LOG_DIR/DM1-R1_MaskPrimers.log \
    --outname $SAMPLE_ID-R1 --outdir $OUTPUT_DIR \
    >> $PIPELINE_LOG \
    2> $ERROR_LOG

    MaskPrimers.py score \
    -s $OUTPUT_DIR/${SAMPLE_ID}-R2-MIGEC.fastq \
    -p $PRIMERS2 \
    --mode cut \
    --start 0 \
    --maxerror 0.5 \
    --log $LOG_DIR/${SAMPLE_ID}-R2_MaskPrimers.log \
    --outname $SAMPLE_ID-R2 --outdir $OUTPUT_DIR \
    >> $PIPELINE_LOG \
    2> $ERROR_LOG

    PairSeq.py \
    -1 $OUTPUT_DIR/${SAMPLE_ID}-R1_primers-pass.fastq \
    -2 $OUTPUT_DIR/${SAMPLE_ID}-R2_primers-pass.fastq  \
    --coord presto \
    --outdir $OUTPUT_DIR \
    >> $PIPELINE_LOG \
    2> $ERROR_LOG

    #AssemblePairs.py align
    AP_ALN_SCANREV=true
    AP_ALN_MAXERR=0.3
    AP_ALN_MINLEN=8
    AP_ALN_ALPHA=1e-5

    AssemblePairs.py align \
    -1 $OUTPUT_DIR/${SAMPLE_ID}-R2_primers-pass_pair-pass.fastq \
    -2 $OUTPUT_DIR/${SAMPLE_ID}-R1_primers-pass_pair-pass.fastq \
    --coord presto \
    --rc tail \
    --minlen $AP_ALN_MINLEN \
    --maxerror $AP_ALN_MAXERR \
    --alpha $AP_ALN_ALPHA \
    --scanrev \
    --failed \
    --log $LOG_DIR/${SAMPLE_ID}_AssemblePairsAlign.log \
    --outname ${SAMPLE_ID}-ALN --outdir $OUTPUT_DIR \
    >> $PIPELINE_LOG \
    2> $ERROR_LOG

    #AssemblePairs.py reference
    AP_REF_MINIDENT=0.5
    AP_REF_EVALUE=1e-5
    AP_REF_MAXHITS=100

    AssemblePairs.py reference \
    -1 $OUTPUT_DIR/${SAMPLE_ID}-ALN-1_assemble-fail.fastq \
    -2 $OUTPUT_DIR/${SAMPLE_ID}-ALN-2_assemble-fail.fastq \
    -r $REF_FILE \
    --coord presto \
    --rc tail \
    --minident $AP_REF_MINIDENT \
    --evalue $AP_REF_EVALUE \
    --maxhits $AP_REF_MAXHITS \
    --exec blastn \
    --dbexec makeblastdb \
    --aligner blastn \
    --failed \
    --log $LOG_DIR/${SAMPLE_ID}_AssemblePairsReference.log \
    --outname $SAMPLE_ID-REF --outdir $OUTPUT_DIR \
    >> $PIPELINE_LOG \
    2> $ERROR_LOG

    #Concatenate the outputs from AssemblePairs.py align and reference
    cat $OUTPUT_DIR/$SAMPLE_ID-ALN_assemble-pass.fastq $OUTPUT_DIR/$SAMPLE_ID-REF_assemble-pass.fastq > \
        $OUTPUT_DIR/$SAMPLE_ID-CAT_assemble-pass.fastq

    #MaskPrimers.py align: align C-region primers
    MaskPrimers.py align \
    -s $OUTPUT_DIR/${SAMPLE_ID}-CAT_assemble-pass.fastq \
    -p $CPRIMERS \
    --maxlen 100 \
    --maxerror 0.4 \
    --mode tag \
    --revpr \
    --skiprc \
    --failed \
    --log $LOG_DIR/${SAMPLE_ID}_MaskPrimersCRegion.log \
    --outname $SAMPLE_ID-CR --outdir $OUTPUT_DIR \
    >> $PIPELINE_LOG \
    2> $ERROR_LOG

    #ParseHeaders.py rename: PRIMER -> CREGION renaming
    ParseHeaders.py rename \
    -s $OUTPUT_DIR/${SAMPLE_ID}-CR_primers-pass.fastq \
    -f PRIMER \
    -k CREGION \
    --outname $SAMPLE_ID-CR --outdir $OUTPUT_DIR \
    >> $PIPELINE_LOG \
    2> $ERROR_LOG	


    #CollapseSeq.py: create DUPCOUNT
    CollapseSeq.py \
    -s $OUTPUT_DIR/${SAMPLE_ID}-CR_reheader.fastq \
    -n 0 \
    --uf PRIMER \
    --inner \
    --keepmiss \
    --outname $SAMPLE_ID-FIN --outdir $OUTPUT_DIR \
    >> $PIPELINE_LOG \
    2> $ERROR_LOG	

    #Create a unique_atleast-2 analogue
    cp $OUTPUT_DIR/${SAMPLE_ID}-FIN_collapse-unique.fastq $OUTPUT_DIR/${SAMPLE_ID}-FIN_collapse-unique_atleast-2.fastq

    ###PARSE LOGS#### 
    #No need for pipeline log checking

    ParseHeaders.py table \
    -s $OUTPUT_DIR/${SAMPLE_ID}-FIN_collapse-unique.fastq \
    -f ID PRCONS CREGION CONSCOUNT DUPCOUNT \
    --outname $SAMPLE_ID-Final-Unique --outdir $TABLE_DIR \
    >> $PIPELINE_LOG \
    2> $ERROR_LOG	

done



