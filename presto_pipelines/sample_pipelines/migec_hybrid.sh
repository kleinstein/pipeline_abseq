#!/bin/bash

# Bash script for combining MIGEC with presto to assemble full length reads
# Ruoyi Jiang
# 7/18/17

#REFERENCES
REFERENCE_DIR=/ysm-gpfs/pi/kleinstein/oconnor_abseq/reference

PRIMER_DIR=$REFERENCE_DIR/primers
PRIMERS1=$PRIMER_DIR/AbSeqV3_Human_R1CPrimers.fasta
PRIMERS2=$PRIMER_DIR/AbSeqV3_Human_R2TSPrimers.fasta
CPRIMERS=$PRIMER_DIR/AbSeqV3_Human_InternalCRegion.fasta
REF_FILE=$REFERENCE_DIR/germlines/IMGT_Human_IGV_ungapped_2017-01-29.fasta

#MIGEC SPECIFIC
MIGEC_BARCODES=$REFERENCE_DIR/migec_barcodes.txt


#SPECIFIC RUN
RAW_DIR=$1
RUN_DIR=$2

SAMPLE=$(basename ${RAW_DIR})
PRESTO_DIR=${RUN_DIR}/${SAMPLE}

#Set starting raw files
FILE1=$RAW_DIR/$(ls $RAW_DIR | grep "R1")
FILE2=$RAW_DIR/$(ls $RAW_DIR | grep "R2")

#Set output directories
OUTPUT_DIR=${PRESTO_DIR}/presto
LOG_DIR=${PRESTO_DIR}/log
TABLE_DIR=${PRESTO_DIR}/table
FINAL_DIR=${PRESTO_DIR}/output

mkdir -p ${PRESTO_DIR}
mkdir -p ${OUTPUT_DIR}
mkdir -p ${LOG_DIR}
mkdir -p ${TABLE_DIR}
mkdir -p ${FINAL_DIR}

PIPELINE_LOG=${PRESTO_DIR}/${SAMPLE}-pipeline.log
ERROR_LOG=${PRESTO_DIR}/${SAMPLE}-error.log

#Start logs
date > ${PIPELINE_LOG}
date > ${ERROR_LOG}



#Run MIGEC to generate barcode reads

#order of R1 and R2 is important, FILE2 must have the barcodes here and ends up getting reverse complemented
migec Checkout \
-cute $MIGEC_BARCODES \
$FILE2 \
$FILE1 \
$OUTPUT_DIR/checkout/

migec Histogram \
$OUTPUT_DIR/checkout/ \
$OUTPUT_DIR/histogram/

migec AssembleBatch \
--force-overseq 2 \
--force-collision-filter \
$OUTPUT_DIR/checkout/ \
$OUTPUT_DIR/histogram/ \
$OUTPUT_DIR/assemble/

#Run script to convert the MIGEC headers to presto style headers
#also R1 and R2 are reversed by MIGEC so you need to undo the reversal
ConvertMIGEC.py \
$OUTPUT_DIR/assemble/BAR_R1.t2.cf.fastq \
$OUTPUT_DIR/${SAMPLE}-R2-MIGEC.fastq 

ConvertMIGEC.py \
$OUTPUT_DIR/assemble/BAR_R2.t2.cf.fastq \
$OUTPUT_DIR/${SAMPLE}-R1-MIGEC.fastq 

#Reverse complement
ReverseComplement.py \
$OUTPUT_DIR/${SAMPLE}-R1-MIGEC.fastq \
$OUTPUT_DIR/${SAMPLE}-R1-MIGEC_flip.fastq 



#########PRESTO
#At this point, R1 and R2 are flipped, need to reverse this during MP
#Run presto maskprimers
MaskPrimers.py score \
-s $OUTPUT_DIR/${SAMPLE}-R1-MIGEC_flip.fastq \
-p $PRIMERS1 \
--mode cut \
--start 0 \
--maxerror 0.2 \
--log $LOG_DIR/DM1-R1_MaskPrimers.log \
--outname $SAMPLE-R1 --outdir $OUTPUT_DIR \
>> $PIPELINE_LOG \
2> $ERROR_LOG

MaskPrimers.py score \
-s $OUTPUT_DIR/${SAMPLE}-R2-MIGEC.fastq \
-p $PRIMERS2 \
--mode cut \
--start 0 \
--maxerror 0.5 \
--log $LOG_DIR/${SAMPLE}-R2_MaskPrimers.log \
--outname $SAMPLE-R2 --outdir $OUTPUT_DIR \
>> $PIPELINE_LOG \
2> $ERROR_LOG

PairSeq.py \
-1 $OUTPUT_DIR/${SAMPLE}-R1_primers-pass.fastq \
-2 $OUTPUT_DIR/${SAMPLE}-R2_primers-pass.fastq  \
--coord presto \
--outdir $OUTPUT_DIR \
>> $PIPELINE_LOG \
2> $ERROR_LOG

#AssemblePairs.py align
AP_ALN_SCANREV=true
AP_ALN_MAXERR=0.3
AP_ALN_MINLEN=8
AP_ALN_ALPHA=1e-5

AssemblePairs.py align \
-1 $OUTPUT_DIR/${SAMPLE}-R2_primers-pass_pair-pass.fastq \
-2 $OUTPUT_DIR/${SAMPLE}-R1_primers-pass_pair-pass.fastq \
--coord presto \
--rc tail \
--minlen $AP_ALN_MINLEN \
--maxerror $AP_ALN_MAXERR \
--alpha $AP_ALN_ALPHA \
--scanrev \
--failed \
--log $LOG_DIR/${SAMPLE}_AssemblePairsAlign.log \
--outname ${SAMPLE}-ALN --outdir $OUTPUT_DIR \
>> $PIPELINE_LOG \
2> $ERROR_LOG

#AssemblePairs.py reference
AP_REF_MINIDENT=0.5
AP_REF_EVALUE=1e-5
AP_REF_MAXHITS=100

AssemblePairs.py reference \
-1 $OUTPUT_DIR/${SAMPLE}-ALN-1_assemble-fail.fastq \
-2 $OUTPUT_DIR/${SAMPLE}-ALN-2_assemble-fail.fastq \
-r $REF_FILE \
--coord presto \
--rc tail \
--minident $AP_REF_MINIDENT \
--evalue $AP_REF_EVALUE \
--maxhits $AP_REF_MAXHITS \
--exec blastn \
--dbexec makeblastdb \
--aligner blastn \
--failed \
--log $LOG_DIR/${SAMPLE}_AssemblePairsReference.log \
--outname $SAMPLE-REF --outdir $OUTPUT_DIR \
>> $PIPELINE_LOG \
2> $ERROR_LOG

#Concatenate the outputs from AssemblePairs.py align and reference
cat $OUTPUT_DIR/$SAMPLE-ALN_assemble-pass.fastq $OUTPUT_DIR/$SAMPLE-REF_assemble-pass.fastq > \
    $OUTPUT_DIR/$SAMPLE-CAT_assemble-pass.fastq

#MaskPrimers.py align: align C-region primers
MaskPrimers.py align \
-s $OUTPUT_DIR/${SAMPLE}-CAT_assemble-pass.fastq \
-p $CPRIMERS \
--maxlen 100 \
--maxerror 0.4 \
--mode tag \
--revpr \
--skiprc \
--failed \
--log $LOG_DIR/${SAMPLE}_MaskPrimersCRegion.log \
--outname $SAMPLE-CR --outdir $OUTPUT_DIR \
>> $PIPELINE_LOG \
2> $ERROR_LOG

#ParseHeaders.py rename: PRIMER -> CREGION renaming
ParseHeaders.py rename \
-s $OUTPUT_DIR/${SAMPLE}-CR_primers-pass.fastq \
-f PRIMER \
-k CREGION \
--outname $SAMPLE-CR --outdir $OUTPUT_DIR \
>> $PIPELINE_LOG \
2> $ERROR_LOG	


#CollapseSeq.py: create DUPCOUNT
CollapseSeq.py \
-s $OUTPUT_DIR/${SAMPLE}-CR_reheader.fastq \
-n 0 \
--uf PRIMER \
--inner \
--keepmiss \
--outname $SAMPLE-FIN --outdir $OUTPUT_DIR \
>> $PIPELINE_LOG \
2> $ERROR_LOG	

#Create a unique_atleast-2 analogue
cp $OUTPUT_DIR/${SAMPLE}-FIN_collapse-unique.fastq $OUTPUT_DIR/${SAMPLE}-FIN_collapse-unique_atleast-2.fastq

###PARSE LOGS#### 
#No need for pipeline log checking

ParseHeaders.py table \
-s $OUTPUT_DIR/${SAMPLE}-FIN_collapse-unique.fastq \
-f ID PRCONS CREGION CONSCOUNT DUPCOUNT \
--outname $SAMPLE-Final-Unique --outdir $TABLE_DIR \
>> $PIPELINE_LOG \
2> $ERROR_LOG	

#No need for pipeline log checking
ParseLog.py -l $LOG_DIR/${SAMPLE}_AssemblePairsAlign.log -f ID LENGTH OVERLAP ERROR PVALUE FIELDS1 FIELDS2 --outdir $TABLE_DIR 2> $ERROR_LOG
ParseLog.py -l $LOG_DIR/${SAMPLE}_AssemblePairsReference.log -f ID REFID LENGTH OVERLAP GAP EVALUE1 EVALUE2 IDENTITY FIELDS1 FIELDS2 --outdir $TABLE_DIR 2> $ERROR_LOG
ParseLog.py -l $LOG_DIR/${SAMPLE}_MaskPrimersCRegion.log -f ID PRIMER ERROR --outdir $TABLE_DIR 2> $ERROR_LOG

#FINAL_DIR selections
mv ${OUTPUT_DIR}/${SAMPLE}-FIN_collapse-unique_atleast-2.fastq ${FINAL_DIR}/${SAMPLE}-FIN_collapse-unique_atleast-2.fastq
