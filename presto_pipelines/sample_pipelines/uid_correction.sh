#!/bin/bash

# Bash script CD-HIT with automatic threshold identification on farnam
# Ruoyi Jiang
# 8/23/18


#REFERENCES
REFERENCE_DIR=/ysm-gpfs/pi/kleinstein/oconnor_abseq/reference

PRIMER_DIR=$REFERENCE_DIR/primers
PRIMERS1=$PRIMER_DIR/AbSeqV3_Human_R1CPrimers.fasta
PRIMERS2=$PRIMER_DIR/AbSeqV3_Human_R2TSPrimers.fasta
CPRIMERS=$PRIMER_DIR/AbSeqV3_Human_InternalCRegion.fasta
REF_FILE=$REFERENCE_DIR/germlines/IMGT_Human_IGV_ungapped_2017-01-29.fasta

#I/O
RAW_DIR=$1
RUN_DIR=$2

# RAW_DIR=/ysm-gpfs/pi/kleinstein/oconnor_abseq/oconnor_thymus/raw/RQ8359_TH03/G409
# RUN_DIR=/ysm-gpfs/pi/kleinstein/oconnor_abseq/oconnor_thymus/presto/uid_correction/RQ8359_TH03


SAMPLE=$(basename ${RAW_DIR})
PRESTO_DIR=${RUN_DIR}/${SAMPLE}

#Set starting raw files
FILE1=${RAW_DIR}/$(ls ${RAW_DIR} | grep "R1")
FILE2=${RAW_DIR}/$(ls ${RAW_DIR} | grep "R2")

#Set output directories
OUTPUT_DIR=${PRESTO_DIR}/presto
LOG_DIR=${PRESTO_DIR}/log
TABLE_DIR=${PRESTO_DIR}/table
FINAL_DIR=${PRESTO_DIR}/output

mkdir -p ${PRESTO_DIR}
mkdir -p ${OUTPUT_DIR}
mkdir -p ${LOG_DIR}
mkdir -p ${TABLE_DIR}
mkdir -p ${FINAL_DIR}

PIPELINE_LOG=${PRESTO_DIR}/${SAMPLE}-pipeline.log
ERROR_LOG=${PRESTO_DIR}/${SAMPLE}-error.log

#Start logs
date > ${PIPELINE_LOG}
date > ${ERROR_LOG}



# ####
# # Pre-Indexing 
# ####


#FilterSeq.py quality: remove sequences that have average phred scores below q (20)
FilterSeq.py quality \
-s "${FILE1}" \
-q 20 \
--log "${LOG_DIR}"/"${SAMPLE}"-R1_FilterSeq.log \
--outname "${SAMPLE}"-R1 --outdir "${OUTPUT_DIR}" \
>> "${PIPELINE_LOG}" \
2> "${ERROR_LOG}"

FilterSeq.py quality \
-s "${FILE2}" \
-q 20 \
--log "${LOG_DIR}"/"${SAMPLE}"-R2_FilterSeq.log \
--outname "${SAMPLE}"-R2 --outdir "${OUTPUT_DIR}" \
>> "${PIPELINE_LOG}" \
2> "${ERROR_LOG}"


#MaskPrimers.py score --mode cut: identify and cut out primers
MaskPrimers.py score \
-s "${OUTPUT_DIR}"/"${SAMPLE}"-R1_quality-pass.fastq \
-p "${PRIMERS1}" \
--mode cut \
--start 0 \
--maxerror 0.2 \
--log "${LOG_DIR}"/"${SAMPLE}"-R1_MaskPrimers.log \
--outname "${SAMPLE}"-R1 --outdir "${OUTPUT_DIR}" \
>> "${PIPELINE_LOG}" \
2> "${ERROR_LOG}"

MaskPrimers.py score \
-s "${OUTPUT_DIR}"/"${SAMPLE}"-R2_quality-pass.fastq \
-p "${PRIMERS2}" \
--mode cut \
--start 17 \
--barcode \
--maxerror 0.5 \
--log "${LOG_DIR}"/"${SAMPLE}"-R2_MaskPrimers.log \
--outname "${SAMPLE}"-R2 --outdir "${OUTPUT_DIR}" \
>> "${PIPELINE_LOG}" \
2> "${ERROR_LOG}"

#PairSeq.py: pair the reads by presto coord (because ART)
PairSeq.py \
-1 "${OUTPUT_DIR}"/"${SAMPLE}"-R1_primers-pass.fastq \
-2 "${OUTPUT_DIR}"/"${SAMPLE}"-R2_primers-pass.fastq \
--2f BARCODE \
--coord illumina \
--outdir "${OUTPUT_DIR}" \
>> "${PIPELINE_LOG}" \
2> "${ERROR_LOG}"







#AssemblePairs.py join: Concatenate the sequence pairs end-2-end to EstimateError
AssemblePairs.py join \
-1 "${OUTPUT_DIR}"/"${SAMPLE}"-R1_primers-pass_pair-pass.fastq \
-2 "${OUTPUT_DIR}"/"${SAMPLE}"-R2_primers-pass_pair-pass.fastq \
--1f PRIMER \
--2f BARCODE \
--coord illumina \
--log "${LOG_DIR}"/"${SAMPLE}"-AssemblePairsJoin.log \
--outname "${SAMPLE}"-INDEX \
--outdir "${OUTPUT_DIR}" \
>> "${PIPELINE_LOG}" \
2> "${ERROR_LOG}"

#Add the sample identity to the outputs
ParseHeaders.py add \
-s "${OUTPUT_DIR}"/"${SAMPLE}"-INDEX_assemble-pass.fastq \
-f SAMPLE \
-u ${SAMPLE} \
--outname "${SAMPLE}"-INDEX --outdir "${OUTPUT_DIR}" \
>> "${PIPELINE_LOG}" \
2> "${ERROR_LOG}"

####
# Indexing 
####


SplitSeq.py sample -s  "${OUTPUT_DIR}"/"${SAMPLE}"-INDEX_reheader.fastq \
-n 5000 --outname "${SAMPLE}" --outdir "${OUTPUT_DIR}" \
>> "${PIPELINE_LOG}" \
2> "${ERROR_LOG}"


EstimateError.py barcode \
-s "${OUTPUT_DIR}"/"${SAMPLE}"_sample1-n5000.fastq \
-f BARCODE \
--outname "${SAMPLE}" --outdir "${OUTPUT_DIR}" \
>> "${PIPELINE_LOG}" \
2> "${ERROR_LOG}"

#ClusterSets.py barcode: Cluster barcodes
TABLE="${OUTPUT_DIR}"/"${SAMPLE}"_thresh-barcode.tab
UID_THRESHOLD_PERCENT=`python3 << END
import pandas as pd
threshold=1 - pd.read_table("${TABLE}", index_col='TYPE')['THRESH']['ALL']
if threshold < 0.80:
    print(0.80)
else:
    print(threshold)
END`

ClusterSets.py barcode \
-s "${OUTPUT_DIR}"/${SAMPLE}-INDEX_reheader.fastq \
-f BARCODE \
-k INDEX_UID \
--ident ${UID_THRESHOLD_PERCENT} \
--cluster cd-hit-est \
--outname ${SAMPLE}-uid --outdir ${OUTPUT_DIR} \
--nproc 1 \
>> "${PIPELINE_LOG}" \
2> "${ERROR_LOG}"





#Sample from the uid-cluster file
SplitSeq.py sample \
-s "${OUTPUT_DIR}"/${SAMPLE}-uid_cluster-pass.fastq \
-n 10000 --outname ${SAMPLE} --outdir "${OUTPUT_DIR}" \
>> "${PIPELINE_LOG}" \
2> "${ERROR_LOG}"


#EstimateError.py
EstimateError.py set \
-s "${OUTPUT_DIR}"/${SAMPLE}_sample1-n10000.fastq \
-f INDEX_UID -n 2 \
--outname "${SAMPLE}" --outdir "${OUTPUT_DIR}" \
>> "${PIPELINE_LOG}" \
2> "${ERROR_LOG}"


TABLE="${OUTPUT_DIR}"/"${SAMPLE}"_thresh-set.tab

SEQ_THRESHOLD_PERCENT=`python3 << END
import pandas as pd
threshold=1 - pd.read_table("${TABLE}", index_col='TYPE')['THRESH']['ALL']
if threshold < 0.80:
    print(0.80)
else:
    print(threshold)
END`

ClusterSets.py set \
-s "${OUTPUT_DIR}"/${SAMPLE}-uid_cluster-pass.fastq \
-f INDEX_UID \
-k INDEX_SEQ \
--ident ${SEQ_THRESHOLD_PERCENT} \
--cluster cd-hit-est \
--outname ${SAMPLE}-seq --outdir ${OUTPUT_DIR} \
>> "${PIPELINE_LOG}" \
2> "${ERROR_LOG}"




#ParseHeaders.py copy: collapse the INDEX_UID and INDEX_SEQ to create a final "true" barcode
ParseHeaders.py merge \
-s "${OUTPUT_DIR}"/${SAMPLE}-seq_cluster-pass.fastq \
-f INDEX_UID INDEX_SEQ \
-k INDEX_NEW \
--outname ${SAMPLE}-seq --outdir "${OUTPUT_DIR}" \
>> "${PIPELINE_LOG}" \
2> "${ERROR_LOG}"

#PairSeq.py: transfer the new barcodes over to the original read1 
PairSeq.py \
-1 "${OUTPUT_DIR}"/${SAMPLE}-R1_primers-pass_pair-pass.fastq \
-2 "${OUTPUT_DIR}"/${SAMPLE}-seq_reheader.fastq \
--2f INDEX_NEW \
--coord presto \
--outname ${SAMPLE}-R1 --outdir "${OUTPUT_DIR}" \
>> $PIPELINE_LOG \
2> $ERROR_LOG

#PairSeq.py: transfer the new barcodes over to the original read2
PairSeq.py \
-1 "${OUTPUT_DIR}"/${SAMPLE}-R2_primers-pass_pair-pass.fastq \
-2 "${OUTPUT_DIR}"/${SAMPLE}-seq_reheader.fastq \
--2f INDEX_NEW \
--coord presto \
--outname ${SAMPLE}-R2 --outdir "${OUTPUT_DIR}" \
>> $PIPELINE_LOG \
2> $ERROR_LOG



####
# Post-Indexing 
####

BuildConsensus.py \
-s "${OUTPUT_DIR}"/${SAMPLE}-R1-1_pair-pass.fastq \
--bf INDEX_NEW \
--pf PRIMER \
-q 0 \
--maxgap 0.5 \
--prcons 0.6 \
--log "${LOG_DIR}"/"${SAMPLE}"-R1_BuildConsensus.log \
--outname ${SAMPLE}-R1 --outdir "${OUTPUT_DIR}" \
>> $PIPELINE_LOG \
2> $ERROR_LOG

BuildConsensus.py \
-s "${OUTPUT_DIR}"/${SAMPLE}-R2-1_pair-pass.fastq \
--bf INDEX_NEW \
-q 0 \
--maxgap 0.5 \
--log "${LOG_DIR}"/"${SAMPLE}"-R2_BuildConsensus.log \
--outname ${SAMPLE}-R2 --outdir "${OUTPUT_DIR}" \
>> $PIPELINE_LOG \
2> $ERROR_LOG

#PairSeq.py: re-pair the reads after consensus building 
PairSeq.py \
-1 "${OUTPUT_DIR}"/${SAMPLE}-R1_consensus-pass.fastq \
-2 "${OUTPUT_DIR}"/${SAMPLE}-R2_consensus-pass.fastq \
--coord presto \
--outname "${SAMPLE}"-consensus --outdir "${OUTPUT_DIR}" \
>> $PIPELINE_LOG \
2> $ERROR_LOG






#AssemblePairs.py align
AP_ALN_SCANREV=true
AP_ALN_MAXERR=0.3
AP_ALN_MINLEN=8
AP_ALN_ALPHA=1e-5

AssemblePairs.py align \
-1 "${OUTPUT_DIR}"/${SAMPLE}-consensus-2_pair-pass.fastq \
-2 "${OUTPUT_DIR}"/${SAMPLE}-consensus-1_pair-pass.fastq \
--1f PRCONS CONSCOUNT \
--2f CONSCOUNT \
--coord presto \
--rc tail \
--minlen ${AP_ALN_MINLEN} \
--maxerror ${AP_ALN_MAXERR} \
--alpha ${AP_ALN_ALPHA} \
--scanrev \
--failed \
--log "${LOG_DIR}"/"${SAMPLE}"_AssemblePairsAlign.log \
--outname "${SAMPLE}"-ALN --outdir "${OUTPUT_DIR}" \
>> $PIPELINE_LOG \
2> $ERROR_LOG

#AssemblePairs.py reference
AP_REF_MINIDENT=0.5
AP_REF_EVALUE=1e-5
AP_REF_MAXHITS=100

AssemblePairs.py reference \
-1 "${OUTPUT_DIR}"/${SAMPLE}-ALN-1_assemble-fail.fastq \
-2 "${OUTPUT_DIR}"/${SAMPLE}-ALN-2_assemble-fail.fastq \
-r ${REF_FILE} \
--1f PRCONS CONSCOUNT \
--2f CONSCOUNT \
--coord presto \
--rc tail \
--minident ${AP_REF_MINIDENT} \
--evalue ${AP_REF_EVALUE} \
--maxhits ${AP_REF_MAXHITS} \
--exec blastn \
--dbexec makeblastdb \
--aligner blastn \
--failed \
--log "${LOG_DIR}"/"${SAMPLE}"_AssemblePairsReference.log \
--outname "${SAMPLE}"-REF --outdir "${OUTPUT_DIR}" \
>> $PIPELINE_LOG \
2> $ERROR_LOG



if [ "$(ls -A "${OUTPUT_DIR}"/${SAMPLE}-REF_assemble-pass.fastq)" ]; then
    cat $OUTPUT_DIR/$SAMPLE-ALN_assemble-pass.fastq $OUTPUT_DIR/$SAMPLE-REF_assemble-pass.fastq > $OUTPUT_DIR/$SAMPLE-CAT_assemble-pass.fastq
else 
    cp $OUTPUT_DIR/$SAMPLE-ALN_assemble-pass.fastq $OUTPUT_DIR/$SAMPLE-CAT_assemble-pass.fastq
fi


#MaskPrimers.py align: align C-region primers
MaskPrimers.py align \
-s "${OUTPUT_DIR}"/"${SAMPLE}"-CAT_assemble-pass.fastq \
-p ${CPRIMERS} \
--maxlen 100 \
--maxerror 0.4 \
--mode tag \
--revpr \
--skiprc \
--failed \
--log "${LOG_DIR}"/"${SAMPLE}"_MaskPrimersCRegion.log \
--outname "${SAMPLE}"-CR --outdir "${OUTPUT_DIR}" \
>> $PIPELINE_LOG \
2> $ERROR_LOG

#ParseHeaders.py rename: PRIMER -> CREGION renaming
ParseHeaders.py rename \
-s "${OUTPUT_DIR}"/"${SAMPLE}"-CR_primers-pass.fastq \
-f PRIMER \
-k CREGION \
--outname "${SAMPLE}"-CR --outdir "${OUTPUT_DIR}" \
>> $PIPELINE_LOG \
2> $ERROR_LOG	



#ParseHeaders.py collapse: return the minimum conscount
ParseHeaders.py collapse \
-s "${OUTPUT_DIR}"/"${SAMPLE}"-CR_reheader.fastq \
-f CONSCOUNT \
--act min \
--outname "${SAMPLE}"-FIN --outdir "${OUTPUT_DIR}" \
>> $PIPELINE_LOG \
2> $ERROR_LOG	

#CollapseSeq.py: create DUPCOUNT
CollapseSeq.py \
-s "${OUTPUT_DIR}"/"${SAMPLE}"-FIN_reheader.fastq \
-n 0 \
--uf PRCONS CREGION \
--cf CONSCOUNT \
--act sum \
--inner \
--keepmiss \
--outname "${SAMPLE}"-FIN --outdir "${OUTPUT_DIR}" \
>> $PIPELINE_LOG \
2> $ERROR_LOG	

#SplitSeq.py group: only report those sequences with CONSCOUNT greater than 2
SplitSeq.py group \
-s "${OUTPUT_DIR}"/"${SAMPLE}"-FIN_collapse-unique.fastq \
-f CONSCOUNT \
--num 2 \
>> $PIPELINE_LOG \
2> $ERROR_LOG	


#ParseHeaders.py: converts outputs to logs
ParseHeaders.py table \
-s "$OUTPUT_DIR/$SAMPLE"-FIN_reheader.fastq \
-f ID PRCONS CREGION CONSCOUNT \
--outname "${SAMPLE}"-Final --outdir "${TABLE_DIR}" \
>> "${PIPELINE_LOG}" \
2> "${ERROR_LOG}"

ParseHeaders.py table \
-s "$OUTPUT_DIR/$SAMPLE"-FIN_collapse-unique.fastq \
-f ID PRCONS CREGION CONSCOUNT DUPCOUNT \
--outname "${SAMPLE}"-Final-Unique --outdir "${TABLE_DIR}" \
>> "${PIPELINE_LOG}" \
2> "${ERROR_LOG}"

ParseHeaders.py table \
-s "$OUTPUT_DIR/$SAMPLE"-FIN_collapse-unique_atleast-2.fastq \
-f ID PRCONS CREGION CONSCOUNT DUPCOUNT \
--outname "${SAMPLE}"-Final-Unique-Atleast2 --outdir "${TABLE_DIR}" \
>> "${PIPELINE_LOG}" \
2> "${ERROR_LOG}"

#No need for pipeline log checking
ParseLog.py -l "${LOG_DIR}"/"${SAMPLE}"-R[1-2]_FilterSeq.log -f ID QUALITY --outdir $TABLE_DIR  >> "${PIPELINE_LOG}" 2> "${ERROR_LOG}"
ParseLog.py -l "${LOG_DIR}"/"${SAMPLE}"-R[1-2]_MaskPrimers.log -f ID BARCODE PRIMER ERROR --outdir $TABLE_DIR >> "${PIPELINE_LOG}" 2> "${ERROR_LOG}"
ParseLog.py -l "${LOG_DIR}"/"${SAMPLE}"-R[1-2]_BuildConsensus.log -f BARCODE SEQCOUNT CONSCOUNT PRIMER PRCONS PRCOUNT PRFREQ ERROR --outdir $TABLE_DIR >> "${PIPELINE_LOG}" 2> "${ERROR_LOG}"
ParseLog.py -l "${LOG_DIR}"/"${SAMPLE}"_AssemblePairsAlign.log -f ID LENGTH OVERLAP ERROR PVALUE FIELDS1 FIELDS2 --outdir $TABLE_DIR >> "${PIPELINE_LOG}" 2> "${ERROR_LOG}"
ParseLog.py -l "${LOG_DIR}"/"${SAMPLE}"_AssemblePairsReference.log -f ID REFID LENGTH OVERLAP GAP EVALUE1 EVALUE2 IDENTITY FIELDS1 FIELDS2 --outdir $TABLE_DIR >> "${PIPELINE_LOG}" 2> "${ERROR_LOG}"
ParseLog.py -l "${LOG_DIR}"/"${SAMPLE}"_MaskPrimersCRegion.log -f ID PRIMER ERROR --outdir $TABLE_DIR >> "${PIPELINE_LOG}" 2> "${ERROR_LOG}"


#FINAL_DIR selections
mv ${OUTPUT_DIR}/${SAMPLE}-FIN_collapse-unique_atleast-2.fastq ${FINAL_DIR}/${SAMPLE}-FIN_collapse-unique_atleast-2.fastq




