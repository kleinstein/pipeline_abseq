#!/bin/bash

#Script for showing a series of jobs on farnam
#ShowSlurm.sh 28156 28160 
#includes 28156 and 28160

START=$1
END=$2

VEC=$(seq $START $END)

for JOB in $VEC; do
	scontrol show job $JOB
done
