#!/bin/bash

#Script for cancelling a series of jobs on farnam
#CancelSlurm.sh 28156 28160 
#includes 28156 and 28160

START=$1
END=$2

VEC=$(seq $START $END)

for JOB in $VEC; do
	scancel $JOB
done
