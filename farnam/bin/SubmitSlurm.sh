#!/bin/bash

# run submission scripts

SLURM_DIR=/ysm-gpfs/pi/kleinstein/oconnor_abseq/oconnor_myositis/pipeline/slurm 
JOBS=$(ls $SLURM_DIR | grep ".sh")
for JOB in $JOBS; do
	sbatch $SLURM_DIR/$JOB
done