#!/bin/bash

#SBATCH --partition=general
#SBATCH --job-name=presto
#SBATCH --ntasks=4 --nodes=1
#SBATCH --cpus-per-task=1
#SBATCH --time=5-
#SBATCH --mem=32GB
#SBATCH --constraint="E5-2660_v3"
