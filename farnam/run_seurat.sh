#!/bin/bash
#SBATCH --partition bigmem
#SBATCH --constraint=E7-4809_v3
#SBATCH --nodes 1
#SBATCH --mem 200G
#SBATCH --time 5-
#SBATCH --job-name seurat
#SBATCH --output seurat-%J.log

# MAKE SURE SEURAT IS INSTALLED BEFOREHAND
# CONSTRAINT IS BECAUSE R DOES NOT RUN PROPERLY ON OPTERON

module load R/3.5.0-foss-2016b-avx2


CCA_COMP=30
ALIGN_COMP=13
CLUSTER_COMP=13
BASE_DIR=/ysm-gpfs/pi/kleinstein/oconnor_musk

mkdir -p ${BASE_DIR}/analysis

ComputeSeurat.R --cca $CCA_COMP --align $ALIGN_COMP --cluster $CLUSTER_COMP \
--input ${BASE_DIR}/10x/defaults \
--out ${BASE_DIR}/analysis/immunecombined_${CCA_COMP}_${ALIGN_COMP}_${CLUSTER_COMP}.rds \
--tsne_pic ${BASE_DIR}/analysis/tsne_${CCA_COMP}_${ALIGN_COMP}_${CLUSTER_COMP}.pdf \
--exp_pic ${BASE_DIR}/analysis/exp_${CCA_COMP}_${ALIGN_COMP}_${CLUSTER_COMP}.pdf 
