#!/bin/bash
#SBATCH --partition bigmem
#SBATCH --nodes 1
#SBATCH --mem 300G
#SBATCH --time 7-
#SBATCH --job-name jupyter-notebook
#SBATCH --output jupyter-notebook-%J.log

# SOURCE: https://research.computing.yale.edu/support/hpc/user-guide/running-jupyter-notebooks-clusters 

# get tunneling info
XDG_RUNTIME_DIR=""
port=8888
port_home=9000
node=$(hostname -s)
user=$(whoami)
cluster=$(hostname -f | awk -F"." '{print $2}')

# print tunneling instructions jupyter-log
echo -e "
MacOS or linux terminal command to create your ssh tunnel:
ssh -N -L ${port_home}:${node}:${port} ${user}@${cluster}.hpc.yale.edu
   
For more info and how to connect from windows, 
   see research.computing.yale.edu/jupyter-nb
Here is the MobaXterm info:

Forwarded port:same as remote port
Remote server: ${node}
Remote port: ${port}
SSH server: ${cluster}.hpc.yale.edu
SSH login: $user
SSH port: 22

Use a Browser on your local machine to go to:
localhost:${port}  (prefix w/ https:// if using password)
"

# load modules or conda environments here
# e.g. farnam:
# module load Python/2.7.13-foss-2016b 

# DON'T USE ADDRESS BELOW. 
# DO USE TOKEN BELOW

# Run from oconnor_abseq directory, this will set as home for jupyter
DATA_DIR=$HOME
singularity run --bind $DATA_DIR:/notebook ~/ganymede-0.0.1.sif
