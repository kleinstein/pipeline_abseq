from collections import Counter
from Bio.Seq import translate
import re
import pandas as pd
import numpy as np
from sklearn.metrics.pairwise import cosine_similarity
from scipy.stats import norm
from presto.Sequence import getDNAScoreDict

from scipy.spatial.distance import braycurtis

from Bio import SeqIO

import tempfile
import os
import glob
import subprocess
import shlex

from changeo.Applications import runIgBLASTN


#### General ####


# input 2 lists containing items
# outputs 2 lists that represent the tabled/count form of the 2 lists
# with the indexes representing the common set of items from the 2 lists
def tableLists(list1, list2):
    dict1 = Counter(list1)
    dict2 = Counter(list2)
    common_keys = list(set(dict1.keys()) | set(dict2.keys()))
    commonkeys_dict1 = {k: dict1[k] if dict1[k] else 0 for k in common_keys}
    commonkeys_dict2 = {k: dict2[k] if dict2[k] else 0 for k in common_keys}
    return (list(commonkeys_dict1.values()), list(commonkeys_dict2.values()))

# compute pairwise overlaps, returns a dataframe
def calculateOverlap(DF, index = 'CLONE', columns = ['SAMPLE'], dist_func = braycurtis):
    
    # pivot the table
    DF_PRE = DF[[index] + columns]
    DF_PRE['COUNT'] = 1
    DF_POST = DF_PRE.pivot_table(index = index, columns = columns, values = 'COUNT').fillna(0)
    DF_POST = DF_POST/DF_POST.sum()
    
    # compute pairwise distances
    dist_dict = {}
    for column1 in DF_POST.columns:
        dist_dict[column1] = {}
        for column2 in DF_POST.columns: 
            dist_dict[column1][column2] = dist_func(np.array(DF_POST[column1]), np.array(DF_POST[column2]))

    return pd.DataFrame(dist_dict)


# compute pairwise overlaps, returns a dataframe
def countOverlap(DF, index = 'CLONE', columns = ['SAMPLE'], collapse=True):
    
    # define function for counting overlap
    if collapse:
        dist_func = (lambda x0, x1: sum([a > 0 and b > 0 for a,b in zip(x0,x1)]))
    else:
        dist_func = (lambda x0, x1: sum([a+b if a > 0 and b > 0 else 0 for a,b in zip(x0,x1)]))
    
    # pivot the table
    DF_PRE = DF[[index] + columns]
    DF_PRE['COUNT'] = 1
    DF_POST = DF_PRE.pivot_table(index = index, columns = columns, values = 'COUNT', aggfunc = np.sum).fillna(0)
    
    # compute pairwise distances
    dist_dict = {}
    for column1 in DF_POST.columns:
        dist_dict[column1] = {}
        for column2 in DF_POST.columns: 
            dist_dict[column1][column2] = dist_func(np.array(DF_POST[column1]), np.array(DF_POST[column2]))

    return pd.DataFrame(dist_dict)

# traks overlap for each comparison, for each element
def tallyPairwiseOverlap(DF, index = 'CLONE', columns = ['SAMPLE'], collapse = True):

    # pivot the table
    DF_PRE = DF[[index] + columns]
    DF_PRE['COUNT'] = 1
    DF_POST = DF_PRE.pivot_table(index = index, columns = columns, values = 'COUNT', aggfunc = np.sum).fillna(0)

    overlap_dict = {}
    for clone in DF_POST.index:
        for sample1 in DF_POST.columns:
            for sample2 in DF_POST.columns:
                if DF_POST.loc[clone, sample1] > 0 or DF_POST.loc[clone,sample2] > 0:
                    result = 0
                    if DF_POST.loc[clone, sample1] > 0 and DF_POST.loc[clone,sample2] > 0:
                        if collapse:
                            result = 1
                        else:
                            result = DF_POST.loc[clone, sample1] + DF_POST.loc[clone,sample2]
                    overlap_dict[clone + '_' + sample1 + '_' + sample2] = \
                        {'CLONE' : clone, 
                         'COMPARISON': sample1 + ',' + sample2, 
                         'SAMPLE1': DF_POST.loc[clone, sample1] > 0,
                         'SAMPLE2': DF_POST.loc[clone, sample2] > 0,
                         'RESULT': result}

    return pd.DataFrame.from_dict(overlap_dict).T


#Used for muation counting, IMGT limit for counting
V_GENE_TOP = 312


TRANSLATE_CREGION_DICT = {
    'Human-IGHA-InternalC': 'IgA',
    'Human-IGHG-InternalC': 'IgG',
    'Human-IGHM-InternalC': 'IgM',
    'Human-IGHD-InternalC': 'IgD',
    'Human-IGHE-InternalC': 'IgE'
}

def translateCregion(cregion):
    try: 
        return TRANSLATE_CREGION_DICT[cregion]
    except KeyError:
        return cregion


# Translate SEQUENCE_IMGT
def translateSequence(sequence):
    try:
        return translate(re.sub('\.','N',sequence))
    except:
        return ''

  

#### Chimera filter


# define window and mismatch size
default_window_size = 10
default_max_mismatch_size = 6

# only assess when germline and sequence are ACGT e.g. not N - . or None (i.e. different lengths)
default_nt_set = ['A', 'C', 'G', 'T']

def detectChimera(germline, \
                  sequence, \
                  window = default_window_size, \
                  max_mismatch = default_max_mismatch_size, \
                  nt_set = default_nt_set):
    
    """
    Detects the presence of chimeras using sliding window analysis

    Arguments:
      germline (str): string corresponding to germline sequence.
      sequence (str): string corresponding to observed sequence.
      window (int): integer corresponding to the window size for scanning.
      max_mismatch (int): integer corresponding to the maximum count of mutations in window size.
      nt_set (iterable): set of nt's used for scanning across germline and observed sequences.

    Returns:
      bool: presence or absence of chimera in sequence.
    """
    
    #generate a bool vector of mismatches 
    mismatch_vec = [g != s for g,s in zip(germline, sequence) if g in nt_set and s in nt_set]
    
    #bool if window found containing more than max_mismatches
    return not any(sum(mismatch_vec[i:(i + window)]) > max_mismatch for i in range(len(mismatch_vec) - window + 1))



#### observedMutations

def observedMutations(sequence, germline, dna_dict=getDNAScoreDict(mask_score=(1, 1), gap_score=(1, 1)), regionDefinition=None):
    
    #1. account for IMGT gapping
    def _substituteSeq(seq): 
        seq = re.sub('\.\.\.', 'ZZZ', seq)
        seq = re.sub('\.', 'N', seq)
        seq = re.sub('ZZZ', '...', seq)
        return seq
    preprocess_sequence = _substituteSeq(sequence)
    preprocess_germline = _substituteSeq(germline)
        
    #2. trim the length of the sequences
    if regionDefinition:
        trim_len = min(len(preprocess_sequence), len(preprocess_germline), regionDefinition)
    else:
        trim_len = min(len(preprocess_sequence), len(preprocess_germline))
    
    #3. account for triplet overhang
    trim_len = int(trim_len/3)*3
    
    #4. if sequence is less than length 3 at this point
    if trim_len < 3:
        return None
    
    preprocess_sequence = preprocess_sequence[:trim_len] 
    preprocess_germline = preprocess_germline[:trim_len] 
    
    hamming = sum(dna_dict[(c1, c2)] for c1, c2 in zip(preprocess_sequence, preprocess_germline))
    return hamming/trim_len


#### Hamming calculator

ambig_char = ['N', '.', '-']

def hamming(s1, s2):
    """Calculate the Hamming distance between two bit strings"""
    #assert len(s1) == len(s2)
    def _compare(c1, c2):
        if c1 != c2 and c1 not in ambig_char and c2 not in ambig_char:
            return 1
        else:
            return 0
    
    return sum(_compare(c1, c2) for c1, c2 in zip(s1, s2))


#### KMeans Threshold
import numpy as np
from sklearn.cluster import KMeans


def findKmeansThreshold(pdf):
    
    #check that pdf is a pdf
    pdf = [x/sum(pdf) for x in pdf]
    
    #convert pdf to sampling (KMeans cannot take a pdf as input)
    sample_choices = np.arange(0, len(pdf))
    sample = np.random.choice(sample_choices, p=pdf, size = 500).reshape(-1,1)
    
    #fit to 2-cluster 1D KMeans 
    km = KMeans(n_clusters=2, random_state=0).fit(sample)
    labels = km.predict(sample_choices.reshape(-1,1))
    
    #find the boundary between the labels and report
    threshold = len([label for label in labels if label == labels[0]])
    
    return threshold


####pandas->tidyr

#function equivalent to tidyr complete
#Example
#completePandas(summary_df, axis1=['SAMPLE', 'STATUS', 'CREGION'], axis2=['V_GENE'], fill_dict={'MEAN':0})
def completePandas(df, axis1, axis2, fill_dict):
    df_dict = df.T.to_dict()
    
    #initialize
    spread_dict = {}
    axis1_set = set()
    axis2_set = set()

    #parse through indexes of df
    for index in df_dict:  
        axis1_key = tuple((field, df_dict[index][field]) for field in axis1)
        axis2_key = tuple((field, df_dict[index][field]) for field in axis2)
        value_key = tuple((field, df_dict[index][field]) for field in fill_dict)

        if spread_dict.get(axis1_key):
            spread_dict[axis1_key][axis2_key] = value_key
        else:
            spread_dict[axis1_key] = {axis2_key : value_key}

        axis1_set.add(axis1_key)
        axis2_set.add(axis2_key)

    #fill in gaps
    spread_dict = {x1: {x2: spread_dict[x1][x2] if spread_dict.get(x1).get(x2) \
                        else tuple(fill_dict.items()) for x2 in axis2_set} for x1 in axis1_set}

    return pd.DataFrame.from_dict(dict(enumerate(dict(spread_dict[x1][x2] + x2 + x1) \
                                                 for x1 in spread_dict for x2 in spread_dict[x1]))).T


#### RDI Calculation

def calcPairwise(matrix, distance_func=(lambda x,y: cosine_similarity(x,y)[0][0])):
    similarity_matrix = np.zeros([matrix.shape[1], matrix.shape[1]])
    for i in range(matrix.shape[1]):
        for j in range(matrix.shape[1]):
            sample1 = matrix[:,i]
            sample2 = matrix[:,j]
            similarity_matrix[i,j] = distance_func(sample1, sample2)
    return similarity_matrix


def calcRDI(DF, sample_column, status_column, select_column, nboot=10, distance_func=(lambda x,y: cosine_similarity(x,y)[0][0])):
    
    # depth of sampling
    min_depth = Counter(DF[sample_column]).most_common()[-1][1]
    
    iter_dict = {}
    for n in range(nboot):
        
        # Subsample DF (with replacement because bootstrap)
        DF_iter = DF.groupby(sample_column).apply(lambda row: row.sample(min_depth, replace=True))

        # Pivot the iteration
        DF_iter_pivot = DF_iter[[select_column, sample_column, status_column]]\
            .pivot_table(index=select_column, columns = [sample_column, status_column],\
                         aggfunc=len, fill_value=0)

        # Calculate the distance matrix (np array) and convert to DF
        distance_matrix = calcPairwise(DF_iter_pivot.as_matrix(), distance_func=distance_func)
        distance_DF = pd.DataFrame(distance_matrix, \
                                   index=DF_iter_pivot.columns.get_level_values(status_column), \
                                   columns=DF_iter_pivot.columns.get_level_values(status_column).values)\
            .reset_index()

        # Take only the upper triangle
        upper_triangle_i = np.triu(np.ones(distance_matrix.shape), k = 1)\
            .astype(np.bool)\
            .reshape(distance_matrix.size)

        # Melt
        melt_distance_DF = distance_DF\
            .melt(id_vars=[status_column], var_name = [status_column + '_'], value_name = 'DISTANCE')\
            [upper_triangle_i]

        # Calculate the means of the melt
        iter_dict[n] = melt_distance_DF.loc[melt_distance_DF[status_column] == melt_distance_DF[status_column + '_'],:]\
            .groupby([status_column])\
            .apply(lambda row: pd.Series({\
                                          'DISTANCE': np.mean(row['DISTANCE'])
                                          }))\
            .reset_index()

    return iter_dict

### P-value tests for 2 CIs

# Returns pairwise diffCI tests from a dataframe
def pairwise_diffCI_test(DF, status, value):
    
    # Given 2 means and 2 std from CIs, computes a significance for the difference
    def _diffCI_test(m1, m2, sd1, sd2):
        return norm.sf(abs(m1-m2)/np.sqrt(sd1**2 + sd2**2))

    # format DF to compute means and std from bootstrap iterations
    result_dict = DF\
        .groupby([status])\
        .apply(lambda row: pd.Series({\
                                      'MEAN': np.mean(row[value]),\
                                      'STD': np.std(row[value])\
                                      }))\
        .reset_index().T.to_dict()

    # compute pairwise p values 
    p_dict = {}
    
    for status1 in result_dict:
        p_dict[result_dict[status1][status]] =  {}
        for status2 in result_dict:
            p_dict[result_dict[status1][status]][result_dict[status2][status]] = \
                _diffCI_test(result_dict[status1]['MEAN'], \
                              result_dict[status2]['MEAN'], \
                              result_dict[status1]['STD'], \
                              result_dict[status2]['STD'])
    
    return pd.DataFrame.from_dict(p_dict)





#### RunIgblast purely console python from seqiter to pandas

reference_dir = "/home/ruoyi/kleinstein/reference/bulk"
default_species = 'human'
default_loci = 'ig'

def runIgBLAST_MakeDb(seq_iter, loci = default_loci, species = default_species,  format_type="changeo"):
    
    # write seq_iter to temp fasta file
    seq_handle = tempfile.NamedTemporaryFile(mode ='w+t', encoding='utf-8', suffix = '.fasta')
    file_type = 'fasta'

    SeqIO.write(seq_iter, seq_handle.name, file_type)

    # create handle for igblast output
    fmt_handle = tempfile.NamedTemporaryFile(mode ='w+t', encoding='utf-8')

    # run igblast
    runIgBLASTN( \
    seq_handle.name, \
    os.path.join(reference_dir, "igblast_db"), \
    loci=loci, \
    organism=species, \
    output=fmt_handle.name,
    format='blast', \
    threads=1, \
    exec='igblastn')

    # create handle for MakeDb output
    db_handle = tempfile.NamedTemporaryFile(mode ='w+t', encoding='utf-8', suffix = "")

    # run MakeDb
    cmd = [
    "MakeDb.py igblast", 
    "--format", format_type,
    "-s", seq_handle.name,
    "-i", fmt_handle.name,
    "-r", " ".join(glob.glob(os.path.join(reference_dir, "imgt/human/vdj/*.fasta"))),
    "--outdir", os.path.dirname(db_handle.name),
    "--outname", os.path.basename(db_handle.name)
    ]

    result = subprocess.Popen(shlex.split(' '.join(cmd)), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    output = result.communicate()
    #print(output[0].decode("utf-8") + "\nERROR: \n" + output[1].decode("utf-8"))

    # read in the output DF
    if format_type == "changeo":
        out_df = pd.read_csv(db_handle.name + "_db-pass.tab", sep = '\t', dtype = 'object')
    else:
        out_df = pd.read_csv(db_handle.name + "_db-pass.tsv", sep = '\t', dtype = 'object')

    seq_handle.close()
    fmt_handle.close()
    db_handle.close()
    
    return out_df


def runDefineClones(DF, threshold=0.15, JUNCTION = "JUNCTION", V_CALL = "V_CALL", J_CALL = "J_CALL"):
    
    #threshold = 0.15
    # create handle for MakeDb output
    input_handle = tempfile.NamedTemporaryFile(mode ='w+t', encoding='utf-8', suffix = ".tab")
    db_handle = tempfile.NamedTemporaryFile(mode ='w+t', encoding='utf-8', suffix = "_clone-pass.tab")

    DF.to_csv(input_handle.name, sep = '\t')

    cmd = [
    "DefineClones.py",
    "-d", input_handle.name,
    "--model ham --norm len --mode gene --act first --dist", str(threshold),
    "--sf", JUNCTION, "--vf", V_CALL, "--jf", J_CALL,
    "--outdir", os.path.dirname(db_handle.name),
    "--outname", os.path.basename(re.sub("_clone-pass.tab","", db_handle.name))
    ]

    result = subprocess.Popen(shlex.split(' '.join(cmd)), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    output = result.communicate()
    
    input_handle.close()

    DF_clone = pd.read_csv(db_handle.name, sep = '\t', dtype = 'object')

    db_handle.close()

    return DF_clone


def runCreateGermlines(DF, ref_dir="/home/ruoyi/kleinstein/reference/bulk/imgt/human/vdj", mask="dmask", cloned = "--cloned", format_type="changeo"):

    input_handle = tempfile.NamedTemporaryFile(mode ='w+t', encoding='utf-8', suffix = ".tab")
    db_handle = tempfile.NamedTemporaryFile(mode ='w+t', encoding='utf-8', suffix = "_germ-pass.tab")

    DF.to_csv(input_handle.name, sep = '\t')

    cmd = [
    "CreateGermlines.py",
    "--format", format_type,
    "-d", input_handle.name,
    "-r", ref_dir,
    "-g", mask, 
    "--sf SEQUENCE_IMGT --vf V_CALL", 
    cloned,
    "--outdir", os.path.dirname(db_handle.name),
    "--outname", os.path.basename(re.sub("_germ-pass.tab","", db_handle.name))
    ]

    result = subprocess.Popen(shlex.split(' '.join(cmd)), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    output = result.communicate()
    
    input_handle.close()

    DF_out = pd.read_csv(db_handle.name, sep = '\t', dtype = 'object')

    db_handle.close()

    return DF_out


def runShazamPlot(DF, plot_dir, name = "default"):
    # plot_dir specifies a directory to drop output tables and pdf of DTN
    
    input_handle = tempfile.NamedTemporaryFile(mode ='w+t', encoding='utf-8', suffix = ".tab")

    DF.to_csv(input_handle.name, sep = '\t')

    cmd = [
    "shazam-threshold.R",
    "-n", name,
    "-d", input_handle.name,
    "-o", plot_dir
    ]

    result = subprocess.Popen(shlex.split(' '.join(cmd)), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    output = result.communicate()
    print(output[0].decode("utf-8") + "\nERROR: \n" + output[1].decode("utf-8"))
    
    try:
        threshold = pd.read_csv(os.path.join(plot_dir, name + "_threshold-values.tab"),\
                                          sep = '\t')['threshold']
    except: 
        threshold = None

    return threshold




#### complete-linkage light chain cloning for multiple light chain cells

def lightchainCompleteLinkage(cell_series, group_series):
    """
    Performs complete linkage of V_GENE, J_GENE columns

    Arguments:
      cell_series (pandas series): pandas series of CELL labels.
      group_series (pandas series): pandas series of grouping labels (e.g. VJJL).

    Returns:
      assign_dict (dict): dictionary assigning cells to VJJL clusters corrected with complete linkage.
    """
    
    # assign initial clusters
    lc_dict = {}
    for cell, group in zip(cell_series, group_series):
        try:    
            lc_dict[group].append(cell)
        except KeyError:
            lc_dict[group] = [cell]

    # link clusters (ON^2) ...ie for cells with multiple light chains does complete linkage
    cluster_dict = {}
    for i, gene in enumerate(lc_dict.keys()):
        notfound = True
        for cluster in cluster_dict:
            if any(i in lc_dict[gene] for i in cluster_dict[cluster]):
                cluster_dict[cluster] = cluster_dict[cluster] + lc_dict[gene]
                notfound = False
                break
        if notfound:
            cluster_dict[i] = lc_dict[gene]
    
    assign_dict = {cell:k for k,v in cluster_dict.items() for cell in set(v)}
    
    return assign_dict




from presto.Sequence import localAlignment, maskSeq, getDNAScoreDict
from presto.IO import readPrimerFile
from presto.Defaults import default_primer_max_error, default_primer_max_len, default_barcode_field, \
default_primer_field, default_primer_gap_penalty, default_delimiter
from presto.Annotation import parseAnnotation

from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord

primer_file = "/home/ruoyi/kleinstein/reference/bulk/primers/AbSeqV3_Human_InternalCRegion.fasta"
primers = readPrimerFile(primer_file)

def maskInternalCregion(sequence, primers=primers):
    
    # set default parameters (used in MaskPrimers.py)
    max_error=default_primer_max_error
    max_len=default_primer_max_len
    rev_primer=False
    skip_rc=False
    mode='tag'
    barcode=False
    barcode_field=default_barcode_field
    primer_field=default_primer_field
    delimiter=default_delimiter
    gap_penalty=default_primer_gap_penalty
    score_dict=getDNAScoreDict(mask_score=(0, 1), gap_score=(0, 0))
    primers_regex=None
    
    # create a dummy input SeqRecord
    input_seqrecord = SeqRecord(Seq(sequence))

    # run maskprimers on input SeqRecord
    align = localAlignment(input_seqrecord, primers, primers_regex=primers_regex, max_error=max_error,
                       max_len=max_len, rev_primer=rev_primer, skip_rc=skip_rc,
                       gap_penalty=gap_penalty, score_dict=score_dict)
    seqrecord = maskSeq(align, mode=mode, barcode=barcode, barcode_field=barcode_field,
                      primer_field=primer_field, delimiter=delimiter)

    # parse output
    if align.error <= max_error:
        call = parseAnnotation(seqrecord.id)['PRIMER']
    else:
        call = False
    
    return call


# Dictionary of codon to codon mutations and if they are R vs S

def generateCodonVariants(codon):
    return (''.join([codon[i] if i is not j else nt for i in range(3)]) for j in range(3) for nt in 'ACGT')

all_codons = [nt1 + nt2 + nt3 for nt1 in 'ACTG' for nt2 in 'ACTG' for nt3 in 'ACTG']
all_codon_dict = {codon: translate(codon) for codon in all_codons}

RS_dict = {codon: {variant: 'S' if all_codon_dict[codon] == all_codon_dict[variant] \
                   else 'R' for variant in generateCodonVariants(codon)} for codon in all_codons}
