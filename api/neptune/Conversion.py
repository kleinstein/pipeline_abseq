# Info
__author__ = 'Anthony Ma, Ruoyi Jiang'
#from hocuspocus import __version__, __date__

# Imports
import pandas as pd
import re
from Bio.Seq import Seq, translate
from Bio.SeqRecord import SeqRecord
from Bio import SeqIO

# Presto and changeo imports
from presto.Defaults import default_delimiter
from presto.Annotation import flattenAnnotation


def transformF3L1(seq):
    """
    Truncate the first 3 nt and last 1 nt of a full sequence
    
    Arguments:
      seq : JUNCTION DNA sequence (string)

    Returns:
      translated_seq : JUNCTION AA sequence (string)
    """
    processed_seq = seq[3:-1]
    return processed_seq


def translateCDR3(seq):
    """
    Translates a DNA JUNCTION sequence into an AA JUNCTION sequence, does appropriate checks

    Arguments:
      seq : JUNCTION DNA sequence (string)

    Returns:
      translated_seq : JUNCTION AA sequence (string)
    """
   
    def _filter_DNA_seq(seq):
        return '.' not in seq and '-' not in seq and '*' not in seq and len(seq) % 3 == 0

    def _filter_cdr3_aa(seq):
        return 'X' not in seq and '*' not in seq and seq[0] == 'C' and (seq[-1] == 'W' or seq[-1] == 'F')

    if _filter_DNA_seq(seq):
        translated_seq = translate(seq)
        if _filter_cdr3_aa(translated_seq):
            return translated_seq
    return None

def translateRegion(seq):
    """
    Translates a DNA JUNCTION sequence into an AA JUNCTION sequence, does appropriate checks

    Arguments:
      seq : JUNCTION DNA sequence (string)

    Returns:
      translated_seq : JUNCTION AA sequence (string)
    """
   
    def _filter_DNA_seq(seq):
        return 'N' not in seq and '*' not in seq and len(seq) % 3 == 0
    
    seq = re.sub('[-.]', '', seq)
    
    if _filter_DNA_seq(seq):
        translated_seq = translate(seq)
        if '*' not in translated_seq:
            return translated_seq
    return ''



def writeSeqIter(seq_iter, fasta_filename, file_type='fasta'):
    """
    Writes out a seq_iter into a presto-annotated fasta or fastq file

    Arguments:
      seq_iter : an iterator of SeqRecords
      fasta_filename : string name of the output fasta file
      file_type : SeqRecord output file type, fastq or fasta

    Returns:
      None : None
    """
    
    out_handle = open(fasta_filename, 'w')
    
    for seq in seq_iter:
        SeqIO.write(seq, out_handle, file_type)
    
    out_handle.close()
    
    return 1


def SeqIterToDataFrame(seq_iter, seq_column='SEQUENCE', id_column='ID'):
    """
    Converts a seq_iter into a pandas dataframe with 2 columns: one SEQUENCE, one annotation

    Arguments:
      seq_iter : an iterator of SeqRecords
      seq_column : the column in the pandas dataframe containing sequence information
      id_column : the column in the pandas dataframe containing information for the sequence ID 
      
    Returns:
      out_df : pandas dataframe with 2 columns: one SEQUENCE, one annotation
    """
    
    seq_dict = {}
    for index, seq_record in enumerate(seq_iter):
        new_entry = {}
        new_entry[id_column] = seq_record.description
        new_entry[seq_column] = str(seq_record.seq)
        seq_dict[index] = new_entry
        
    out_df = pd.DataFrame.from_dict(seq_dict).T
    
    return out_df


def DataFrameToSeqIter(df, seq_column,
                     id_column=None, ann_columns=[]):
    """
    Converts a pandas dataframe into a presto-annotated seq_iter

    Arguments:
      df : a pandas dataframe
      seq_column : the column in the pandas dataframe containing sequence information
      id_column : the column in the pandas dataframe containing information for the presto ID 
      ann_columns : additional columns to extract from the pandas dataframe

    Returns:
      out_iter : iter of SeqRecords for writing
    """
    def _rowToSeqRecord(idx, row):
        try:
            seq_record = SeqRecord(Seq(str(row[seq_column])))
        except KeyError:
            raise('seq_column not found in table')
            
        if len(ann_columns) > 0:
            out_ann = {key: row[key] for key in ann_columns}
        else: 
            out_ann = {}

        if id_column is None and 'ID' not in ann_columns:
            out_ann.update({'ID': str(idx)})
        elif 'ID' in ann_columns: 
            raise ValueError('column ID is special and cannot be in ann_columns.')
        else:
            out_ann.update({'ID': row[id_column]})
            
        seq_record.id = flattenAnnotation(out_ann, delimiter=default_delimiter)
        seq_record.description = ''
        seq_record.name = ''
        
        return seq_record
    
    # does not support multi-index because changeo does not accept tuples
    df = df.reset_index()
    
    out_iter = (_rowToSeqRecord(idx, row) for idx, row in df.iterrows())
    
    return out_iter 
    
    
def SampleClones(df, clone_column, n_count):
    """
    Samples n_count rows from each clone

    Arguments:
      df : a pandas dataframe
      clone_column : the column in the pandas dataframe containing CLONE information
      n_count : the number of rows from each clone to sample

    Returns:
      a pandas dataframe : df grouped by clone and sampled at n_count depth
    """
    
    def _sampleRows(row, n_count):
        if row.shape[0] < n_count:
            return row
        else:
            return row.sample(n_count)

    return df.\
        groupby(clone_column).\
        apply(lambda row: _sampleRows(row, n_count))