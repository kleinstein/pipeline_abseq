### Flow cytometry related functions


import numpy as np
from matplotlib.lines import Line2D
from matplotlib.artist import Artist
from matplotlib.mlab import dist_point_to_segment

from matplotlib.patches import Polygon

#From https://matplotlib.org/gallery/event_handling/poly_editor.html

class PolygonInteractor(object):
    showverts = True
    epsilon = 5  # max pixel distance to count as a vertex hit
    
    def __init__(self, ax, poly):
        if poly.figure is None:
            raise RuntimeError('You must first add the polygon to a figure '
                               'or canvas before defining the interactor')
        self.ax = ax
        canvas = poly.figure.canvas
        self.poly = poly
        x, y = zip(*self.poly.xy)
        self.line = Line2D(x, y,
                           marker='o', markerfacecolor='r',
                           animated=True)
        self.ax.add_line(self.line)
        self.cid = self.poly.add_callback(self.poly_changed)
        self._ind = None  # the active vert
        canvas.mpl_connect('draw_event', self.draw_callback)
        canvas.mpl_connect('button_press_event', self.button_press_callback)
        canvas.mpl_connect('key_press_event', self.key_press_callback)
        canvas.mpl_connect('button_release_event', self.button_release_callback)
        canvas.mpl_connect('motion_notify_event', self.motion_notify_callback)
        self.canvas = canvas
    def draw_callback(self, event):
        self.background = self.canvas.copy_from_bbox(self.ax.bbox)
        self.ax.draw_artist(self.poly)
        self.ax.draw_artist(self.line)
        # do not need to blit here, this will fire before the screen is
        # updated
    
    def poly_changed(self, poly):
        'this method is called whenever the polygon object is called'
        # only copy the artist props to the line (except visibility)
        vis = self.line.get_visible()
        Artist.update_from(self.line, poly)
        self.line.set_visible(vis)  # don't use the poly visibility state
    
    def get_ind_under_point(self, event):
        'get the index of the vertex under point if within epsilon tolerance'
        
        # display coords
        xy = np.asarray(self.poly.xy)
        xyt = self.poly.get_transform().transform(xy)
        xt, yt = xyt[:, 0], xyt[:, 1]
        d = np.hypot(xt - event.x, yt - event.y)
        indseq, = np.nonzero(d == d.min())
        ind = indseq[0]
        
        if d[ind] >= self.epsilon:
            ind = None
        
        return ind
    
    def button_press_callback(self, event):
        'whenever a mouse button is pressed'
        if not self.showverts:
            return
        if event.inaxes is None:
            return
        if event.button != 1:
            return
        self._ind = self.get_ind_under_point(event)
    
    def button_release_callback(self, event):
        'whenever a mouse button is released'
        if not self.showverts:
            return
        if event.button != 1:
            return
        self._ind = None
    
    def key_press_callback(self, event):
        'whenever a key is pressed'
        if not event.inaxes:
            return
        if event.key == 't':
            self.showverts = not self.showverts
            self.line.set_visible(self.showverts)
            if not self.showverts:
                self._ind = None
        elif event.key == 'd':
            ind = self.get_ind_under_point(event)
            if ind is not None:
                self.poly.xy = np.delete(self.poly.xy,
                                         ind, axis=0)
                self.line.set_data(zip(*self.poly.xy))
        elif event.key == 'i':
            xys = self.poly.get_transform().transform(self.poly.xy)
            p = event.x, event.y  # display coords
            for i in range(len(xys) - 1):
                s0 = xys[i]
                s1 = xys[i + 1]
                d = dist_point_to_segment(p, s0, s1)
                if d <= self.epsilon:
                    self.poly.xy = np.insert(
                        self.poly.xy, i+1,
                        [event.xdata, event.ydata],
                        axis=0)
                    self.line.set_data(zip(*self.poly.xy))
                    break
        if self.line.stale:
            self.canvas.draw_idle()
    
    def motion_notify_callback(self, event):
        'on mouse movement'
        if not self.showverts:
            return
        if self._ind is None:
            return
        if event.inaxes is None:
            return
        if event.button != 1:
            return
        x, y = event.xdata, event.ydata
        
        self.poly.xy[self._ind] = x, y
        if self._ind == 0:
            self.poly.xy[-1] = x, y
        elif self._ind == len(self.poly.xy) - 1:
            self.poly.xy[0] = x, y
        self.line.set_data(zip(*self.poly.xy))
        
        self.canvas.restore_region(self.background)
        self.ax.draw_artist(self.poly)
        self.ax.draw_artist(self.line)
        self.canvas.blit(self.ax.bbox)
        

        
def subsamplePopulation(pop, depth = 50000):
    if pop.shape[0] < depth:
        return pop
    else:
        return pop.loc[0:depth,:]
    
    
def plotPopulation(data, x_label, y_label, ax, color = 'gray'):
    x_gate = x_label
    y_gate = y_label
    sub = subsamplePopulation(data)[[x_gate, y_gate]]

    x_diff = max(sub[x_gate]) - min(sub[x_gate]) 
    y_diff = max(sub[y_gate]) - min(sub[y_gate]) 
    x_limits = (min(sub[x_gate]) - 0.2*x_diff, 1.5*max(sub[x_gate]) + 0.2*x_diff)
    y_limits = (min(sub[y_gate]) - 0.2*y_diff, 1.5*max(sub[y_gate]) + 0.2*y_diff)
    
    ax.set_xlim(x_limits[0], x_limits[1])
    ax.set_ylim(y_limits[0], y_limits[1])
    ax.set_xlabel(x_label, size = 14)
    ax.set_ylabel(y_label, size = 14)
    ax.scatter(sub[x_gate], sub[y_gate], color=color, s = 0.5)
    
    return ax, x_limits, y_limits


def applyGate(data, gate, x_label, y_label):
    #returns a array of T and F
    return Polygon(gate).contains_points(data[[x_label, y_label]].values)


def shapeGate(ax, x_limits, y_limits):
    poly = Polygon(np.array([[x_limits[1]*0.2,y_limits[1]*0.8],\
                     [x_limits[1]*0.2,y_limits[1]*0.2],
                     [x_limits[1]*0.8,y_limits[1]*0.2],\
                     [x_limits[1]*0.8,y_limits[1]*0.8]]), animated=True, fill=False)
    ax.add_patch(poly)
    p = pluto.PolygonInteractor(ax, poly)
    return ax, poly


#add gates
def gatePopulation(data, x_label, y_label):
    fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(8,8))

    #Add the population
    ax, x_limits, y_limits = plotPopulation(data, x_label, y_label, ax)

    #Add the gate and reshape
    ax, poly = shapeGate(ax, x_limits, y_limits)

    plt.show()
    
    return poly


def plotGated(data, gated, x_label, y_label):
    fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(8,8))

    #Add the background
    ax, x_limits, y_limits = plotPopulation(data, x_label, y_label, ax)

    #Add the gated
    ax, _, _ = plotPopulation(gated, x_label, y_label, ax, color = 'red')

    #Set the axes
    ax.set_xlim(x_limits[0], x_limits[1])
    ax.set_ylim(y_limits[0], y_limits[1])

    plt.show()
       