import json
import pandas as pd
import math
import os

base_dir = "/home/ruoyi/Dropbox/OConnor_LabNotebook_RuoyiJiang/Ruoyi_LN2_Locations/maps/experiments"
map_dir = "/home/ruoyi/Dropbox/OConnor_LabNotebook_RuoyiJiang/Ruoyi_LN2_Locations/boxes"
inventory_json = "/home/ruoyi/Dropbox/OConnor_LabNotebook_RuoyiJiang/Ruoyi_LN2_Locations/inventory.json"

experiment_label_dict = {
'Other MG' : '/home/ruoyi/Dropbox/OConnor_LabNotebook_RuoyiJiang/Ruoyi_LN2_Locations/inventories/other_mg_inventory-31_08_19.csv',
'MuSK MG': '/home/ruoyi/Dropbox/OConnor_LabNotebook_RuoyiJiang/Ruoyi_LN2_Locations/inventories/musk_mg_inventory-29_03_18.csv',
'NeuroNext': '/home/ruoyi/Dropbox/OConnor_LabNotebook_RuoyiJiang/Ruoyi_LN2_Locations/inventories/neuronext_inventory-13_12_18.csv',
'Therapy Naive AChR MG': '/home/ruoyi/Dropbox/OConnor_LabNotebook_RuoyiJiang/Ruoyi_LN2_Locations/inventories/therapy_naive_achr_mg_inventory-15_12_17.csv',
'Healthy Donor': '/home/ruoyi/Dropbox/OConnor_LabNotebook_RuoyiJiang/Ruoyi_LN2_Locations/inventories/hd_inventory-17_12_17.csv'
}

def updateInventory():

    inventory_dict = {}

    for experiment in experiment_label_dict:
        experiment_df = pd.read_csv(experiment_label_dict[experiment], dtype = 'object')    

        for row in experiment_df.iterrows():
            meta_dict = {
                'experiment': experiment
            }
            eval_dict = {}
            for column in row[1].keys(): 
                if column == 'id':
                    identity = row[1][column]
                if column.split(';')[0] == 'meta':
                    meta_dict[column.split(';')[1]] = row[1][column]
                if column.split(';')[0] == 'eval':
                    eval_dict[column.split(';')[1]] = row[1][column]
            
            inventory_dict[identity] = {"locations": [], "eval": eval_dict, "meta": meta_dict}

    racks = os.listdir(map_dir)
    for rack in racks:
        curr_rack_dir = os.path.join(map_dir, rack)
        boxes = os.listdir(curr_rack_dir)
        for box in boxes:
            curr_box_file = os.path.join(curr_rack_dir, box)
            box_df = pd.read_csv(curr_box_file, header = None)
            for column in box_df:
                for row, sample in enumerate(box_df[column]):
                    try:
                        inventory_dict[sample]['locations'].append((rack, box, row, column))
                    except KeyError:
                        pass

    with open(inventory_json, 'w') as outfile:
        json.dump(inventory_dict, outfile, indent=4)
        
    return inventory_dict

def loadInventory():
    with open(inventory_json, 'r') as outfile:
        return json.load(outfile)
    
def removeTubes(sample, coordinates):
    rack, box, row, column = coordinates
    box_df = pd.read_csv(os.path.join(map_dir, rack, box), header=None)
    box_df.iloc[row, column] = 'nan'
    box_df.to_csv(os.path.join(map_dir, rack, box), index=False, header=False)

    with open(inventory_json, 'r') as outfile:
        inventory_dict = json.load(outfile)

    with open(inventory_json, 'w') as outfile:
        inventory_dict[sample]['locations'] = [location for location in inventory_dict[sample]['locations'] if location != coordinates]
        json.dump(inventory_dict, outfile, indent=4)
        
        
qc_columns = [
    "SAMPLE",
    "STATUS",
    "RUN",
    "SET",
    "comments",
    "location",
    "found",
    "cell_count_1k",
    "rna_nanodrop_conc",
    "rna_nanodrop_260_230",
    "rna_nanodrop_260_280",
    "rna_nanodrop_curve_bool",
    "rna_tapestation_dilution",
    "rna_tapestation_conc",
    "rna_tapestation_rin",
    "rna_proceed_bool",
    "protocol",
    "rna_vol_for_pcr1_(1k_divided_by_conc)",
    "index",
    "primer",
    "qpcr_ct",
    "pcr2_nanodrop_conc",
    "pcr2_nanodrop_260_230",
    "pcr2_nanodrop_260_280",
    "pcr2_nanodrop_curve_bool",
    "pcr2_tapestation_dilution",
    "pcr2_tapestation_conc_diluted",
    "pcr2_tapestation_700bp_conc_diluted",
    "pcr2_proceed_bool",
    "pcr2_final_conc(700bp_conc_dilutedxdilution)",
    "pcr2_final_6ng_vol(6/pcr2_final_conc)"
]