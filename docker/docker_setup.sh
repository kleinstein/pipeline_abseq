# Build
docker build -t ganyemede .

# Login
docker login --username=ruoyijiang

# Find image ID
docker images

# Tag image with count, name and version
docker tag 5866de306053 ruoyijiang/ganymede:0.0.1

# Push image
docker push ruoyijiang/ganymede:0.0.1

#### Pull as singularity

IMAGE="ganymede-0.0.1.sif"
singularity build $IMAGE docker://ruoyijiang/ganymede:0.0.1

# sbatch docker to start jupyter notebook
singularity shell $IMAGE