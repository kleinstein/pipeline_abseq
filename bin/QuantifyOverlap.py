#!/usr/bin/env python3
"""
Computes clonal overlap using scipy and sklearn metrics
"""
# Info
__author__ = 'Ruoyi Jiang'

import sys
import os
from argparse import ArgumentParser
import pandas as pd

from sklearn.metrics.pairwise import cosine_similarity
from scipy.spatial.distance import braycurtis

sys.path.append(os.path.join(os.environ['HOME'],'Dropbox/git/pipeline_abseq/api/neptune'))
import neptune

### Arguments
parser = ArgumentParser(description="Quantify overlap.")

parser.add_argument('-i', action='store', dest='input_file', type=str,
                         help='''The input file.''')					 
parser.add_argument('-o', action='store', dest='output_file', type=str,
                         help='''The output file.''')	
                         	
parser.add_argument('-g', action='store', dest='group', type=str,
                         help='''The group field to calculate overlap across.''')					 
parser.add_argument('-c', action='store', dest='clone', type=str,
                         help='''The clone field containing species.''')						 
parser.add_argument('--act', action='store', dest='action', required=False,
                         choices=['count_bool', 'count_value', 'braycurtis', 'cosine', 'sorenson', 'sorenson_bool', 'frequency'],
                         help='''The overlap calculation method.''')					

args = vars(parser.parse_args())

input_file = args['input_file']
output_file = args['output_file']
group = args['group']
index = args['clone']
action = args['action']

### Assign functions to run based on inputs
distance_measure_dict = {
    'cosine': (lambda x0, x1: cosine_similarity(x0,x1)[0][0]),
    'braycurtis' : braycurtis,
    'sorenson': (lambda x0, x1: 1 - braycurtis(x0,x1)),
    'sorenson_bool': (lambda x0, x1: 1 - braycurtis(1*(x0 > 0), 1*(x1 > 0))),
    'frequency': (lambda x0, x1: sum([a > 0 and b > 0 for a,b in zip(x0,x1)])/sum([b > 0 for b in x1]))
}

run_dict = {
    'count_bool' : {
        'func': neptune.countOverlap,
        'args': {
            'collapse': True
        }
    },
    'count_value' : {
        'func': neptune.countOverlap,
        'args': {
            'collapse': False
        }
    },
    'braycurtis' : {
        'func': neptune.calculateOverlap,
        'args': {
            'dist_func': distance_measure_dict['braycurtis'] 
        }
    },
    'sorenson' : {
        'func': neptune.calculateOverlap,
        'args': {
            'dist_func': distance_measure_dict['sorenson'] 
        }
    },
    'sorenson_bool' : {
        'func': neptune.calculateOverlap,
        'args': {
            'dist_func': distance_measure_dict['sorenson_bool'] 
        }
    },
    'cosine' : {
        'func': neptune.calculateOverlap,
        'args': {
            'dist_func': distance_measure_dict['cosine'] 
        }
    },
    'frequency' : {
        'func': neptune.calculateOverlap,
        'args': {
            'dist_func': distance_measure_dict['frequency'] 
        }
    }
}

### Input
df = pd.read_csv(input_file, sep = '\t', dtype = 'object')

### Compute
overlap = run_dict[action]['func'](df, index=index, columns=[group], **run_dict[action]['args'])

### Output
#overlap.reset_index().to_csv(output_file)
overlap.reset_index().melt(id_vars=['index'], var_name=group, value_name='OVERLAP').to_csv(output_file)