#!/usr/bin/env python3

#Ruoyi Jiang
#1/30/2018
#read in fastq line by line
#excise seq by nt coordinates and move to annotation field

from presto.IO import readSeqFile
from presto.Annotation import parseAnnotation, mergeAnnotation, flattenAnnotation

from Bio import SeqIO
import re
import sys

start_cut =  int(sys.argv[1])
end_cut = int(sys.argv[2])
field_name = sys.argv[3]
input_file = sys.argv[4]
output_file = sys.argv[5]

out_type = 'fastq' 

data_iter = readSeqFile(input_file, index=False) #seq_id + seq_record #2 mins

pass_handle = open(output_file, 'w')


for seq in data_iter:
	header = parseAnnotation(seq.description)
	header[field_name] = seq.seq._data[start_cut:end_cut]
	seq.id = seq.name = flattenAnnotation(header)
	seq.description = ''

	phred_scores = seq.letter_annotations["phred_quality"] 
	seq.letter_annotations = {}
	seq.seq = seq.seq[:start_cut] + seq.seq._data[end_cut:]
	seq.letter_annotations["phred_quality"] = phred_scores[:start_cut] + phred_scores[end_cut:] 

	results = [seq]
	SeqIO.write(results, pass_handle, out_type)

pass_handle.close()
