#!/usr/bin/env python3
"""
Reverse complements fastq sequences
"""
# Info
__author__ = 'Ruoyi Jiang'

from presto.IO import readSeqFile

from Bio import SeqIO

import sys
import os

out_type="fastq"

#file_input = "/home/ruoyi/kleinstein/abseq/test_data/presto/migec_eval/DM1-R1_primers-pass_pair-pass.fastq"
file_input = sys.argv[1]
file_output = sys.argv[2]

seq_iter = readSeqFile(file_input)

pass_handle = open(file_output, 'w')

#Update the annotation field using the shuffled list
for seq in seq_iter:
    tmp_id = seq.id
    tmp_desc = seq.description
    seq = seq.reverse_complement()
    seq.id = tmp_id
    seq.description = tmp_desc
    SeqIO.write(seq, pass_handle, out_type)

pass_handle.close()

