#!/usr/bin/env python3
"""
Translates SEQUENCE_IMGT and quantifies glycosylation motif frequencies including for V regions
"""

# Info
__author__ = 'Ruoyi Jiang'
__date__ = 'July 22, 2020'

# Imports
import pandas as pd
import numpy as np
import re
import sys
sys.path.append('/home/ruoyi/Dropbox/hg/pipeline_abseq/api/neptune')
import neptune
from Bio.Seq import translate

# Parse arguments
in_file = sys.argv[1]
out_file = sys.argv[2]

# Define function for identifying glycosylation sites
def countGlycosylation(sequence):
    if len(sequence) > 2:
        return len(re.findall('N[^P^X][ST]', sequence))
    else:
        return 0
        
# Read in and perform computations
samples_df = pd.read_csv(in_file, dtype = 'object', sep = '\t')

samples_df['V_SEQUENCE_AA'] = \
    samples_df.apply(lambda x: neptune.translateSequence(x['SEQUENCE_IMGT'][0:neptune.V_GENE_TOP]), axis = 1)

samples_df['V_GERMLINE_AA'] = \
    samples_df.apply(lambda x: neptune.translateSequence(x['GERMLINE_IMGT_D_MASK'][0:neptune.V_GENE_TOP]), axis = 1)

samples_df['GLYCOSYLATION_COUNT_V_SEQUENCE'] = \
    samples_df.apply(lambda x: countGlycosylation(x['V_SEQUENCE_AA']), axis = 1)

samples_df['GLYCOSYLATION_COUNT_V_GERMLINE'] = \
    samples_df.apply(lambda x: countGlycosylation(x['V_GERMLINE_AA']), axis = 1)


# Perform computations for regions
regions = ['FWR1_IMGT', 'CDR1_IMGT', 'FWR2_IMGT', 'CDR2_IMGT', 'FWR3_IMGT', 'JUNCTION']

def filterDNASeq(seq):
    return '-' not in seq and '*' not in seq and len(seq) % 3 == 0 and seq != ''

def filterRegion(seq):
    return '*' not in seq #and 'X' not in seq and seq[0] == 'C' and (seq[-1] == 'W' or seq[-1] == 'F')

def translateRegion(in_seq):
    seq = re.sub('\.', '', in_seq)
    if filterDNASeq(seq):
        translated_seq = translate(seq)
        if filterRegion(translated_seq):
            return translated_seq
    return 'NA'
    
for region in regions:
    #print(region)
    samples_df[region + '_AA'] = samples_df.apply(lambda row: translateRegion(row[region]), axis = 1)
    samples_df[region + '_GLYC'] = samples_df.apply(lambda row: countGlycosylation(row[region + '_AA']) if row[region + '_AA'] is not 'NA' else 'NA', axis = 1)

samples_df.to_csv(out_file)

#samples_df.to_csv(out_file, sep = '\t')