#!/usr/bin/env python3
"""
Export as DF, runs neptune (converts to fasta, IgBlast, Makedb), outputs DF
"""
# Info
__author__ = 'Ruoyi Jiang'

import sys
import os
from argparse import ArgumentParser
import pandas as pd

sys.path.append(os.path.join(os.environ['HOME'],'Dropbox/git/pipeline_abseq/api/neptune'))
import neptune
from Conversion import *

### Arguments
parser = ArgumentParser(description="Quantify overlap.")

parser.add_argument('-i', action='store', dest='input_file', type=str,
                         help='''The input file.''')					 
parser.add_argument('-o', action='store', dest='output_file', type=str,
                         help='''The output file.''')	
parser.add_argument('-s', action='store', dest='sequence', type=str,
                         help='''The sequence column.''')	
parser.add_argument('-f', action='store', dest='format_type', type=str,
                         help='''The output format airr or changeo.''')	
parser.add_argument('--gf', nargs='+', action='store', dest='annotate', default=None,
                        help='Additional fields to use for annotation.')
                        
args = vars(parser.parse_args())

input_file = args['input_file']
output_file = args['output_file']
sequence = args['sequence']
annotate = args['annotate']
format_type = args['format_type']

### Input 
# Must have SEQUENCE_ID column

df = pd.read_csv(input_file, dtype = 'object', sep = '\t')

seq_iter = DataFrameToSeqIter(df, seq_column = sequence, ann_columns = annotate)

df_igblast_raw = neptune.runIgBLAST_MakeDb(seq_iter, format_type = format_type)

df_igblast_raw.to_csv(output_file)