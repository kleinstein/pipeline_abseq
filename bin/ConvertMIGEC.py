#!/usr/bin/env python3
"""
Converts MIGEC headers to presto format
"""
# Info
__author__ = 'Ruoyi Jiang'

from presto.IO import readSeqFile

from Bio import SeqIO
from presto.IO import getFileType

import sys
import os

out_type="fastq"


file_input = sys.argv[1]
file_output = sys.argv[2]

seq_iter = readSeqFile(file_input)

pass_handle = open(file_output, 'w')

#Update the annotation field using the shuffled list
for seq in seq_iter:
    seq.id = seq.id.replace("MIG.","")
    seq.description = ''
    SeqIO.write(seq, pass_handle, out_type)

pass_handle.close()
