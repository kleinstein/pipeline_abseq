#!/usr/bin/env python3
"""
Performs pairwise sequence comparisons within clones to define a threshold
"""
# Info
__author__ = 'Ruoyi Jiang, Namita Gupta, Jason Anthony Vander Heiden, Gur Yaari, Mohamed Uduman'
from changeo import __version__, __date__

# Imports
import os
import re
import sys
import csv
import numpy as np
import pandas as pd         
from math import ceil
from argparse import ArgumentParser
from collections import OrderedDict
from itertools import chain
from textwrap import dedent
from time import time
from Bio import pairwise2
from Bio.Seq import translate

# Presto and changeo imports
from presto.Defaults import default_out_args
from presto.IO import getFileType, getOutputHandle, printLog, printProgress
from presto.Multiprocessing import manageProcesses
from presto.Sequence import getDNAScoreDict
from changeo.Commandline import CommonHelpFormatter, getCommonArgParser, parseCommonArgs
from changeo.Distance import distance_models, calcDistances, formClusters
from changeo.IO import getDbWriter, readDbFile, countDbFile
from changeo.Multiprocessing import DbData

# Import threshold detection methods from sklearn
import scipy.stats as stats
from sklearn import mixture
from sklearn.model_selection import GridSearchCV
from sklearn.neighbors import KernelDensity
from sklearn.cluster import KMeans

# Import custom distance calculator
from dist import Distance

## Set maximum field size for csv.reader
csv.field_size_limit(sys.maxsize)

# Defaults
default_translate = False
default_index_mode = 'gene'
default_index_action = 'set'
default_bygroup_model = 'ham'
default_hclust_model = 'chen2010'
default_seq_field = 'JUNCTION'
default_norm = None
default_sym = 'avg'
default_linkage = 'single'
choices_bygroup_model = ('ham', 'aa', 'hh_s1f', 'hh_s5f', 'mk_rs1nf', 'mk_rs5nf', 'hs1f_compat', 'm1n_compat')
default_dtn = False
default_bin_count = 50


#@ API change for printing np arrays: return open(out_file, 'wb')
def getOutputHandle(in_file, out_label=None, out_dir=None, out_name=None, out_type=None):
    """
    Opens an output file handle

    Arguments:
    in_file = input filename
    out_label = text to be inserted before the file extension;
                  if None do not add a label
    out_type = the file extension of the output file;
                 if None use input file extension
    out_dir = the output directory; if None use directory of input file
    out_name = the short filename to use for the output file;
                 if None use input file short name

    Returns:
    file = File handle
    """
    # Get in_file components
    dir_name, file_name = os.path.split(in_file)
    short_name, ext_name = os.path.splitext(file_name)

    # Define output directory
    if out_dir is None:
        out_dir = dir_name
    else:
        out_dir = os.path.abspath(out_dir)
        if not os.path.exists(out_dir):  os.mkdir(out_dir)
    # Define output file prefix
    if out_name is None:  out_name = short_name
    # Define output file extension
    if out_type is None:  out_type = ext_name.lstrip('.')

    # Define output file name
    if out_label is None:
        out_file = os.path.join(out_dir, '%s.%s' % (out_name, out_type))
    else:
        out_file = os.path.join(out_dir, '%s_%s.%s' % (out_name, out_label, out_type))

    # Open and return handle
    try:
        # TODO:  mode may need to be 'wt'. or need universal_newlines=True all over the place. check tab file parsing.
        return open(out_file, 'wb')
    except:
        sys.exit('ERROR:  File %s cannot be opened' % out_file)
        
        

#@ API change for NumResult
class DbNumResult:
    """
    A class defining numeric result objects for collector processes
    """
    # Instantiation
    def __init__(self, key, records):
        self.id = key
        self.data = records
        self.results = None
        self.valid = False
        self.log = OrderedDict([('ID', key)])
        self.count = None
        
    # Boolean evaluation
    def __bool__(self):
        return self.valid

    # Length evaluation
    def __len__(self):
        if isinstance(self.results, IgRecord):
            return 1
        elif self.results is None:
            return 0
        else:
            return len(self.results)




def indexByIdentity(index, key, rec, fields=None):
    """
    Updates a preclone index with a simple key

    Arguments:
    index = preclone index from indexJunctions
    key = index key
    rec = IgRecord to add to the index
    fields = additional annotation fields to use to group preclones;
             if None use only V, J and junction length

    Returns:
    None. Updates index with new key and records.
    """
    index.setdefault(tuple(key), []).append(rec)


def indexByUnion(index, key, rec, fields=None):
    """
    Updates a preclone index with the union of nested keys

    Arguments:
    index = preclone index from indexJunctions
    key = index key
    rec = IgRecord to add to the index
    fields = additional annotation fields to use to group preclones;
             if None use only V, J and junction length

    Returns:
    None. Updates index with new key and records.
    """
    # List of values for this/new key
    val = [rec]
    f_range = list(range(2, 3 + (len(fields) if fields else 0)))

    # See if field/junction length combination exists in index
    outer_dict = index
    for field in f_range:
        try:
            outer_dict = outer_dict[key[field]]
        except (KeyError):
            outer_dict = None
            break
    # If field combination exists, look through Js
    j_matches = []
    if outer_dict is not None:
        for j in outer_dict.keys():
            if not set(key[1]).isdisjoint(set(j)):
                key[1] = tuple(set(key[1]).union(set(j)))
                j_matches += [j]
    # If J overlap exists, look through Vs for each J
    for j in j_matches:
        v_matches = []
        # Collect V matches for this J
        for v in outer_dict[j].keys():
            if not set(key[0]).isdisjoint(set(v)):
                key[0] = tuple(set(key[0]).union(set(v)))
                v_matches += [v]
        # If there are V overlaps for this J, pop them out
        if v_matches:
            val += list(chain(*(outer_dict[j].pop(v) for v in v_matches)))
            # If the J dict is now empty, remove it
            if not outer_dict[j]:
                outer_dict.pop(j, None)

    # Add value(s) into index nested dictionary
    outer_dict = index
    for field in f_range:
        outer_dict.setdefault(key[field], {})
        outer_dict = outer_dict[key[field]]
    # Add J, then V into index
    if key[1] in outer_dict:
        outer_dict[key[1]].update({key[0]: val})
    else:
        outer_dict[key[1]] = {key[0]: val}


def indexJunctions(db_iter, fields=None, mode=default_index_mode,
                   action=default_index_action):
    """
    Identifies preclonal groups by V, J and junction length

    Arguments: 
    db_iter = an iterator of IgRecords defined by readDbFile
    fields = additional annotation fields to use to group preclones;
             if None use only V, J and junction length
    mode = specificity of alignment call to use for assigning preclones;
           one of ('allele', 'gene')
    action = how to handle multiple value fields when assigning preclones;
             one of ('first', 'set')
    
    Returns: 
    a dictionary of {(V, J, junction length):[IgRecords]}
    """
    # print(fields)
    # Define functions for grouping keys
    if mode == 'allele' and fields is None:
        def _get_key(rec, act):
            return [rec.getVAllele(act), rec.getJAllele(act),
                    None if rec.junction is None else len(rec.junction)]
    elif mode == 'gene' and fields is None:
        def _get_key(rec, act):  
            return [rec.getVGene(act), rec.getJGene(act),
                    None if rec.junction is None else len(rec.junction)]
    elif mode == 'allele' and fields is not None:
        def _get_key(rec, act):
            vdj = [rec.getVAllele(act), rec.getJAllele(act),
                    None if rec.junction is None else len(rec.junction)]
            ann = [rec.toDict().get(k, None) for k in fields]
            return list(chain(vdj, ann))
    elif mode == 'gene' and fields is not None:
        def _get_key(rec, act):
            vdj = [rec.getVGene(act), rec.getJGene(act),
                    None if rec.junction is None else len(rec.junction)]
            ann = [rec.toDict().get(k, None) for k in fields]
            return list(chain(vdj, ann))

    # Function to flatten nested dictionary
    def _flatten_dict(d, parent_key=''):
        items = []
        for k, v in d.items():
            new_key = parent_key + [k] if parent_key else [k]
            if isinstance(v, dict):
                items.extend(_flatten_dict(v, new_key).items())
            else:
                items.append((new_key, v))
        flat_dict = {None if None in i[0] else tuple(i[0]): i[1] for i in items}
        return flat_dict

    if action == 'first':
        index_func = indexByIdentity
    elif action == 'set':
        index_func = indexByUnion
    else:
        sys.stderr.write('Unrecognized action: %s.\n' % action)

    start_time = time()
    clone_index = {}
    rec_count = 0
    for rec in db_iter:
        key = _get_key(rec, action)

        # Print progress
        if rec_count == 0:
            print('PROGRESS> Grouping sequences')

        printProgress(rec_count, step=1000, start_time=start_time)
        rec_count += 1

        # Assigned passed preclone records to key and failed to index None
        if all([k is not None and k != '' for k in key]):
            # Update index dictionary
            index_func(clone_index, key, rec, fields)
        else:
            clone_index.setdefault(None, []).append(rec)

    printProgress(rec_count, step=1000, start_time=start_time, end=True)

    if action == 'set':
        clone_index = _flatten_dict(clone_index)

    return clone_index

def distanceClonesHist(records,
                    model=default_bygroup_model,
                    dist_mat=None, norm=default_norm, sym=default_sym,
                    linkage=default_linkage, seq_field=default_seq_field):
    """
    Calculates distances among sequences of a potential clone

    Arguments: 
    records = an iterator of IgRecords
    model = substitution model used to calculate distance
    dist_mat = pandas DataFrame of pairwise nucleotide or amino acid distances
    norm = normalization method
    sym = symmetry method
    linkage = type of linkage
    seq_field = sequence field used to calculate distance between records

    Returns: 
    a list containing the distance distribution for sequences within that potential clone
    """
    # Get distance matrix if not provided
    if dist_mat is None:
        try:
            dist_mat = distance_models[model]
        except KeyError:
            sys.exit('Unrecognized distance model: %s' % args_dict['model'])

    # TODO:  can be cleaned up with abstract model class
    # Determine length of n-mers
    if model in ['hs1f_compat', 'm1n_compat', 'aa', 'ham', 'hh_s1f', 'mk_rs1nf']:
        nmer_len = 1
    elif model in ['hh_s5f', 'mk_rs5nf']:
        nmer_len = 5
    else:
        sys.exit('Unrecognized distance model: %s.\n' % model)

    # Function for converting sequences to a form that can be accepted by calcDistances
    def _convert_seq(ig):
        seq = ig.getSeqField(seq_field)
        seq = re.sub('[\.-]', 'N', str(seq))
        if model == 'aa':  seq = translate(seq)
        return seq

    seq_field_list = list(_convert_seq(ig) for ig in records)
    
    # If there is only one unique sequence, break and return None
    if len(set(seq_field_list)) is 1:
        return None
        
    # Calculate pairwise distance matrix
    dists = calcDistances(seq_field_list, nmer_len, dist_mat, norm, sym)

    # Convert pairwise distance matrix into a hist distribution
    return None
    
    # # Perform hierarchical clustering
    # clusters = formClusters(dists, linkage, distance)
    #
    # # Turn clusters into clone dictionary
    # clone_dict = {}
    # for i, c in enumerate(clusters):
    #     clone_dict.setdefault(c, []).extend(seq_map[seqs[i]])
    #
    # return clone_dict
      

def histogramFloatDistMatrix(array, max_dist=1, triangle=False, DistToNearest=default_dtn, norm=default_norm, \
                            bins=default_bin_count, nozero=True):
    """
    Histograms the results of a distance matrix array

    Arguments:
    array = distance matrix in the form of an np.array
    max_dist = required only when norm is not 'len', maximum plausible distanc between records for histogram binning
    triangle = no I/O way to modify currently, if true, uses the upper triangular part of the distance matrix for values
    DistToNearest = if true, histograms the distance to nearest value rather than all values in the matrix
    norm = normalization choice, only relevant insofar as it is 'len' or not 'len'
    bins = number of bins to use if norm is 'len', otherwise irrelevant
    nonzero = no I/O way to modify currently, specifies whether DTN dist between identical sequences should be histogrammed

    Returns:
    output_hist = The output histogram distribution, a numpy array.
    """
    #we assume the input array is a square matrix/array
    n = len(array)
    
    #check that the array is not empty
    if not n:
        raise Exception('barcode/annotation group with no sequences encountered')
    
    #flattens whole or upper triangular part of matrix (distToNearest optional)
    output = []
    if DistToNearest:
        if triangle:
            output = [0]*(n-1)
            for i in range(n-1):
                output[i] = min(array[i,i+1:])
        else:
            output = [0]*(n)
            for i in range(n):
                if nozero:
                    try:
                        output[i] = min(d for d in np.append(array[i,:i],array[i,i+1:]) if d != 0.0)
                    except:
                        output[i] = 0.0
                else:
                    output[i] = min(np.append(array[i,:i],array[i,i+1:]))
    else:
        if triangle:
            output = [0]*int((n*(n-1))/2)
            prev_index=0
            for i in range(n):
                for j in range(i+1,n):
                    output[prev_index] = array[i,j]
                    prev_index += 1
        else:
            output = [0]*n*n
            for i in range(n):
                for j in range(n):
                    output[i*n + j] = array[i,j]
    
    #select binning method
    if norm == 'len':
        output_hist = list(np.histogram(output, bins=bins, range = (0.0, 1.0))[0])
    else:
        output_hist = list(np.histogram(output, bins=list(range(max_dist)))[0])
    
    return output_hist


def findThreshold(pd, normal=default_norm, bins=default_bin_count):
    """
    Finds the threshold from a probability distribution using GMM, KDE

    Arguments:
    pd = a proability distribution (e.g. histogram of distances from a distance matrix)
    normal = the type of normalization used, relevant only if 'len' or not 'len' was used
    bins = only relevant if 'len' was specified, the number of bins used to generate the histogram

    Returns:
    A dict of values representing the result from gmm, kde, kmeans
    """
    
    #finds the points of intersection between two gaussians (the corresponding points on the x axis) 
    def _intersectGaussian(o1, o2, m1,m2,std1,std2):
        a = 1/(2*std1**2) - 1/(2*std2**2)
        b = m2/(std2**2) - m1/(std1**2)
        c = m1**2 /(2*std1**2) - m2**2 / (2*std2**2) - np.log((o1*std2)/(o2*std1))
        return np.roots([a,b,c])

    #returns the first value in a list between val1 and val2
    def _findBetween(list, val1, val2):
        for item in list:
            if (val1 < item < val2) or (val2 < item < val1):
                return item

    #converts a histogram/probability distribution to a format that can be read by sklearn
    def _PD2Distribution(pd_array):
    	output = []
    	for i in range(len(pd_array)):
    		output += int(pd_array[i]) * [i]
    	return np.array([output])
     
    #finds the threshold (not normalized) from a KDE smoothed distribution
    def _findKDEThreshold(arr):
        diff_arr = np.gradient(arr)
        set = 0
        for i in range(len(diff_arr)):
            if diff_arr[i] < 0: 
                set = 1
            elif diff_arr[i] > 0 and set is 1:
                return i
        return len(diff_arr)
    
    #finds the boundary between 0 and 1 in an array of 0s and 1s
    def _findBoundary(arr):
        if len(arr) < 2:
            return 0
        else:
            for i in range(len(arr)-1):
                if arr[i] == 0 and arr[i+1] == 1 or arr[i] == 1 and arr[i+1] == 0:
                    return i
        return None
        
    #GMM
    try: 
        #sklearn GMM
        distribution = _PD2Distribution(pd)
        g = mixture.GaussianMixture(n_components=2)
        g.fit(np.transpose(distribution))

        #obtain standard intersect
        intersect_pts = _intersectGaussian(g.weights_[0], g.weights_[1], g.means_[0][0], g.means_[1][0], \
                np.sqrt(g.covariances_[0][0][0]), np.sqrt(g.covariances_[1][0][0]))
        threshold = _findBetween(intersect_pts, g.means_[0][0], g.means_[1][0])
    
        #obtain intersect that maximizes spec and sens
        ss_intersect_pts = _intersectGaussian(np.sqrt(g.covariances_[0][0][0]), np.sqrt(g.covariances_[1][0][0]), g.means_[0][0], g.means_[1][0], \
                np.sqrt(g.covariances_[0][0][0]), np.sqrt(g.covariances_[1][0][0]))
        ss_threshold = _findBetween(ss_intersect_pts, g.means_[0][0], g.means_[1][0])
        
    except:
        print("Failed gmm.")
        raise

    #extract the relevant stat values
    if g.means_[0][0] > g.means_[1][0]:
        scale1 = g.weights_[1]
        scale2 = g.weights_[0]
        mean1 = g.means_[1][0]
        mean2 = g.means_[0][0]
        std1 = np.sqrt(g.covariances_[1][0][0])
        std2 = np.sqrt(g.covariances_[0][0][0])
    else:
        scale1 = g.weights_[0]
        scale2 = g.weights_[1]
        mean1 = g.means_[0][0]
        mean2 = g.means_[1][0]
        std1 = np.sqrt(g.covariances_[0][0][0])
        std2 = np.sqrt(g.covariances_[1][0][0])
    
    #finds the local minimum between the two means (np.argmin returns the first minima if multiple minima occur)
    local_min = int(mean1) + np.argmin(pd[int(mean1):ceil(mean2)])
    
    #KDE   
    try:
        #sklearn, scipy stats gaussian KDE using cross validation
        if normal == 'len':
            x_grid = np.linspace(0, bins, bins)
        else:
            x_grid = np.linspace(0, len(pd), len(pd))
            
        #np.set_printoptions(threshold=np.inf)
        x = np.array(distribution[0])
        
        kde = stats.gaussian_kde(x)
        
        #obtain cross-validated bandwith using GridSearchCV (similar to h.ucv)
        # grid = GridSearchCV(KernelDensity(), {'bandwidth': np.linspace(0.1, 1.0, 20)}, cv=10)
        # grid.fit(x[:, None])
        # print("fitting optimal bandwith")
        # best_bw = grid.best_params_['bandwidth']
        #
        # #return kde for distribution
        # kde.set_bandwidth(bw_method=best_bw)
        # result = kde(x_grid)
        
        #using reference rules (not implemented)
        kde.set_bandwidth(bw_method='scott')
        result = kde(x_grid)
        
        kde_thresh = _findKDEThreshold(result)
    except:
        print("Failed kde.")
        raise
          
    #Kmeans
    try:
        x = np.array(distribution[0])
        
        #cluster
        kmeans = KMeans(n_clusters=2, random_state=0).fit(x[:,None])
        
        #return the boundary between the two clusters
        boundary = _findBoundary(kmeans.labels_)
        
        #use the average of the two values at either side of the boundary as the threshold
        kmeans_thresh = (x[boundary] + x[boundary+1])/2
    except:
        print("Failed kmeans.")
        raise  
        
    #specify output for 'len' vs non 'len' normalized inputs
    if normal == 'len':
        return {
                'thresh': threshold*(1.0/bins), 
                'min': local_min*(1.0/bins), 
                'sens-spec': ss_threshold*(1.0/bins), 
                'scale': (scale1*(1.0/bins), scale2*(1.0/bins)), 
                'means': (mean1*(1.0/bins), mean2*(1.0/bins)), 
                'std': (std1*(1.0/bins), std2*(1.0/bins)), 
                'kde-thresh': kde_thresh*(1.0/bins), 
                'kmeans-thresh': kmeans_thresh*(1.0/bins)
            }
    else:
        return {
                'thresh': threshold, 
                'min': local_min, 
                'sens-spec': ss_threshold, 
                'scale': (scale1, scale2), 
                'means': (mean1, mean2), 
                'std': (std1, std2), 
                'kde-thresh': kde_thresh, 
                'kmeans-thresh': kmeans_thresh
            }
                

# TODO:  Merge duplicate feed, process and collect functions.
def feedQueue(alive, data_queue, db_file, group_func, group_args={}):
    """
    Feeds the data queue with Ig records

    Arguments: 
    alive = a multiprocessing.Value boolean controlling whether processing continues
            if False exit process
    data_queue = a multiprocessing.Queue to hold data for processing
    db_file = the Ig record database file
    group_func = the function to use for assigning preclones
    group_args = a dictionary of arguments to pass to group_func
    
    Returns: 
    None
    """
    # Open input file and perform grouping
    try:
        # Iterate over Ig records and assign groups
        db_iter = readDbFile(db_file)
        clone_dict = group_func(db_iter, **group_args)
        
        #print("SIZE",len(clone_dict))
    except:
        #sys.stderr.write('Exception in feeder grouping step\n')
        alive.value = False
        raise
    
    # Add groups to data queue
    try:
        #print 'START FEED', alive.value
        # Iterate over groups and feed data queue
        clone_iter = iter(clone_dict.items())
        while alive.value:
            # Get data from queue
            if data_queue.full():  continue
            else:  data = next(clone_iter, None)
            # Exit upon reaching end of iterator
            if data is None:  break
            #print "FEED", alive.value, k
            
            # Feed queue
            data_queue.put(DbData(*data))
        else:
            sys.stderr.write('PID %s:  Error in sibling process detected. Cleaning up.\n' \
                             % os.getpid())
            return None
    except:
        #sys.stderr.write('Exception in feeder queue feeding step\n')
        alive.value = False
        raise

    return None


def processQueue(alive, data_queue, result_queue, dist_matrix, clone_args={}):
    """
    Pulls from data queue, performs calculations, and feeds results queue

    Arguments: 
    alive = a multiprocessing.Value boolean controlling whether processing continues
            if False exit process
    data_queue = a multiprocessing.Queue holding data to process
    result_queue = a multiprocessing.Queue to hold processed results
    dist_matrix = Distance class object for distance calculations 
    clone_args = a dictionary of arguments for clone assignment

    Returns: 
    None
    """
    seq_field = clone_args['seq_field']
    
    def _convert_seq(ig):
        seq = ig.getSeqField(seq_field)
        seq = re.sub('[\.-]', 'N', str(seq))
        #if model == 'aa':  seq = translate(seq)
        return seq
    
    try:
        # Iterator over data queue until sentinel object reached
        while alive.value:
            # Get data from queue
            if data_queue.empty():  continue
            else:  data = data_queue.get()
            # Exit upon reaching sentinel
            if data is None:  break

            # Define list of sequences for clone assignment in rec_list
            rec_list = data.data
            rec_id = data.id
            rec_count = len(rec_list)
            #print(rec_id, rec_count)
            
            # Generate the return object
            result = DbNumResult(rec_id, rec_list)
            result.count = rec_count       

            # Check for invalid data (due to failed indexing) and add failed result
            if not data:
                result_queue.put(result)
                continue
                
            # Return the junction length(s) in rec_list (r.junction can be false)
            rec_lengths = sorted(list(set([(len(r.junction) or 0) for r in rec_list])))

            # Add V(D)J to log
            result.log['ID'] = ','.join([str(x) for x in data.id])
            result.log['VALLELE'] = ','.join(set([(r.getVAllele() or '') for r in rec_list]))
            result.log['DALLELE'] = ','.join(set([(r.getDAllele() or '') for r in rec_list]))
            result.log['JALLELE'] = ','.join(set([(r.getJAllele() or '') for r in rec_list]))
            result.log['JUNCLEN'] = ','.join(str(l) for l in rec_lengths)
            result.log['SEQUENCES'] = rec_count
            
            #default hist value
            hist = None

            # Proceed if (1) there is more than 1 record (2) the records have the same length \
            #(3) that record length is not 0 (a clone but no point obtaining a dist matrix). (2) and (3) rarely happen
            if rec_count > 1 and len(rec_lengths) is 1 and rec_lengths[0] is not 0:
                #dists.calcPairwiseHam(rec_list)
                seq_field_list = list(_convert_seq(ig) for ig in rec_list)
                dist = dist_matrix.calcPairwiseHam(seq_field_list)
                #if distance matrix is not None OR not all zeroes (ie for N characters)
                if dist is not None and np.any(dist):
                    hist = histogramFloatDistMatrix(dist, rec_lengths[0], DistToNearest = clone_args['dtn'],\
                         norm = clone_args['norm'], bins = clone_args['bins'])
                
                del seq_field_list
            
            #print(rec_id, rec_count)
            
            # Feed results to result queue
            #result.valid = False
            result.valid = True if hist else False
            result.results = {'id' : rec_id, 'hist': hist}
            result_queue.put(result)
                
        else:
            sys.stderr.write('PID %s:  Error in sibling process detected. Cleaning up.\n' \
                             % os.getpid())
            return None
    except:
        #sys.stderr.write('Exception in worker\n')
        alive.value = False
        raise
    
    return None


def collectQueue(alive, result_queue, collect_queue, db_file, out_args, clone_args={}):
    """
    Assembles results from a queue of individual sequence results and manages log/file I/O

    Arguments: 
    alive = a multiprocessing.Value boolean controlling whether processing continues
            if False exit process
    result_queue = a multiprocessing.Queue holding processQueue results
    collect_queue = a multiprocessing.Queue to store collector return values
    db_file = the input database file name
    out_args = common output argument dictionary from parseCommonArgs
    clone_args = arguments used for generating the histogram and threshold
    
    Returns: 
    None
    (adds 'log' and 'out_files' to collect_dict)
    """
    #converts dict of histograms to two numpy arrays, one with the sums, another concatenated
    def _dict2arr(input_dict, long):
        sum_arr = np.array([0]*long)
        hist_arr = np.array([str(('V', 'J', 'L'))] + [0]*long)
        for k,v in hist_dict.items():
        	row = np.array(v + [0]*(long - len(v)))
        	hist_arr = np.vstack((hist_arr,  np.append(k, row)))
        	sum_arr += row
        return {'hist': hist_arr, 'sum': sum_arr}
    
    # Open output files
    try:
        # Count sets in file
        #This takes a very long time
        result_count = countDbFile(db_file)

        hist_dict = dict()
        longest_hist = 0

        # Open log file
        if out_args['log_file'] is None:
            log_handle = None
        else:
            log_handle = open(out_args['log_file'], 'w')
    except:
        alive.value = False
        raise
        

    # Get results from queue and write to files
    try:
        #print 'START COLLECT', alive.value
        # Iterator over results queue until sentinel object reached
        start_time = time()
        rec_count = clone_count = pass_count = fail_count = 0
        print('PROGRESS> Assigning clones')
        
        while alive.value:
            # Get result from queue
            if result_queue.empty():  continue
            else:  result = result_queue.get()
            # Exit upon reaching sentinel
            if result is None:  break
            
            # Print progress for previous iteration and update record count
            printProgress(rec_count, result_count, 0.05, start_time) 
        
            clone_count += 1
            ##@@ Buggy, why does printProgress not print smoothly?
            ##print(str(rec_count) + ' ' + str(result_count))
            rec_count += result.count

            result_id = result.results['id']
            result_hist = result.results['hist']
            
            if result_hist is not None:
                hist_dict[str(result_id)] = result_hist
                pass_count += 1
                longest_hist = max(longest_hist, len(result_hist))
            else:
                # fail if a result is a single sequence UMI group (no histogram)
                fail_count += 1
             
            # Write log
            printLog(result.log, handle=log_handle)
        else:
            sys.stderr.write('PID %s:  Error in sibling process detected. Cleaning up.\n' \
                             % os.getpid())
            return None
        
        # Print total counts
        printProgress(rec_count, result_count, 0.05, start_time)
        
        result_hists = _dict2arr(hist_dict, longest_hist)

        #Check if threshold can be found from sum_df. Generate input for the function (list)
        sum_hist = result_hists['sum']
        log_sum_hist = np.log(sum_hist+1)
        
        # Generate log
        log = OrderedDict()
        
        try:
            
            # Output log
            for i in range(2): 
                log['OUTPUT%i' % (i + 1)] = None
            log['SETS'] = clone_count
            log['PASS'] = pass_count
            log['FAIL'] = fail_count
            
            log['HIST'] = sum_hist
            log['LOG-HIST'] = log_sum_hist
            
            # Find thresholds using gmm
            thresh_result = findThreshold(sum_hist, normal=clone_args['norm'], bins=clone_args['bins'])
            log_thresh_result = findThreshold(log_sum_hist, normal=clone_args['norm'], bins=clone_args['bins'])
            
            log['THRESH-KDE'] = thresh_result['kde-thresh']
            log['THRESH-KMEANS'] = thresh_result['kmeans-thresh']
            log['THRESH-GMM'] = thresh_result['thresh']
            log['THRESH-MIN'] = thresh_result['min']
            log['THRESH-SS'] = thresh_result['sens-spec']
            log['THRESH-SCALE'] = thresh_result['scale']
            log['THRESH-MEANS'] = thresh_result['means']
            log['THRESH-STD'] = thresh_result['std']
            
            log['LOG-THRESH-KDE'] = log_thresh_result['kde-thresh']
            log['LOG-THRESH-KMEANS'] = log_thresh_result['kmeans-thresh']
            log['LOG-THRESH-GMM'] = log_thresh_result['thresh']
            log['LOG-THRESH-MIN'] = log_thresh_result['min']
            log['LOG-THRESH-SS'] = log_thresh_result['sens-spec']
            log['LOG-THRESH-SCALE'] = log_thresh_result['scale']
            log['LOG-THRESH-MEANS'] = log_thresh_result['means']
            log['LOG-THRESH-STD'] = log_thresh_result['std']
            
            # Organize arrays to print
            indexes=np.array(['THRESH-GMM', 'THRESH-MIN', 'THRESH-SS', 'THRESH-KDE', 'THRESH-KMEANS',
                'LOG-THRESH-GMM', 'LOG-THRESH-MIN', 'LOG-THRESH-SS', 'LOG-THRESH-KDE', 'LOG-THRESH-KMEANS'])
            threshes = np.array([thresh_result['thresh'], thresh_result['min'], \
                thresh_result['sens-spec'], thresh_result['kde-thresh'], thresh_result['kmeans-thresh'], \
                log_thresh_result['thresh'], log_thresh_result['min'], \
                log_thresh_result['sens-spec'], log_thresh_result['kde-thresh'], log_thresh_result['kmeans-thresh']])
            thresh_arr = np.hstack((indexes[:,None], threshes[:,None]))
                
            # Build results dictionary
            assembled = {'hist': result_hists['hist'], 'sum': result_hists['sum'], 'log-sum': log_sum_hist, 'thresh': thresh_arr}
            
        except:
            # If the threshold method fails
            log['SETS'] = clone_count
            log['THRESHOLD'] = 'FAILED'
            assembled = {'hist': result_hists['hist'], 'sum': result_hists['sum'], 'log-sum': log_sum_hist}
            raise

        # Write assembled error counts to output files
        out_files = writeResults(assembled, db_file, out_args)
    
        # Update log
        for i, f in enumerate(out_files):
            log['OUTPUT%i' % (i + 1)] = os.path.basename(f)

        # Update collector results
        collect_dict = {'log':log, 'out_files': out_files}
        collect_queue.put(collect_dict)
        
    except:
        #sys.stderr.write('Exception in collector result processing step\n')
        alive.value = False
        raise

    return None
    

def writeResults(results, seq_file, out_args):
    """
    Formats results and writes to output files

    Arguments: 
    results = assembled results dictionary
    seq_file = the sample sequence file name
    out_args = common output argument dictionary from parseCommonArgs

    Returns:
    a tuple of file names
    """
    hist_arr = results['hist']
    sum_arr = np.array([results['sum']])
    log_sum_arr = np.array([results['log-sum']])
    
    # Write to comma delimited files
    file_args = {'out_dir':out_args['out_dir'], 'out_name':out_args['out_name'], 'out_type':'tab'}
    
    try:
        thresh_arr = results['thresh']

        with getOutputHandle(seq_file, 'dist-all', **file_args) as hist_handle, \
                getOutputHandle(seq_file, 'dist-sum', **file_args) as sum_handle, \
                getOutputHandle(seq_file, 'dist-log-sum', **file_args) as log_sum_handle, \
                getOutputHandle(seq_file, 'dist-thresh', **file_args) as thresh_handle: \

            np.savetxt(hist_handle, hist_arr, delimiter='\t', fmt='%s')
            np.savetxt(sum_handle, sum_arr, delimiter='\t', fmt='%d')
            np.savetxt(log_sum_handle, log_sum_arr, delimiter='\t', fmt='%f')
            np.savetxt(thresh_handle, thresh_arr, delimiter='\t', fmt='%s')

        return (hist_handle.name, sum_handle.name, log_sum_handle.name, thresh_handle.name)
    
    #if no threshold was found 
    except KeyError:
        with getOutputHandle(seq_file, 'dist-all', **file_args) as hist_handle, \
                getOutputHandle(seq_file, 'dist-log-sum', **file_args) as log_sum_handle, \
                getOutputHandle(seq_file, 'dist-sum', **file_args) as sum_handle: \

            np.savetxt(hist_handle, hist_arr,  delimiter='\t', fmt='%s')
            np.savetxt(sum_handle, sum_arr,  delimiter='\t', fmt='%d')
            np.savetxt(log_sum_handle, log_sum_arr,  delimiter='\t', fmt='%f')

        return (hist_handle.name, sum_handle.name, log_sum_handle.name)
    
    

    
def defineCloneThreshold(db_file, feed_func, work_func, collect_func, 
                 group_func=None, group_args={}, clone_args={},
                 out_args=default_out_args, nproc=None, queue_size=None):
    """
    Define clonally related sequences
    
    Arguments:
    db_file = filename of input database
    feed_func = the function that feeds the queue
    work_func = the worker function that will run on each CPU
    collect_func = the function that collects results from the workers
    group_func = the function to use for assigning preclones
    group_args = a dictionary of arguments to pass to group_func
    clone_args = a dictionary of arguments for clonal assignment
    out_args = common output argument dictionary from parseCommonArgs
    nproc = the number of processQueue processes; if None defaults to the number of CPUs
    queue_size = maximum size of the argument queue; if None defaults to 2*nproc    
    
    Returns:
    a list of successful output file names
    """
    #private function to instantiate the distance calculator
    def _createDistCalculator(pd_distance_model):
        
        height_nt = list(pd_distance_model.index)
        height_nmer = len(list(pd_distance_model.index)[0])
        width_nt = list(pd_distance_model.columns)
        width_nmer = len(list(pd_distance_model.columns)[0])

        matrix = np.array(pd_distance_model)

        return Distance(matrix = matrix, height_nt = height_nt,
                width_nt = width_nt, height_nmer = height_nmer, 
                width_nmer = width_nmer, sym = clone_args['sym'], norm = clone_args['norm'])  
                 
    # Print parameter info
    log = OrderedDict()
    log['START'] = 'DefineClones'
    log['DB_FILE'] = os.path.basename(db_file)
    if group_func is not None:
        log['GROUP_FUNC'] = group_func.__name__
        log['GROUP_ARGS'] = group_args
    #@ log['CLONE_FUNC'] = clone_func.__name__

    # TODO:  this is yucky, but can be fixed by using a model class
    clone_log = clone_args.copy()
    if 'dist_mat' in clone_log:  del clone_log['dist_mat']
    log['CLONE_ARGS'] = clone_log

    log['NPROC'] = nproc
    printLog(log)
    
    # Instantiate the distance calculator
    pd_distance_model = distance_models[clone_args['model']]
    dist_matrix = _createDistCalculator(pd_distance_model)
    
    # Define feeder function and arguments
    feed_args = {'db_file': db_file,
                 'group_func': group_func, 
                 'group_args': group_args}
    # Define worker function and arguments
    work_args = {'clone_args': clone_args,
                 'dist_matrix': dist_matrix}
    # Define collector function and arguments
    collect_args = {'db_file': db_file,
                    'out_args': out_args,
                    'clone_args': clone_args}
    
    # Call process manager
    result = manageProcesses(feed_func, work_func, collect_func, 
                             feed_args, work_args, collect_args, 
                             nproc, queue_size)
        
    # Print log
    result['log']['END'] = 'DefineClones'
    printLog(result['log'])
    
    if result['out_files']:
        return result['out_files']
    else:
        return None


def getArgParser():
    """
    Defines the ArgumentParser

    Arguments: 
    None
                      
    Returns: 
    an ArgumentParser object
    """
    # Define input and output fields
    fields = dedent(
             '''
             output files:
                 clone-pass
                     database with assigned clonal group numbers.
                 clone-fail
                     database with records failing clonal grouping.

             required fields:
                 SEQUENCE_ID, V_CALL or V_CALL_GENOTYPED, D_CALL, J_CALL, JUNCTION

                 <field>
                     sequence field specified by the --sf parameter
                
             output fields:
                 CLONE
              ''')

    # Define ArgumentParser
    parser = ArgumentParser(description=__doc__, epilog=fields,
                            formatter_class=CommonHelpFormatter)
    parser.add_argument('--version', action='version',
                        version='%(prog)s:' + ' %s-%s' %(__version__, __date__))
    subparsers = parser.add_subparsers(title='subcommands', dest='command', metavar='',
                                       help='Cloning method')
    # TODO:  This is a temporary fix for Python issue 9253
    subparsers.required = True
    
    # Parent parser    
    parser_parent = getCommonArgParser(seq_in=False, seq_out=False, db_in=True, 
                                       multiproc=True)
    
    # Distance cloning method
    parser_bygroup = subparsers.add_parser('bygroup', parents=[parser_parent],
                                           formatter_class=CommonHelpFormatter,
                                           help='''Defines clones as having same V assignment,
                                                J assignment, and junction length with
                                                specified substitution distance model.''',
                                           description='''Defines clones as having same V assignment,
                                                       J assignment, and junction length with
                                                       specified substitution distance model.''')
    parser_bygroup.add_argument('-f', nargs='+', action='store', dest='fields', default=None,
                             help='Additional fields to use for grouping clones (non VDJ)')
    parser_bygroup.add_argument('--mode', action='store', dest='mode', 
                             choices=('allele', 'gene'), default=default_index_mode,
                             help='''Specifies whether to use the V(D)J allele or gene for
                                  initial grouping.''')
    parser_bygroup.add_argument('--act', action='store', dest='action',
                             choices=('first', 'set'), default=default_index_action,
                             help='''Specifies how to handle multiple V(D)J assignments
                                  for initial grouping.''')
    parser_bygroup.add_argument('--model', action='store', dest='model', 
                             choices=choices_bygroup_model,
                             default=default_bygroup_model,
                             help='''Specifies which substitution model to use for calculating distance
                                  between sequences. The "ham" model is nucleotide Hamming distance and
                                  "aa" is amino acid Hamming distance. The "hh_s1f" and "hh_s5f" models are
                                  human specific single nucleotide and 5-mer content models, respectively,
                                  from Yaari et al, 2013. The "mk_rs1nf" and "mk_rs5nf" models are
                                  mouse specific single nucleotide and 5-mer content models, respectively,
                                  from Cui et al, 2016. The "m1n_compat" and "hs1f_compat" models are
                                  deprecated models provided backwards compatibility with the "m1n" and
                                  "hs1f" models in Change-O v0.3.3 and SHazaM v0.1.4. Both
                                  5-mer models should be considered experimental.''')
    #@ parser_bygroup.add_argument('--dist', action='store', dest='distance', type=float,
#                              default=default_distance,
#                              help='The distance threshold for clonal grouping')
    parser_bygroup.add_argument('--norm', action='store', dest='norm',
                             choices=('len', 'mut', 'none'), default=default_norm,
                             help='''Specifies how to normalize distances. One of none
                                  (do not normalize), len (normalize by length),
                                  or mut (normalize by number of mutations between sequences).''')
    parser_bygroup.add_argument('--sym', action='store', dest='sym',
                             choices=('avg', 'min'), default=default_sym,
                             help='''Specifies how to combine asymmetric distances. One of avg
                                  (average of A->B and B->A) or min (minimum of A->B and B->A).''')
    parser_bygroup.add_argument('--link', action='store', dest='linkage',
                             choices=('single', 'average', 'complete'), default=default_linkage,
                             help='''Type of linkage to use for hierarchical clustering.''')
    parser_bygroup.add_argument('--sf', action='store', dest='seq_field',
                                default=default_seq_field,
                                help='''The name of the field to be used to calculate
                                     distance between records''')
    parser_bygroup.add_argument('--dtn', action='store_true', dest='dtn',
                                default=False,
                                help='''True/False. If true, uses distance to nearest to find the threshold.''')
    parser_bygroup.add_argument('--bins', action='store', dest='bins',
                                default=default_bin_count,
                                help='''A quantity. Stores the number of bins for the histogram when length norm is set.''')
                    
    parser_bygroup.set_defaults(feed_func=feedQueue)
    parser_bygroup.set_defaults(work_func=processQueue)
    parser_bygroup.set_defaults(collect_func=collectQueue)  
    parser_bygroup.set_defaults(group_func=indexJunctions)  
    #@ parser_bygroup.set_defaults(clone_func=distanceClonesHist)
    
    return parser


if __name__ == '__main__':
    """
    Parses command line arguments and calls main function
    """
    # Parse arguments
    parser = getArgParser()
    args = parser.parse_args()
    args_dict = parseCommonArgs(args)
    # Convert case of fields
    if 'seq_field' in args_dict:
        args_dict['seq_field'] = args_dict['seq_field'].upper()
    if 'fields' in args_dict and args_dict['fields'] is not None:  
        args_dict['fields'] = [f.upper() for f in args_dict['fields']]
    
    # Define clone_args
    if args.command == 'bygroup':
        args_dict['group_args'] = {'fields': args_dict['fields'],
                                   'action': args_dict['action'], 
                                   'mode':args_dict['mode']}
        args_dict['clone_args'] = {'model':  args_dict['model'],
                                   'norm': args_dict['norm'],
                                   'sym': args_dict['sym'],
                                   'linkage': args_dict['linkage'],
                                   'seq_field': args_dict['seq_field'],
                                   'dtn':  args_dict['dtn'],
                                   'bins': args_dict['bins']}

    # Get distance matrix
    try:
        args_dict['clone_args']['dist_mat'] = distance_models[args_dict['model']]
    except KeyError:
        sys.exit('Unrecognized distance model: %s' % args_dict['model'])

    del args_dict['fields']
    del args_dict['action']
    del args_dict['mode']
    del args_dict['model']
    del args_dict['norm']
    del args_dict['sym']
    del args_dict['linkage']
    del args_dict['seq_field']
    del args_dict['dtn']
    del args_dict['bins']

    # Call defineClones
    del args_dict['command']
    del args_dict['db_files']
    for f in args.__dict__['db_files']:
        args_dict['db_file'] = f
        defineCloneThreshold(**args_dict)
