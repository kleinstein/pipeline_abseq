#!/usr/bin/env python3
"""
Translates SEQUENCE_IMGT and quantifies glycosylation motif frequencies
"""

# Info
__author__ = 'Ruoyi Jiang'
__date__ = 'April 18, 2019'

# Imports
import pandas as pd
import re
import sys
sys.path.append('/home/ruoyi/Dropbox/hg/pipeline_abseq/api/neptune')
import neptune

# Parse arguments
in_file = sys.argv[1]
out_file = sys.argv[2]

# Define function for identifying glycosylation sites
def countGlycosylation(sequence):
    if len(sequence) > 2:
        return len(re.findall('N[^P^X][ST]', sequence))
    else:
        return 0

# Read in and perform computations
samples_df = pd.read_csv(in_file, dtype = 'object', sep = '\t')

samples_df['V_SEQUENCE_AA'] = \
    samples_df.apply(lambda x: neptune.translateSequence(x['SEQUENCE_IMGT'][0:neptune.V_GENE_TOP]), axis = 1)

samples_df['V_GERMLINE_AA'] = \
    samples_df.apply(lambda x: neptune.translateSequence(x['GERMLINE_IMGT_D_MASK'][0:neptune.V_GENE_TOP]), axis = 1)

samples_df['GLYCOSYLATION_COUNT_V_SEQUENCE'] = \
    samples_df.apply(lambda x: countGlycosylation(x['V_SEQUENCE_AA']), axis = 1)

samples_df['GLYCOSYLATION_COUNT_V_GERMLINE'] = \
    samples_df.apply(lambda x: countGlycosylation(x['V_GERMLINE_AA']), axis = 1)

samples_df.to_csv(out_file)